/**
 * 
 */
package javacryption.hash;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author rhtrejo
 *
 */
public class SHA {
	
	/**
	 * @param text
	 * @return SHA-1 as HEX
	 */
	public static String sha1(String text){		
		BigInteger bInt = null;
				
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-1");
			md.update(text.getBytes());
			bInt = new BigInteger(1,md.digest());
			
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return bInt.toString(16);
	}
}
