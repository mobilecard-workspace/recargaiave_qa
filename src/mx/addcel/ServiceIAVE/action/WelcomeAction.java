package mx.addcel.ServiceIAVE.action;

import javacryption.aes.AesCtr;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.logging.Log;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class WelcomeAction
  extends DispatchAction
{
  public ActionForward inicio(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    String forward = null;
    String keyResponse = null;
    try
    {
      log.info("Entro a WelcomeAction inicio");
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      if ((keyResponse == null) || (keyResponse.length() == 0))
      {
        request.setAttribute("errorMensaje", "Error al validar session");
        forward = "error";
      }
      else
      {
        request.setAttribute("nextPage", "goToWelcome");
        forward = "welcomePage";
      }
    }
    catch (Exception e)
    {
      log.fatal("Excepcion : ", e);
      request.setAttribute("titulo", "Error");
      request.setAttribute("errorMensaje", "Error al completar las operaciones");
      forward = "error";
    }
    return mapping.findForward(forward);
  }
  
  public ActionForward iniciaRegistro(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    String forward = null;
    String keyResponse = null;
    try
    {
      log.info("Entro a WelcomeAction iniciaRegistro");
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      if ((keyResponse == null) || (keyResponse.length() == 0))
      {
        request.setAttribute("errorMensaje", "Error al validar session");
        forward = "error";
      }
      else
      {
        request.setAttribute("nextPage", "goToDatosComplementariosUno");
        request.setAttribute("flujo", "REGISTRO");
        forward = "nuevoUsuario";
      }
    }
    catch (Exception e)
    {
      log.fatal("Excepcion : ", e);
      request.setAttribute("titulo", "Error");
      request.setAttribute("errorMensaje", "Error al completar las operaciones");
      forward = "error";
    }
    return mapping.findForward(forward);
  }
  
  public ActionForward actualizaDatos(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    String forward = null;
    String keyResponse = null;
    String login = null;
    try
    {
      log.info("Entro a WelcomeAction actualizaDatos");
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      if ((keyResponse == null) || (keyResponse.length() == 0))
      {
        request.setAttribute("errorMensaje", "Error al validar session");
        forward = "error";
      }
      else
      {
        request.setAttribute("nextPage", "goToActualizaDatos");
        login = (String)request.getSession().getAttribute("loginMobileCard");
        request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
        request.setAttribute("login", AesCtr.encrypt(login, keyResponse, 256));
        forward = "actualizaDatos";
      }
    }
    catch (Exception e)
    {
      log.fatal("Excepcion : ", e);
      request.setAttribute("titulo", "Error");
      request.setAttribute("errorMensaje", "Error al completar las operaciones");
      forward = "error";
    }
    return mapping.findForward(forward);
  }
  
  public ActionForward cambiaPass(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    String forward = null;
    String keyResponse = null;
    String login = null;
    try
    {
      log.info("Entro a WelcomeAction cambiaPass");
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      if ((keyResponse == null) || (keyResponse.length() == 0))
      {
        request.setAttribute("errorMensaje", "Error al validar session");
        forward = "error";
      }
      else
      {
        request.setAttribute("nextPage", "goToCambiaPass");
        login = (String)request.getSession().getAttribute("loginMobileCard");
        request.setAttribute("login", login);
        forward = "cambiaPass";
      }
    }
    catch (Exception e)
    {
      log.fatal("Excepcion : ", e);
      request.setAttribute("titulo", "Error");
      request.setAttribute("errorMensaje", "Error al completar las operaciones");
      forward = "error";
    }
    return mapping.findForward(forward);
  }
  
  public ActionForward goToRecarga(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    String forward = null;
    String keyResponse = null;
    String login = null;
    try
    {
      log.info("Entro a WelcomeAction goToRecarga");
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      if ((keyResponse == null) || (keyResponse.length() == 0))
      {
        request.setAttribute("errorMensaje", "Error al validar session");
        forward = "error";
      }
      else
      {
        request.setAttribute("nextPage", "goToRecargaIAVE");
        login = (String)request.getSession().getAttribute("loginMobileCard");
        request.setAttribute("login", login);
        forward = "recargaIAVE";
      }
    }
    catch (Exception e)
    {
      log.fatal("Excepcion : ", e);
      request.setAttribute("titulo", "Error");
      request.setAttribute("errorMensaje", "Error al completar las operaciones");
      forward = "error";
    }
    return mapping.findForward(forward);
  }
}


/* Location:              /home/rhtrejo/workspace/RecargaIAVE/RecargaIAVE.war!/WEB-INF/classes/mx/addcel/ServiceIAVE/action/WelcomeAction.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */