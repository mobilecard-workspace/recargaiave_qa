package mx.addcel.ServiceIAVE.action;

import java.util.Enumeration;
import java.util.ResourceBundle;
import javacryption.aes.AesCtr;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.addcel.ServiceIAVE.service.ConsultasService;
import mx.addcel.ServiceIAVE.service.ConsumeServiciosService;
import mx.addcel.ServiceIAVE.service.vo.DatosIAVE_VO;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class DesvinculaCuentasAction
  extends DispatchAction
{
  private ConsumeServiciosService consumeServiciosService;
  private ConsultasService consultasService;
  private Logger log = Logger.getLogger(DesvinculaCuentasAction.class);
  private ResourceBundle bundle = ResourceBundle.getBundle("servicios");
  
  public ActionForward eliminiaVinculo(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    String element = null;
    Enumeration en = null;
    String forward = null;
    String keyResponse = null;
    String keyRequest = null;
    String[] resultado = null;
    DatosIAVE_VO datosIAVE = null;
    String loginAsociado = null;
    String loginEmail = null;
    String loginDn = null;
    String loginIMEI = null;
    String statusUsuario = null;
    this.log.info("Entro desvinculaCuentas");
    try
    {
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      keyRequest = (String)request.getSession().getAttribute("jCryptionKey_");
      if ((keyResponse == null) || (keyResponse.length() == 0) || (keyRequest == null) || (keyRequest.length() == 0))
      {
        request.setAttribute("titulo", "Error");
        request.setAttribute("errorMensaje", "Error al validar session");
        forward = "error";
      }
      else
      {
        datosIAVE = (DatosIAVE_VO)request.getSession().getAttribute("datosIAVE");
        if (request.getAttribute("nextPage").equals("goToDesvincula"))
        {
          datosIAVE = (DatosIAVE_VO)request.getSession().getAttribute("datosIAVE");
          this.consultasService.desvinculaCuentas(datosIAVE.getIduser());
          request.setAttribute("mensajeDesvinculado", AesCtr.encrypt("Su usuario ha sido desvinculado correctamente", keyResponse, 256));
          loginAsociado = this.consultasService.getMapeoIAVE(datosIAVE.getIduser());
          loginEmail = this.consultasService.existeEmail(datosIAVE.getEmail());
          loginDn = this.consultasService.existeTelefono(datosIAVE.getDn());
          loginIMEI = this.consultasService.existeIMEI(datosIAVE.getImei());
          if ((loginAsociado != null) && (loginAsociado.length() > 0))
          {
            this.log.info("entro a login asociado");
            this.log.info(loginAsociado);
            statusUsuario = this.consultasService.validaStatusUsuario(loginAsociado);
            if ((statusUsuario != null) && (statusUsuario.equals("1")))
            {
              request.getSession().setAttribute("loginMobileCard", loginAsociado);
              request.setAttribute("nextPage", "goToRecargaIAVE");
              forward = "recargaIAVE";
            }
            else if ((statusUsuario != null) && (statusUsuario.equals("98")))
            {
              request.setAttribute("login", loginAsociado);
              request.setAttribute("nextPage", "goToCambiaPass");
              forward = "cambiaPass";
            }
          }
          else if ((loginDn != null) && (loginDn.length() > 0))
          {
            this.log.info("entro a usuario existente DN");
            request.setAttribute("titulo", AesCtr.encrypt("Login con su usuario MobileCard", keyResponse, 256));
            request.setAttribute("errorMensaje", AesCtr.encrypt("Usted ya es usuario mobileCard, haga login con su cuenta", keyResponse, 256));
            request.setAttribute("login", AesCtr.encrypt(loginDn, keyResponse, 256));
            request.setAttribute("flujo", AesCtr.encrypt("ASOCIA", keyResponse, 256));
            request.getSession().setAttribute("jCryptionKey_", keyResponse);
            request.setAttribute("nextPage", "goToLogin");
            forward = "loginRedirect";
          }
          else if ((loginEmail != null) && (loginEmail.length() > 0))
          {
            this.log.info("entro a usuario existente email");
            request.setAttribute("titulo", AesCtr.encrypt("Login con su usuario MobileCard", keyResponse, 256));
            request.setAttribute("errorMensaje", AesCtr.encrypt("Usted ya es usuario mobileCard, haga login con su cuenta", keyResponse, 256));
            request.setAttribute("login", AesCtr.encrypt(loginEmail, keyResponse, 256));
            request.setAttribute("flujo", AesCtr.encrypt("ASOCIA", keyResponse, 256));
            request.getSession().setAttribute("jCryptionKey_", keyResponse);
            request.setAttribute("nextPage", "goToLogin");
            forward = "loginRedirect";
          }
          else if ((loginIMEI != null) && (loginIMEI.length() > 0))
          {
            this.log.info("entro a usuario existente imei");
            request.setAttribute("titulo", AesCtr.encrypt("Login con su usuario MobileCard", keyResponse, 256));
            request.setAttribute("errorMensaje", AesCtr.encrypt("Usted ya es usuario mobileCard, haga login con su cuenta", keyResponse, 256));
            request.setAttribute("login", AesCtr.encrypt(loginIMEI, keyResponse, 256));
            request.setAttribute("flujo", AesCtr.encrypt("ASOCIA", keyResponse, 256));
            request.getSession().setAttribute("jCryptionKey_", keyResponse);
            request.setAttribute("nextPage", "goToLogin");
            forward = "loginRedirect";
          }
          else
          {
            this.log.info("entro a usuario nuevo");
            request.setAttribute("nextPage", "goToWelcome");
            forward = "welcome";
          }
        }
        else
        {
          request.setAttribute("titulo", "Error");
          request.setAttribute("errorMensaje", "Acceso denegado");
          forward = "error";
        }
      }
    }
    catch (Exception e)
    {
      this.log.fatal("Error al validar login : ", e);
      request.setAttribute("titulo", "Error");
      request.setAttribute("errorMensaje", "Error al realizar las operaciones");
      forward = "error";
    }
    return mapping.findForward(forward);
  }
  
  public void setConsumeServiciosService(ConsumeServiciosService consumeServiciosService)
  {
    this.consumeServiciosService = consumeServiciosService;
  }
  
  public void setConsultasService(ConsultasService consultasService)
  {
    this.consultasService = consultasService;
  }
}


/* Location:              /home/rhtrejo/workspace/RecargaIAVE/RecargaIAVE.war!/WEB-INF/classes/mx/addcel/ServiceIAVE/action/DesvinculaCuentasAction.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */