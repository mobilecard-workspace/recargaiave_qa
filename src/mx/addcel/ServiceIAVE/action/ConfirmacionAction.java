package mx.addcel.ServiceIAVE.action;

import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.addcel.ServiceIAVE.service.ConsultasService;
import mx.addcel.ServiceIAVE.service.ConsumeServiciosService;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class ConfirmacionAction
  extends DispatchAction
{
  private Logger log = Logger.getLogger(ConfirmacionAction.class);
  private ConsumeServiciosService consumeServiciosService;
  private ConsultasService consultasService;
  private ResourceBundle bundle = ResourceBundle.getBundle("servicios");
  
  public ActionForward inicio(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    String keyResponse = null;
    String forward = null;
    try
    {
      this.log.info("Entro a ConfirmacionAction inicio");
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      if ((keyResponse == null) || (keyResponse.length() == 0))
      {
        request.setAttribute("errorMensaje", "Error al validar session");
        forward = "error";
      }
      else
      {
        forward = "confirmacion";
      }
    }
    catch (Exception e)
    {
      this.log.fatal("Excepcion : ", e);
      request.setAttribute("titulo", "Error");
      request.setAttribute("errorMensaje", "Error al completar las operaciones");
      forward = "error";
    }
    return mapping.findForward(forward);
  }
  
  public void setConsumeServiciosService(ConsumeServiciosService consumeServiciosService)
  {
    this.consumeServiciosService = consumeServiciosService;
  }
  
  public void setConsultasService(ConsultasService consultasService)
  {
    this.consultasService = consultasService;
  }
}


/* Location:              /home/rhtrejo/workspace/RecargaIAVE/RecargaIAVE.war!/WEB-INF/classes/mx/addcel/ServiceIAVE/action/ConfirmacionAction.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */