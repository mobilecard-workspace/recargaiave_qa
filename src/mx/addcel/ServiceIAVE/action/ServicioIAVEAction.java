package mx.addcel.ServiceIAVE.action;

import com.google.gson.Gson;
import com.ironbit.mc.system.crypto.Crypto;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javacryption.aes.AesCtr;
import javacryption.hash.SHA;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.addcel.ServiceIAVE.form.DatosInicioForm;
import mx.addcel.ServiceIAVE.form.LoginForm;
import mx.addcel.ServiceIAVE.service.ConsultasService;
import mx.addcel.ServiceIAVE.service.ConsumeServiciosService;
import mx.addcel.ServiceIAVE.service.utils.Utils;
import mx.addcel.ServiceIAVE.service.vo.DatosIAVE_VO;
import mx.addcel.ServiceIAVE.service.vo.TransactionProcomVO;
import mx.addcel.ServiceIAVE.service.vo.UsuarioVO;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.json.JSONObject;

public class ServicioIAVEAction
  extends DispatchAction
{
  private ConsumeServiciosService consumeServiciosService;
  private ConsultasService consultasService;
  private Logger log = Logger.getLogger(ServicioIAVEAction.class);
  private ResourceBundle bundle = ResourceBundle.getBundle("servicios");
  
  public void setConsultasService(ConsultasService consultasService)
  {
    this.consultasService = consultasService;
  }
  
  public ActionForward loginUsuario(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    String element = null;
    Enumeration en = null;
    String forward = null;
    String keyResponse = null;
    String keyRequest = null;
    String[] resultado = null;
    DatosIAVE_VO datosIAVE = null;
    LoginForm loginForm = null;
    String password = null;
    
    this.log.info("Entro loginUsuario");
    en = request.getParameterNames();
    this.log.info("***************inicio***************");
    while (en.hasMoreElements())
    {
      element = (String)en.nextElement();
      this.log.info(element + " : " + request.getParameter(element));
    }
    this.log.info("***************fin***************");
    try
    {
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      keyRequest = (String)request.getSession().getAttribute("jCryptionKey_");
      if ((keyResponse == null) || (keyResponse.length() == 0) || (keyRequest == null) || (keyRequest.length() == 0))
      {
        request.setAttribute("titulo", "Error");
        request.setAttribute("errorMensaje", "Error al validar session");
        forward = "error";
      }
      else
      {
        loginForm = (LoginForm)form;
        datosIAVE = (DatosIAVE_VO)request.getSession().getAttribute("datosIAVE");
        String login = AesCtr.decrypt(loginForm.getLoginEnc(), keyRequest, 256);
        password = AesCtr.decrypt(loginForm.getPasswordEnc(), keyRequest, 256);
        this.log.info(login);
        this.log.info(password);
        resultado = Utils.validacionLogin(this.bundle, this.consumeServiciosService, datosIAVE.getImei(), login, password, request.isSecure());
        if ((resultado != null) && (resultado[0].equals("1")))
        {
          datosIAVE = (DatosIAVE_VO)request.getSession().getAttribute("datosIAVE");
          this.consultasService.insertaMapeoUsuario(datosIAVE.getIduser(), login);
          request.getSession().setAttribute("loginMobileCard", login);
          forward = Utils.obtenDatosRecargaIAVE(this.bundle, this.consumeServiciosService, this.consultasService, keyResponse, request);
        }
        else
        {
          request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
          request.setAttribute("titulo", AesCtr.encrypt("Falla al autenticar usuario", keyResponse, 256));
          request.setAttribute("errorMensaje", AesCtr.encrypt(resultado[1], keyResponse, 256));
          forward = "login";
        }
      }
    }
    catch (Exception e)
    {
      this.log.fatal("Error al validar login : ", e);
      request.setAttribute("titulo", "Error");
      request.setAttribute("errorMensaje", "Error al realizar las operaciones");
      forward = "error";
    }
    return mapping.findForward(forward);
  }
  
  public ActionForward validaUsuario(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    String forward = null;
    String keyResponse = null;
    DatosInicioForm datos = null;
    DatosIAVE_VO datosIAVE = null;
    String loginAsociado = null;
    String loginEmail = null;
    String loginDn = null;
    String loginIMEI = null;
    try
    {
      this.log.info("Entro a validaUsuario");
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      if ((keyResponse == null) || (keyResponse.length() == 0))
      {
        request.setAttribute("titulo", "Error");
        request.setAttribute("errorMensaje", "Error al validar session");
        forward = "error";
      }
      else
      {
        datos = (DatosInicioForm)form;
        this.log.info(datos);
        datosIAVE = new DatosIAVE_VO();
        datosIAVE.setIduser(AesCtr.decrypt(datos.getIduserEnc(), keyResponse, 256));
        datosIAVE.setDn(AesCtr.decrypt(datos.getDnEnc(), keyResponse, 256));
        datosIAVE.setEmail(AesCtr.decrypt(datos.getEmailEnc(), keyResponse, 256));
        datosIAVE.setImei(AesCtr.decrypt(datos.getImeiEnc(), keyResponse, 256));
        datosIAVE.setTag(AesCtr.decrypt(datos.getTagEnc(), keyResponse, 256));
        datosIAVE.setNombreTag(AesCtr.decrypt(datos.getNombreTagEnc(), keyResponse, 256));
        datosIAVE.setPin(AesCtr.decrypt(datos.getPinEnc(), keyResponse, 256));
        this.log.info(datosIAVE);
        if ((datosIAVE.getIduser() == null) || (datosIAVE.getIduser().length() == 0) || (datosIAVE.getDn() == null) || (datosIAVE.getDn().length() == 0) || (datosIAVE.getEmail() == null) || (datosIAVE.getEmail().length() == 0) || (datosIAVE.getTag() == null) || (datosIAVE.getTag().length() == 0) || (datosIAVE.getNombreTag() == null) || (datosIAVE.getNombreTag().length() == 0) || (datosIAVE.getPin() == null) || (datosIAVE.getPin().length() == 0))
        {
          this.log.fatal("Error, datos insuficientes de IAVE");
          request.setAttribute("titulo", "Error");
          request.setAttribute("errorMensaje", "Error al recibir datos de TuTag, informaci&oacute;n incompleta");
          forward = "error";
        }
        else
        {
          request.getSession().setAttribute("datosIAVE", datosIAVE);
          loginAsociado = this.consultasService.getMapeoIAVE(datosIAVE.getIduser());
          loginEmail = this.consultasService.existeEmail(datosIAVE.getEmail());
          loginDn = this.consultasService.existeTelefono(datosIAVE.getDn());
          loginIMEI = this.consultasService.existeIMEI(datosIAVE.getImei());
          if ((loginAsociado != null) && (loginAsociado.length() > 0))
          {
            this.log.info("entro a login asociado");
            this.log.info(loginAsociado);                       
            request.getSession().setAttribute("loginMobileCard", loginAsociado);
            forward = Utils.obtenDatosRecargaIAVE(this.bundle, this.consumeServiciosService, this.consultasService, keyResponse, request);
          }
          else if ((loginDn != null) && (loginDn.length() > 0))
          {
            request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
            request.setAttribute("titulo", AesCtr.encrypt("Login con su usuario MobileCard", keyResponse, 256));
            request.setAttribute("errorMensaje", AesCtr.encrypt("Usted ya es usuario mobileCard, haga login con su cuenta", keyResponse, 256));
            request.setAttribute("login", AesCtr.encrypt(loginDn, keyResponse, 256));
            forward = "login";
          }
          else if ((loginEmail != null) && (loginEmail.length() > 0))
          {
            request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
            request.setAttribute("titulo", AesCtr.encrypt("Login con su usuario MobileCard", keyResponse, 256));
            request.setAttribute("errorMensaje", AesCtr.encrypt("Usted ya es usuario mobileCard, haga login con su cuenta", keyResponse, 256));
            request.setAttribute("login", AesCtr.encrypt(loginEmail, keyResponse, 256));
            forward = "login";
          }
          else if ((loginIMEI != null) && (loginIMEI.length() > 0))
          {
            request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
            request.setAttribute("titulo", AesCtr.encrypt("Login con su usuario MobileCard", keyResponse, 256));
            request.setAttribute("errorMensaje", AesCtr.encrypt("Usted ya es usuario mobileCard, haga login con su cuenta", keyResponse, 256));
            request.setAttribute("login", AesCtr.encrypt(loginIMEI, keyResponse, 256));
            forward = "login";
          }
          else
          {
            this.log.info("entro a usuario nuevo");
            forward = "nuevoUsuario";
          }
        }
      }
    }
    catch (Exception e)
    {
      this.log.fatal("Excepcion : ", e);
      request.setAttribute("titulo", "Error");
      request.setAttribute("errorMensaje", "Error al realizar las operaciones");
      forward = "error";
    }
    return mapping.findForward(forward);
  }
  
  public ActionForward terminosCondiciones(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    String urlBase = null;
    String servicio = null;
    String protocol = "http://";
    String keyResponse = null;
    String cadenaOriginal = null;
    String cadenaDesencriptada = null;
    String terminos = null;
    PrintWriter out = null;
    JSONObject json = null;
    try
    {
      this.log.info("Entro a terminosCondiciones");
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      if ((keyResponse == null) || (keyResponse.length() == 0))
      {
        json = new JSONObject();
        json.put("error", "-2");
      }
      else
      {
        if (request.isSecure()) {
          protocol = "https://";
        }
        urlBase = this.bundle.getString("urlbase");
        servicio = this.bundle.getString("servicioCondicionesURL");
        cadenaOriginal = this.consumeServiciosService.getCatalogo(protocol + urlBase + servicio, null);
        if ((cadenaOriginal != null) && (cadenaOriginal.length() > 0))
        {
          this.log.info("cadenaOriginal : " + cadenaOriginal);
          cadenaDesencriptada = Crypto.aesDecrypt("1234567890ABCDEF0123456789ABCDEF", cadenaOriginal);
          json = new JSONObject(cadenaDesencriptada);
          terminos = (String)json.get("Descripcion");
          terminos = terminos.replace("\n", "<br/>");
          this.log.info("terminos : " + terminos);
          this.log.info(terminos);
          if (terminos != null)
          {
            terminos = AesCtr.encrypt(terminos, keyResponse, 256);
            json.put("Descripcion", terminos);
          }
          json.put("llavePublica", request.getSession().getAttribute("publicKey"));
          json.put("error", "0");
        }
        else
        {
          json = new JSONObject();
          json.put("error", "-3");
          json.put("messaje", "No se encontro activo el servicio de terminos");
        }
      }
    }
    catch (Exception e)
    {
      this.log.fatal("Error al obtener terminos: ", e);
      json = new JSONObject();
      json.put("error", "-1");
    }
    finally
    {
      try
      {
        response.setContentType("application/json");
        out = response.getWriter();
        out.print(json.toString());
        this.log.info(json.toString());
        out.flush();
        out.close();
      }
      catch (Exception e)
      {
        this.log.fatal("Error al escribir terminos y condiciones");
      }
    }
    return null;
  }
  
  public ActionForward datosComplementarios(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    String urlBase = null;
    String servicio = null;
    String cadenaOriginal = null;
    String cadenaDesencriptada = null;
    String protocol = "http://";
    String keyResponse = null;
    String proveedores = null;
    String forward = null;
    DatosIAVE_VO datosIAVE = null;
    Calendar cal = null;
    String nuevoLogin = null;
    try
    {
      this.log.info("Entro a datosComplementarios");
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      if ((keyResponse == null) || (keyResponse.length() == 0))
      {
        request.setAttribute("titulo", "Error");
        request.setAttribute("errorMensaje", "Error al validar session");
        forward = "error";
      }
      else
      {
        if (request.isSecure()) {
          protocol = "https://";
        }
        datosIAVE = (DatosIAVE_VO)request.getSession().getAttribute("datosIAVE");
        this.log.info(datosIAVE);
        urlBase = this.bundle.getString("urlbase");
        servicio = this.bundle.getString("servicioCatalogoProvidersURL");
        cadenaOriginal = this.consumeServiciosService.getCatalogo(protocol + urlBase + servicio, null);
        if ((cadenaOriginal != null) && (cadenaOriginal.length() > 0))
        {
          this.log.info("cadenaOriginal : " + cadenaOriginal);
          cadenaDesencriptada = Crypto.aesDecrypt("1234567890ABCDEF0123456789ABCDEF", cadenaOriginal);
          this.log.info("cadenaDesencriptada : " + cadenaDesencriptada);
          proveedores = AesCtr.encrypt(cadenaDesencriptada, keyResponse, 256);
          nuevoLogin = datosIAVE.getEmail().substring(0, datosIAVE.getEmail().indexOf("@"));
          if (this.consultasService.existeUsuario(nuevoLogin).intValue() > 0) {
            nuevoLogin = "";
          }
          cal = Calendar.getInstance();
          request.setAttribute("email", AesCtr.encrypt(datosIAVE.getEmail(), keyResponse, 256));
          request.setAttribute("dn", AesCtr.encrypt(datosIAVE.getDn(), keyResponse, 256));
          request.setAttribute("login", AesCtr.encrypt(nuevoLogin, keyResponse, 256));
          request.setAttribute("yearFrom", AesCtr.encrypt(Integer.toString(cal.get(1) - 90), keyResponse, 256));
          request.setAttribute("yearTo", AesCtr.encrypt(Integer.toString(cal.get(1) - 6), keyResponse, 256));
          request.setAttribute("defaultYear", AesCtr.encrypt(Integer.toString(cal.get(1) - 30), keyResponse, 256));
          request.setAttribute("proveedores", proveedores);
          request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
          forward = "datosComplementarios";
        }
        else
        {
          request.setAttribute("titulo", "Error");
          request.setAttribute("errorMensaje", "Error de comunicación, intentelo nuevamente.");
          forward = "error";
        }
      }
    }
    catch (Exception e)
    {
      this.log.fatal("Excepcion : ", e);
      request.setAttribute("titulo", "Error");
      request.setAttribute("errorMensaje", "Error al completar las operaciones");
      forward = "error";
    }
    return mapping.findForward(forward);
  }
  
  public ActionForward datosComplementarios2(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    String urlBase = null;
    String servicio = null;
    String cadenaOriginal = null;
    String cadenaDesencriptada = null;
    String protocol = "http://";
    String keyRequest = null;
    String keyResponse = null;
    String estados = null;
    String tarjetas = null;
    String forward = null;
    UsuarioVO usuarioVO = null;
    DatosInicioForm datos = null;
    Calendar cal = null;
    PrintWriter out = null;
    try
    {
      out = response.getWriter();
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      keyRequest = (String)request.getSession().getAttribute("jCryptionKey_");
      if ((keyRequest == null) || (keyRequest.length() == 0) || (keyResponse == null) || (keyResponse.length() == 0))
      {
        request.setAttribute("titulo", "Error");
        request.setAttribute("errorMensaje", "Error al validar session");
        forward = "error";
      }
      else
      {
        if (request.isSecure()) {
          protocol = "https://";
        }
        datos = (DatosInicioForm)form;
        this.log.info(datos.getNombreUsuarioEnc());
        usuarioVO = new UsuarioVO();
        usuarioVO.setNombreUsuario(AesCtr.decrypt(datos.getNombreUsuarioEnc(), keyRequest, 256));
        usuarioVO.setNumCelular(AesCtr.decrypt(datos.getNumCelularEnc(), keyRequest, 256));
        usuarioVO.setProveedor(AesCtr.decrypt(datos.getProveedorEnc(), keyRequest, 256));
        usuarioVO.setNombre(AesCtr.decrypt(datos.getNombreEnc(), keyRequest, 256));
        usuarioVO.setApellidoP(AesCtr.decrypt(datos.getApellidoPEnc(), keyRequest, 256));
        usuarioVO.setApellidoM(AesCtr.decrypt(datos.getApellidoMEnc(), keyRequest, 256));
        usuarioVO.setFechaNac(AesCtr.decrypt(datos.getFechaNacEnc(), keyRequest, 256));
        usuarioVO.setSexo(AesCtr.decrypt(datos.getSexoEnc(), keyRequest, 256));
        usuarioVO.setTelefonoCasa(AesCtr.decrypt(datos.getTelefonoCasaEnc(), keyRequest, 256));
        usuarioVO.setTelefonoOficina(AesCtr.decrypt(datos.getTelefonoOficinaEnc(), keyRequest, 256));
        usuarioVO.setEmail(AesCtr.decrypt(datos.getEmailEnc(), keyRequest, 256));
        this.log.info(usuarioVO);
        urlBase = this.bundle.getString("urlbase");
        servicio = this.bundle.getString("servicioCatalogoEstadosURL");
        cadenaOriginal = this.consumeServiciosService.getCatalogo(protocol + urlBase + servicio, null);
        if ((cadenaOriginal != null) && (cadenaOriginal.length() > 0))
        {
          this.log.info("cadenaOriginal : " + cadenaOriginal);
          cadenaDesencriptada = Crypto.aesDecrypt("1234567890ABCDEF0123456789ABCDEF", cadenaOriginal);
          this.log.info("cadenaDesencriptada : " + cadenaDesencriptada);
          estados = AesCtr.encrypt(cadenaDesencriptada, keyResponse, 256);
          
          servicio = this.bundle.getString("servicioCatalogoTarjetasURL");
          cadenaOriginal = this.consumeServiciosService.getCatalogo(protocol + urlBase + servicio, null);
          if ((cadenaOriginal != null) && (cadenaOriginal.length() > 0))
          {
            this.log.info("cadenaOriginal : " + cadenaOriginal);
            cadenaDesencriptada = Crypto.aesDecrypt("1234567890ABCDEF0123456789ABCDEF", cadenaOriginal);
            this.log.info("cadenaDesencriptada : " + cadenaDesencriptada);
            tarjetas = AesCtr.encrypt(cadenaDesencriptada, keyResponse, 256);
            
            cal = Calendar.getInstance();
            request.setAttribute("estados", estados);
            request.setAttribute("tarjetas", tarjetas);
            request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
            request.setAttribute("yearFrom", AesCtr.encrypt(Integer.toString(cal.get(1)), keyResponse, 256));
            request.setAttribute("yearTo", AesCtr.encrypt(Integer.toString(cal.get(1) + 15), keyResponse, 256));
            request.setAttribute("curMonth", AesCtr.encrypt(Integer.toString(cal.get(2) + 1), keyResponse, 256));
            forward = "datosComplementarios2";
            request.getSession().setAttribute("usuarioVO", usuarioVO);
          }
          else
          {
            request.setAttribute("titulo", "Error");
            request.setAttribute("errorMensaje", "Error de comunicación, intentelo nuevamente.");
            forward = "error";
          }
        }
        else
        {
          request.setAttribute("titulo", "Error");
          request.setAttribute("errorMensaje", "Error de comunicación, intentelo nuevamente.");
          forward = "error";
        }
      }
    }
    catch (Exception e)
    {
      this.log.fatal("Excepcion : ", e);
      request.setAttribute("titulo", "Error");
      request.setAttribute("errorMensaje", "Error al completar las operaciones");
      forward = "error";
    }
    return mapping.findForward(forward);
  }
  
  public ActionForward registraUsuario(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    JSONObject jsonObj = null;
    UsuarioVO usuarioVO = null;
    String protocol = "http://";
    String keyResponse = null;
    String keyRequest = null;
    DatosInicioForm datos = null;
    String forward = null;
    DateFormat df = null;
    DatosIAVE_VO datosIAVE = null;
    String urlBase = null;
    String servicio = null;
    String cadenaOriginal = null;
    String cadenaDesencriptada = null;
    String jEnc = null;
    String newEnc = null;
    HashMap parametros = null;
    String[] resultado = null;
    try
    {
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      keyRequest = (String)request.getSession().getAttribute("jCryptionKey_");
      if ((keyResponse == null) || (keyResponse.length() == 0) || (keyRequest == null) || (keyRequest.length() == 0))
      {
        request.setAttribute("titulo", "Error");
        request.setAttribute("errorMensaje", "Error al validar session");
        forward = "error";
      }
      else
      {
        if (request.isSecure()) {
          protocol = "https://";
        }
        datos = (DatosInicioForm)form;
        usuarioVO = (UsuarioVO)request.getSession().getAttribute("usuarioVO");
        datosIAVE = (DatosIAVE_VO)request.getSession().getAttribute("datosIAVE");
        usuarioVO.setEstado(AesCtr.decrypt(datos.getEstadoEnc(), keyRequest, 256));
        usuarioVO.setCiudad(AesCtr.decrypt(datos.getCiudadEnc(), keyRequest, 256));
        usuarioVO.setCalle(AesCtr.decrypt(datos.getCalleEnc(), keyRequest, 256));
        usuarioVO.setNumExt(AesCtr.decrypt(datos.getNumExtEnc(), keyRequest, 256));
        usuarioVO.setNumInt(AesCtr.decrypt(datos.getNumIntEnc(), keyRequest, 256));
        usuarioVO.setColonia(AesCtr.decrypt(datos.getColoniaEnc(), keyRequest, 256));
        usuarioVO.setCodigoPostal(AesCtr.decrypt(datos.getCodigoPostalEnc(), keyRequest, 256));
        usuarioVO.setNumTarjeta(AesCtr.decrypt(datos.getNumTarjetaEnc(), keyRequest, 256));
        usuarioVO.setTipoTarjeta(AesCtr.decrypt(datos.getTipoTarjetaEnc(), keyRequest, 256));
        usuarioVO.setCodigoPostalAMEX(AesCtr.decrypt(datos.getCodigoPostalAMEXEnc(), keyRequest, 256));
        usuarioVO.setDomicilioAMEX(AesCtr.decrypt(datos.getDomicilioAMEXEnc(), keyRequest, 256));
        usuarioVO.setVigenciaTarjeta(AesCtr.decrypt(datos.getVigenciaTarjetaEnc(), keyRequest, 256));
        usuarioVO.setTerminos(AesCtr.decrypt(datos.getTerminosEnc(), keyRequest, 256));
        usuarioVO.setPlataforma(AesCtr.decrypt(datos.getPlataformaEnc(), keyRequest, 256));
        this.log.info(usuarioVO);
        request.getSession().setAttribute("usuarioVO", usuarioVO);
        df = new SimpleDateFormat("dd/MM/yyyy");
        jsonObj = new JSONObject();
        if (usuarioVO.getNombreUsuario() != null) {
          jsonObj.put("login", usuarioVO.getNombreUsuario());
        }
        if ((usuarioVO.getFechaNac() != null) && (usuarioVO.getFechaNac().length() > 0)) {
          jsonObj.put("nacimiento", usuarioVO.getFechaNac());
        }
        if (usuarioVO.getNumCelular() != null) {
          jsonObj.put("telefono", usuarioVO.getNumCelular());
        }
        jsonObj.put("registro", df.format(new Date()));
        if (usuarioVO.getNombre() != null) {
          jsonObj.put("nombre", usuarioVO.getNombre());
        }
        if (usuarioVO.getApellidoP() != null) {
          jsonObj.put("apellido", usuarioVO.getApellidoP());
        }
        if (usuarioVO.getApellidoM() != null) {
          jsonObj.put("materno", usuarioVO.getApellidoM());
        }
        if (usuarioVO.getSexo() != null) {
          jsonObj.put("sexo", usuarioVO.getSexo());
        }
        if (usuarioVO.getTelefonoCasa() != null) {
          jsonObj.put("tel_casa", usuarioVO.getTelefonoCasa());
        }
        if (usuarioVO.getTelefonoOficina() != null) {
          jsonObj.put("tel_oficina", usuarioVO.getTelefonoOficina());
        }
        if (usuarioVO.getCiudad() != null) {
          jsonObj.put("ciudad", usuarioVO.getCiudad());
        }
        if (usuarioVO.getCalle() != null) {
          jsonObj.put("calle", usuarioVO.getCalle());
        }
        if (usuarioVO.getNumExt() != null) {
          jsonObj.put("num_ext", Integer.parseInt(usuarioVO.getNumExt()));
        }
        if (usuarioVO.getNumInt() != null) {
          jsonObj.put("num_interior", usuarioVO.getNumInt());
        }
        if (usuarioVO.getColonia() != null) {
          jsonObj.put("colonia", usuarioVO.getColonia());
        }
        if (usuarioVO.getCodigoPostal() != null) {
          jsonObj.put("cp", usuarioVO.getCodigoPostal());
        }
        if (usuarioVO.getDomicilioAMEX() != null) {
          jsonObj.put("dom_amex", usuarioVO.getDomicilioAMEX());
        }
        if (usuarioVO.getEstado() != null) {
          jsonObj.put("id_estado", Integer.parseInt(usuarioVO.getEstado()));
        }
        jsonObj.put("direccion", "");
        if (usuarioVO.getEmail() != null) {
          jsonObj.put("mail", usuarioVO.getEmail());
        }
        if (usuarioVO.getNumTarjeta() != null) {
          jsonObj.put("tarjeta", usuarioVO.getNumTarjeta());
        }
        if (usuarioVO.getVigenciaTarjeta() != null) {
          jsonObj.put("vigencia", usuarioVO.getVigenciaTarjeta());
        }
        jsonObj.put("banco", 0);
        if (usuarioVO.getTipoTarjeta() != null) {
          jsonObj.put("tipotarjeta", Integer.parseInt(usuarioVO.getTipoTarjeta()));
        }
        if (usuarioVO.getProveedor() != null) {
          jsonObj.put("proveedor", Integer.parseInt(usuarioVO.getProveedor()));
        }
        jsonObj.put("status", 1000);
        if (datosIAVE.getImei() != null) {
          jsonObj.put("imei", "IAVE" + datosIAVE.getImei());
        }
        jsonObj.put("etiqueta", "");
        jsonObj.put("numero", "");
        
        jsonObj.put("tipo", usuarioVO.getPlataforma());
        jsonObj.put("software", "1.1.1");
        jsonObj.put("modelo", "web");
        jsonObj.put("terminos", "1");
        if (datosIAVE.getImei() != null) {
          jsonObj.put("key", datosIAVE.getImei());
        }
        if (request.isSecure()) {
          protocol = "https://";
        }
        this.log.info(jsonObj);
        urlBase = this.bundle.getString("urlbase");
        servicio = this.bundle.getString("newUserURL");
        jEnc = Crypto.aesEncrypt(Utils.parsePass("mCL8m39sJ1"), jsonObj.toString());
        newEnc = Utils.mergeStr(jEnc, "mCL8m39sJ1");
        
        this.log.info(protocol + urlBase + servicio + "?json=" + newEnc);
        parametros = new HashMap();
        parametros.put("json", newEnc);
        cadenaOriginal = this.consumeServiciosService.registraUsuario(protocol + urlBase + servicio, parametros);
        if ((cadenaOriginal != null) && (cadenaOriginal.length() > 0))
        {
          this.log.info("cadenaOriginal : " + cadenaOriginal);
          cadenaDesencriptada = Crypto.aesDecrypt(Utils.parsePass("mCL8m39sJ1"), cadenaOriginal);
          this.log.info("cadenaDesencriptada : " + cadenaDesencriptada);
          jsonObj = new JSONObject(cadenaDesencriptada);
          resultado = new String[2];
          resultado[0] = Integer.toString(((Integer)jsonObj.get("resultado")).intValue());
          resultado[1] = ((String)jsonObj.get("mensaje"));
          if (resultado[0].equals("1"))
          {
            this.consultasService.insertaMapeoUsuario(datosIAVE.getIduser(), usuarioVO.getNombreUsuario());
            request.setAttribute("mensajeExito", resultado[1]);
            request.getSession().setAttribute("loginMobileCard", usuarioVO.getNombreUsuario());
            forward = "cargaRecarga";
          }
          else
          {
            request.setAttribute("titulo", "Error");
            request.setAttribute("errorMensaje", resultado[1]);
            forward = "error";
          }
        }
        else
        {
          request.setAttribute("titulo", "Error");
          request.setAttribute("errorMensaje", "Error de comunicación, intentelo nuevamente.");
          forward = "error";
        }
      }
    }
    catch (Exception e)
    {
      this.log.error("Error al regsitrar usuario : ", e);
      request.setAttribute("titulo", "Error");
      request.setAttribute("errorMensaje", "Ocurrio un error al realizar las operaciones");
      forward = "error";
    }
    return mapping.findForward(forward);
  }
  
  public ActionForward recargarIAVE(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    String keyResponse = null;
    String forward = null;
    String protocol = "http://";
    try
    {
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      if ((keyResponse == null) || (keyResponse.length() == 0))
      {
        request.setAttribute("titulo", "Error");
        request.setAttribute("errorMensaje", "Error al validar session");
        forward = "error";
      }
      else
      {
        if (request.isSecure()) {
          protocol = "https://";
        }
        forward = Utils.obtenDatosRecargaIAVE(this.bundle, this.consumeServiciosService, this.consultasService, keyResponse, request);
      }
    }
    catch (Exception e)
    {
      this.log.fatal("Excepcion : ", e);
      request.setAttribute("titulo", "Error");
      request.setAttribute("errorMensaje", "Error al completar las operaciones");
      forward = "error";
    }
    return mapping.findForward(forward);
  }
  
  public ActionForward pagar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    String urlBase = null;
    String servicio = null;
    String cadenaOriginal = null;
    String cadenaDesencriptada = null;
    String protocol = "http://";
    String keyResponse = null;
    String keyRequest = null;
    String forward = null;
    String respuesta = null;
    UsuarioVO usuarioVO = null;
    DatosIAVE_VO datosIAVE = null;
    DatosInicioForm datos = null;
    JSONObject jsonObj = null;
    String jEnc = null;
    String newEnc = null;
    HashMap parametros = null;
    String[] resultado = null;
    try
    {
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      keyRequest = (String)request.getSession().getAttribute("jCryptionKey_");
      if ((keyResponse == null) || (keyResponse.length() == 0) || (keyRequest == null) || (keyRequest.length() == 0))
      {
        request.setAttribute("titulo", "Error");
        request.setAttribute("errorMensaje", "Error al validar session");
        forward = "error";
      }
      else
      {
        if (request.isSecure()) {
          protocol = "https://";
        }
        datos = (DatosInicioForm)form;
        usuarioVO = (UsuarioVO)request.getSession().getAttribute("usuarioVO");
        if (usuarioVO == null) {
          usuarioVO = new UsuarioVO();
        }
        datosIAVE = (DatosIAVE_VO)request.getSession().getAttribute("datosIAVE");
        usuarioVO.setPassMobileCard(AesCtr.decrypt(datos.getPassMobileCardEnc(), keyRequest, 256));
        usuarioVO.setPlataforma(AesCtr.decrypt(datos.getPlataformaEnc(), keyRequest, 256));
        usuarioVO.setCvv2(AesCtr.decrypt(datos.getCvv2Enc(), keyRequest, 256));
        usuarioVO.setMonto(AesCtr.decrypt(datos.getMontoEnc(), keyRequest, 256));
        usuarioVO.setNombreUsuario((String)request.getSession().getAttribute("loginMobileCard"));
        this.log.info(usuarioVO);
        resultado = Utils.validacionLogin(this.bundle, this.consumeServiciosService, datosIAVE.getImei(), usuarioVO.getNombreUsuario(), usuarioVO.getPassMobileCard(), request.isSecure());
        if ((resultado != null) && (resultado[0].equals("1")))
        {
          urlBase = this.bundle.getString("urlbase");
          servicio = this.bundle.getString("servicioCompraIAVE");
          jsonObj = new JSONObject();
          jsonObj.put("login", usuarioVO.getNombreUsuario());
          jsonObj.put("password", usuarioVO.getPassMobileCard());
          jsonObj.put("cvv2", usuarioVO.getCvv2());
          jsonObj.put("pin", datosIAVE.getPin());
          jsonObj.put("tarjeta", datosIAVE.getTag());
          jsonObj.put("vigencia", this.consultasService.tdcVigencia(usuarioVO.getNombreUsuario()));
          jsonObj.put("producto", usuarioVO.getMonto());
          jsonObj.put("imei", "IAVE" + datosIAVE.getImei());
          jsonObj.put("cx", "");
          jsonObj.put("cy", "");
          jsonObj.put("tipo", usuarioVO.getPlataforma());
          jsonObj.put("software", "1.1.1");
          jsonObj.put("modelo", "web");
          jsonObj.put("key", datosIAVE.getImei());
          this.log.info(jsonObj);
          
          jEnc = Crypto.aesEncrypt(Utils.parsePass(usuarioVO.getPassMobileCard()), jsonObj.toString());
          newEnc = Utils.mergeStr(jEnc, usuarioVO.getPassMobileCard());
          parametros = new HashMap();
          parametros.put("json", newEnc);
          cadenaOriginal = this.consumeServiciosService.getCatalogo(protocol + urlBase + servicio, parametros);
          if ((cadenaOriginal != null) && (cadenaOriginal.length() > 0))
          {
            this.log.info("cadenaOriginal : " + cadenaOriginal);
            cadenaDesencriptada = Crypto.aesDecrypt(Utils.parsePass(usuarioVO.getPassMobileCard()), cadenaOriginal);
            this.log.info("cadenaDesencriptada : " + cadenaDesencriptada);
            jsonObj = new JSONObject(cadenaDesencriptada);
            if (Integer.toString(((Integer)jsonObj.get("resultado")).intValue()).equals("1"))
            {
              this.log.info("Pago exitoso");
              request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
              request.setAttribute("aliasTag", AesCtr.encrypt(datosIAVE.getNombreTag(), keyResponse, 256));
              request.setAttribute("tag", AesCtr.encrypt(datosIAVE.getTag(), keyResponse, 256));
              request.setAttribute("mensage", AesCtr.encrypt((String)jsonObj.get("resultado"), keyResponse, 256));
              request.setAttribute("autorizacion", AesCtr.encrypt((String)jsonObj.get("folio"), keyResponse, 256));
              forward = "pagoExitoso";
            }
            else
            {
              this.log.info("Error al realizar pago");
              request.setAttribute("titulo", AesCtr.encrypt("Error al realizar pago", keyResponse, 256));
              request.setAttribute("mensajeError", AesCtr.encrypt((String)jsonObj.get("mensaje"), keyResponse, 256));
              forward = Utils.obtenDatosRecargaIAVE(this.bundle, this.consumeServiciosService, this.consultasService, keyResponse, request);
            }
          }
          else
          {
            request.setAttribute("titulo", "Error");
            request.setAttribute("errorMensaje", "Error de comunicación, intentelo nuevamente.");
            forward = "error";
          }
        }
        else
        {
          this.log.info("Entro pass no valido");
          request.setAttribute("titulo", AesCtr.encrypt("Error", keyResponse, 256));
          request.setAttribute("mensajeError", "El password ingresado no coincide con el usuario MobileCard");
          forward = Utils.obtenDatosRecargaIAVE(this.bundle, this.consumeServiciosService, this.consultasService, keyResponse, request);
        }
      }
    }
    catch (Exception e)
    {
      this.log.fatal("Excepcion : ", e);
      request.setAttribute("titulo", "Error");
      request.setAttribute("errorMensaje", "Error al completar las operaciones");
      forward = "error";
    }
    return mapping.findForward(forward);
  }
  
  public ActionForward validaLogin(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    String login = null;
    String keyResponse = null;
    String keyRequest = null;
    PrintWriter out = null;
    JSONObject json = null;
    try
    {
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      keyRequest = (String)request.getSession().getAttribute("jCryptionKey_");
      if ((keyResponse == null) || (keyResponse.length() == 0) || (keyRequest == null) || (keyRequest.length() == 0))
      {
        json = new JSONObject();
        json.put("error", "-4");
      }
      else
      {
        json = new JSONObject();
        login = request.getParameter("login");
        if ((login != null) && (login.length() > 0))
        {
          login = AesCtr.decrypt(login, keyRequest, 256);
          if (this.consultasService.existeUsuario(login).intValue() > 0) {
            json.put("valido", "0");
          } else {
            json.put("valido", "1");
          }
          json.put("error", "0");
        }
        else
        {
          json.put("error", "-3");
        }
      }
    }
    catch (Exception e)
    {
      this.log.fatal("Error al obtener terminos: ", e);
      json = new JSONObject();
      json.put("error", "-1");
    }
    finally
    {
      try
      {
        response.setContentType("application/json");
        out = response.getWriter();
        out.print(AesCtr.encrypt(json.toString(), keyResponse, 256));
        this.log.info(json.toString());
        out.flush();
        out.close();
      }
      catch (Exception e)
      {
        this.log.fatal("Error al validar login");
      }
    }
    return null;
  }
  
  public ActionForward validaTDC(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    String tdc = null;
    String login = null;
    PrintWriter out = null;
    JSONObject json = null;
    String keyResponse = null;
    String keyRequest = null;
    try
    {
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      keyRequest = (String)request.getSession().getAttribute("jCryptionKey_");
      if ((keyResponse == null) || (keyResponse.length() == 0) || (keyRequest == null) || (keyRequest.length() == 0))
      {
        json = new JSONObject();
        json.put("error", "-4");
      }
      else
      {
        json = new JSONObject();
        tdc = request.getParameter("tdc");
        if ((tdc != null) && (tdc.length() > 0))
        {
          tdc = AesCtr.decrypt(tdc, keyRequest, 256);
          login = this.consultasService.existeTDC(tdc);
          if ((login != null) && (login.length() > 0))
          {
            json.put("valido", "0");
            json.put("login", login);
          }
          else
          {
            json.put("valido", "1");
          }
          json.put("error", "0");
        }
        else
        {
          json.put("error", "-3");
        }
      }
    }
    catch (Exception e)
    {
      this.log.fatal("Error al obtener terminos: ", e);
      json = new JSONObject();
      json.put("error", "-1");
    }
    finally
    {
      try
      {
        response.setContentType("application/json");
        out = response.getWriter();
        out.print(AesCtr.encrypt(json.toString(), keyResponse, 256));
        this.log.info(json.toString());
        out.flush();
        out.close();
      }
      catch (Exception e)
      {
        this.log.fatal("Error al validar login");
      }
    }
    return null;
  }
  
  public ActionForward validaEmail(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    String login = null;
    String email = null;
    PrintWriter out = null;
    JSONObject json = null;
    String keyResponse = null;
    String keyRequest = null;
    try
    {
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      keyRequest = (String)request.getSession().getAttribute("jCryptionKey_");
      if ((keyResponse == null) || (keyResponse.length() == 0) || (keyRequest == null) || (keyRequest.length() == 0))
      {
        json = new JSONObject();
        json.put("error", "-4");
      }
      else
      {
        json = new JSONObject();
        email = request.getParameter("email");
        if ((email != null) && (email.length() > 0))
        {
          email = AesCtr.decrypt(email, keyRequest, 256);
          login = this.consultasService.existeEmail(email);
          if ((login != null) && (login.length() > 0))
          {
            json.put("valido", "0");
            json.put("login", login);
          }
          else
          {
            json.put("valido", "1");
          }
          json.put("error", "0");
        }
        else
        {
          json.put("error", "-3");
        }
      }
    }
    catch (Exception e)
    {
      this.log.fatal("Error al validar email: ", e);
      json = new JSONObject();
      json.put("error", "-1");
    }
    finally
    {
      try
      {
        response.setContentType("application/json");
        out = response.getWriter();
        out.print(AesCtr.encrypt(json.toString(), keyResponse, 256));
        this.log.info(json.toString());
        out.flush();
        out.close();
      }
      catch (Exception e)
      {
        this.log.fatal("Error al validar login");
      }
    }
    return null;
  }
  
  public ActionForward recuperaPass(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    String login = null;
    PrintWriter out = null;
    JSONObject json = null;
    JSONObject jsonObj = null;
    int count = 0;
    String keyResponse = null;
    String keyRequest = null;
    String urlBase = null;
    String servicio = null;
    String jEnc = null;
    String cadenaOriginal = null;
    String cadenaDesencriptada = null;
    String respuesta = null;
    String protocol = "http://";
    this.log.info("Entro a recuperaPass");
    HashMap parametros = null;
    try
    {
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      keyRequest = (String)request.getSession().getAttribute("jCryptionKey_");
      if ((keyResponse == null) || (keyResponse.length() == 0) || (keyRequest == null) || (keyRequest.length() == 0))
      {
        json = new JSONObject();
        json.put("error", "-4");
      }
      else
      {
        if (request.isSecure()) {
          protocol = "https://";
        }
        json = new JSONObject();
        login = request.getParameter("login");
        if ((login != null) && (login.length() > 0))
        {
          login = AesCtr.decrypt(login, keyRequest, 256);
          count = this.consultasService.existeUsuario(login).intValue();
          if (count > 0)
          {
            jsonObj = new JSONObject();
            jsonObj.put("cadena", login);
            this.log.info(jsonObj);
            jEnc = Crypto.aesEncrypt("1234567890ABCDEF0123456789ABCDEF", jsonObj.toString());
            parametros = new HashMap();
            parametros.put("json", jEnc);
            urlBase = this.bundle.getString("urlbase");
            servicio = this.bundle.getString("servicioRecuperaPassURL");
            cadenaOriginal = this.consumeServiciosService.getCatalogo(protocol + urlBase + servicio, parametros);
            if ((cadenaOriginal != null) && (cadenaOriginal.length() > 0))
            {
              this.log.info("cadenaOriginal : " + cadenaOriginal);
              cadenaDesencriptada = Crypto.aesDecrypt("1234567890ABCDEF0123456789ABCDEF", cadenaOriginal);
              this.log.info("cadenaDesencriptada : " + cadenaDesencriptada);
              if ((cadenaDesencriptada != null) && (cadenaDesencriptada.equals("0"))) {
                json.put("valido", "1");
              } else {
                json.put("error", "-5");
              }
            }
            else
            {
              json.put("error", "-4");
            }
          }
          else
          {
            json.put("valido", "0");
          }
          json.put("error", "0");
        }
        else
        {
          json.put("error", "-3");
        }
      }
    }
    catch (Exception e)
    {
      this.log.fatal("Error al validar login: ", e);
      json = new JSONObject();
      json.put("error", "-1");
    }
    finally
    {
      try
      {
        response.setContentType("application/json");
        out = response.getWriter();
        out.print(AesCtr.encrypt(json.toString(), keyResponse, 256));
        this.log.info(json.toString());
        out.flush();
        out.close();
      }
      catch (Exception e)
      {
        this.log.fatal("Error al validar login, envio de json");
      }
    }
    return null;
  }
  
  public ActionForward validaDn(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    String dn = null;
    PrintWriter out = null;
    JSONObject json = null;
    String login = null;
    String keyResponse = null;
    String keyRequest = null;
    this.log.info("Entro a validaDn");
    try
    {
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      keyRequest = (String)request.getSession().getAttribute("jCryptionKey_");
      if ((keyResponse == null) || (keyResponse.length() == 0) || (keyRequest == null) || (keyRequest.length() == 0))
      {
        json = new JSONObject();
        json.put("error", "-4");
      }
      else
      {
        json = new JSONObject();
        dn = request.getParameter("dn");
        if ((dn != null) && (dn.length() > 0))
        {
          dn = AesCtr.decrypt(dn, keyRequest, 256);
          login = this.consultasService.existeTelefono(dn);
          if ((login != null) && (login.length() > 0))
          {
            json.put("valido", "0");
            json.put("login", login);
          }
          else
          {
            json.put("valido", "1");
          }
          json.put("error", "0");
        }
        else
        {
          json.put("error", "-3");
        }
      }
    }
    catch (Exception e)
    {
      this.log.fatal("Error al validar dn: ", e);
      json = new JSONObject();
      json.put("error", "-1");
    }
    finally
    {
      try
      {
        response.setContentType("application/json");
        out = response.getWriter();
        out.print(AesCtr.encrypt(json.toString(), keyResponse, 256));
        this.log.info(json.toString());
        out.flush();
        out.close();
      }
      catch (Exception e)
      {
        this.log.fatal("Error al validar dn, envio de json");
      }
    }
    return null;
  }
  
  public void setConsumeServiciosService(ConsumeServiciosService consumeServiciosService)
  {
    this.consumeServiciosService = consumeServiciosService;
  }
  
	
  public ActionForward respuestaProsa3Ds(ActionMapping mapping,  ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		String EM_Response = null, EM_Total = null, EM_OrderID = null, EM_Merchant = null, EM_Store = null,
				EM_Term = null, EM_RefNum = null, EM_Auth = null, EM_Digest = null, email = null, cc_numberback = null,
				cc_nameback = null, tag = null, pin = null, imei = null, nombreTag = null;
		
		String digest;
		String protocol = "http://";
		String urlBase = this.bundle.getString("urlbase");
		String servicio = this.bundle.getString("servicioCompraIAVE3Ds");
		String cadenaOriginal = null;
		
		String forward = "errorRecarga";

		this.log.info("Dentro del servicio: respuestaProsa3Ds");
				
		@SuppressWarnings("unchecked")
		Map<String, String[]> parameters = request.getParameterMap();

		for (String key : parameters.keySet()) {
			this.log.info(key);

			String[] vals = parameters.get(key);
			if (key.equals("EM_Response"))
				EM_Response = vals[0];
			else if (key.equals("EM_Total"))
				EM_Total = vals[0];
			else if (key.equals("EM_OrderID"))
				EM_OrderID = vals[0];
			else if (key.equals("EM_Merchant"))
				EM_Merchant = vals[0];
			else if (key.equals("EM_Store"))
				EM_Store = vals[0];
			else if (key.equals("EM_Term"))
				EM_Term = vals[0];
			else if (key.equals("EM_RefNum"))
				EM_RefNum = vals[0];
			else if (key.equals("EM_Auth"))
				EM_Auth = vals[0];
			else if (key.equals("EM_Digest"))
				EM_Digest = vals[0];
			else if (key.equals("email"))
				email = vals[0];
			else if (key.equals("tag"))
				tag = vals[0];
			else if (key.equals("nombreTag"))
				nombreTag = vals[0];
			else if (key.equals("pin"))
				pin = vals[0];
			else if (key.equals("imei"))
				imei = vals[0];
			else if (key.equals("cc_number")) {
				cc_numberback = vals[0];
			} else if (key.equals("cc_name"))
				cc_nameback = vals[0];

			for (String val : vals)
				this.log.info(" -> " + val);
		}

		TransactionProcomVO tp = new TransactionProcomVO();
		tp.setEmAuth(EM_Auth);
		tp.setEmMerchant(EM_Merchant);
		tp.setEmOrderID(EM_OrderID);
		tp.setEmRefNum(EM_RefNum);
		tp.setEmResponse(EM_Response);
		tp.setEmStore(EM_Store);
		tp.setEmTerm(EM_Term);
		tp.setEmTotal(EM_Total);
		tp.setEmDigest(EM_Digest);
		
		//TODO insertar espuesta 3Dsecure en bitacora
		
		this.log.debug("email: "+ email);
		
		//SHA1 (EM_Total + EM_Order_id + EM_Merchant + EM_Store + EM_Term + EM_Refnum + "-" +EM_Auth)
		digest = SHA.sha1(EM_Total + EM_OrderID + EM_Merchant + EM_Store + EM_Term + EM_RefNum + "-" +EM_Auth);
		
		//TODO validar con EM_Response = "approved"
		if(EM_Digest.equalsIgnoreCase(digest) && EM_Response.equalsIgnoreCase("approved")){
			if (request.isSecure()) {
				protocol = "https://";
			}
			
			UsuarioVO usuarioVO = this.consultasService.consultaUsuarioByIdBitacora(Long.parseLong(EM_OrderID));
			UsuarioVO usuarioVOCompra = (UsuarioVO)request.getSession().getAttribute("usuarioVO");
			if(usuarioVOCompra != null){
				usuarioVO.setPassMobileCard(usuarioVOCompra.getPassMobileCard());
				usuarioVO.setMonto(usuarioVOCompra.getMonto());
			}
			
			
			//DatosIAVE_VO datosIAVE = (DatosIAVE_VO) request.getSession().getAttribute("datosIAVE");
			
			this.log.info(usuarioVO);
			//this.log.info(datosIAVE);
			
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("login", usuarioVO.getNombreUsuario());
			jsonObj.put("password", usuarioVO.getPassMobileCard());
			jsonObj.put("cvv2", usuarioVO.getCvv2());
			jsonObj.put("pin", pin);
			jsonObj.put("tarjeta", tag);
			String fechaVigencia = this.consultasService.tdcVigencia(usuarioVO.getNombreUsuario());
			jsonObj.put("vigencia", (fechaVigencia != null) && (fechaVigencia.matches("\\d{2}/\\d{2}"))
					? fechaVigencia.replace("/", "") : fechaVigencia);
			jsonObj.put("producto", usuarioVO.getMonto());
			jsonObj.put("imei", "IAVE" + imei);
			jsonObj.put("cx", "");
			jsonObj.put("cy", "");
			jsonObj.put("tipo", usuarioVO.getPlataforma());
			jsonObj.put("software", "1.1.1");
			jsonObj.put("modelo", "web");
			jsonObj.put("key", imei);
			
			this.log.info(jsonObj);

			String jEnc = Crypto.aesEncrypt(Utils.parsePass(usuarioVO.getPassMobileCard()), jsonObj.toString());
			String newEnc = Utils.mergeStr(jEnc, usuarioVO.getPassMobileCard());
			
			HashMap parametros = new HashMap();
			parametros.put("json", newEnc);			
			
			
			//HashMap parametros = (HashMap)request.getSession().getAttribute("parametrosJson");
			parametros.put("idBitacora", EM_OrderID);
			
			Gson gson = new Gson();		
			String jsonTransactionProcomVO = gson.toJson(tp);								
			parametros.put("transactionProcomVO", jsonTransactionProcomVO);
			
			this.log.info("parametros:");
			this.log.info(parametros);
			
			//envío de notificación exitosa de pago a IAVE
			this.log.info("redireccionando a: " + protocol + urlBase + servicio);
			cadenaOriginal = this.consumeServiciosService.compraIAVE(protocol + urlBase + servicio, parametros);			
			this.log.info("cadenaOriginalCif: "+ cadenaOriginal);
			
			String keyResponse = (String) request.getSession().getAttribute("jCryptionKey");
			this.log.info("keyResponse: "+ keyResponse);
			
			String keyJson = usuarioVO.getPassMobileCard();
			
			if ((keyResponse == null) || (keyResponse.length() == 0)) {
				request.setAttribute("titulo", "Error");
				request.setAttribute("errorMensaje", "Error al validar session");
				forward = "error";
			} else {
			
				if ((cadenaOriginal != null) && (cadenaOriginal.length() > 0)) {					
					//this.log.info("key : " + keyJson);
					String cadenaDesencriptada = Crypto.aesDecrypt(Utils.parsePass(keyJson), cadenaOriginal);
					this.log.info("cadenaDesencriptada : " + cadenaDesencriptada);
					try {
						jsonObj = new JSONObject(cadenaDesencriptada);					
						log.info("publicKey: "+request.getSession().getAttribute("publicKey"));
						request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
						if (Integer.toString(((Integer) jsonObj.get("resultado")).intValue()).equals("1")) {
							this.log.info("Pago exitoso");
							String montoRecarga = this.consultasService.obtenMontoRecarga(usuarioVO.getMonto());
							request.setAttribute("aliasTag",
									AesCtr.encrypt(nombreTag, keyResponse, 256));
							request.setAttribute("tag", AesCtr.encrypt(tag, keyResponse, 256));
							request.setAttribute("mensageRecarga",
									AesCtr.encrypt((String) jsonObj.get("mensaje"), keyResponse, 256));
							request.setAttribute("monto", AesCtr.encrypt(montoRecarga, keyResponse, 256));
							request.setAttribute("autorizacion",
									AesCtr.encrypt((String) jsonObj.get("folio"), keyResponse, 256));
							request.setAttribute("nextPage", "goToConfirmacionPago");
							forward = "pagoExitoso";
						} else {
							this.log.info("Error al realizar pago");
							request.setAttribute("mensajeError",
									AesCtr.encrypt((String) jsonObj.get("mensaje"), keyResponse, 256));
							request.setAttribute("nextPage", "goToRecargaIAVE");
							forward = "errorRecarga";
						}
					} catch (ParseException e) {
						this.log.fatal("Excepcion : ", e);
						request.setAttribute("titulo", "Error");
						request.setAttribute("errorMensaje", "Error al completar las operaciones");
						forward = "error";
					}
				} else {
					request.setAttribute("errorMensaje", "Error de comunicaci&oacute;n, intentelo nuevamente.");
					forward = "error";
				}
		}	
			
		}else{
			request.setAttribute("errorMensaje", EM_Response);
			forward = "error";
		}
		
		
		return mapping.findForward(forward);
	}
  
}
