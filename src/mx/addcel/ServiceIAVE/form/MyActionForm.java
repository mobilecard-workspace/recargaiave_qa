package mx.addcel.ServiceIAVE.form;

import java.lang.reflect.Method;
import org.apache.struts.action.ActionForm;

public class MyActionForm
  extends ActionForm
{
  public String toString()
  {
    StringBuffer resultado = new StringBuffer("");
    Class myClass = getClass();
    Method[] metodos = myClass.getMethods();
    int methodsLength = metodos.length;
    metodos = myClass.getDeclaredMethods();
    methodsLength = metodos.length;
    for (int i = 0; i < methodsLength; i++) {
      try
      {
        if (metodos[i].getParameterTypes().length == 0)
        {
          resultado.append(" ");
          resultado.append(metodos[i].getName());
          resultado.append(": ");
          resultado.append(metodos[i].invoke(this, new Object[0]));
          resultado.append(" \n");
        }
      }
      catch (Exception e) {}
    }
    return resultado.toString();
  }
}


/* Location:              /home/rhtrejo/workspace/RecargaIAVE/RecargaIAVE.war!/WEB-INF/classes/mx/addcel/ServiceIAVE/form/MyActionForm.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */