package mx.addcel.ServiceIAVE.form;

import java.util.Iterator;
import javacryption.aes.AesCtr;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import mx.addcel.ServiceIAVE.service.utils.StrutsMensajes;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.util.PropertyMessageResources;

public class CargaIAVEForm
  extends MyActionForm
{
  private Logger log = Logger.getLogger(CargaIAVEForm.class);
  private String iduserEnc;
  private String dnEnc;
  private String emailEnc;
  private String imeiEnc;
  private String tagEnc;
  private String nombreTagEnc;
  private String pinEnc;
  private String iduser;
  private String dn;
  private String email;
  private String imei;
  private String tag;
  private String nombreTag;
  private String pin;
  
  public void reset(ActionMapping mapping, HttpServletRequest request)
  {
    setIduserEnc(null);
    setDnEnc(null);
    setEmailEnc(null);
    setImeiEnc(null);
    setTagEnc(null);
    setNombreTagEnc(null);
    setPinEnc(null);
    setIduser(null);
    setDn(null);
    setEmail(null);
    setImei(null);
    setTag(null);
    setNombreTag(null);
    setPin(null);
  }
  
  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request)
  {
    ActionErrors errors = new ActionErrors();
    String keyResponse = null;
    try
    {
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      if (GenericValidator.isBlankOrNull(keyResponse))
      {
        errors.add(null, new ActionMessage("Application.llaveInexistente"));
      }
      else
      {
        if (GenericValidator.isBlankOrNull(getIduserEnc())) {
          errors.add("idUserEnc", new ActionMessage("CargaIAVEAction.idUserEnc"));
        }
        if (GenericValidator.isBlankOrNull(getDnEnc())) {
          errors.add("dnEnc", new ActionMessage("CargaIAVEAction.dnEnc"));
        }
        if (GenericValidator.isBlankOrNull(getEmailEnc())) {
          errors.add("emailEnc", new ActionMessage("CargaIAVEAction.emailEnc"));
        }
        if (GenericValidator.isBlankOrNull(getImeiEnc())) {
          errors.add("imeiEnc", new ActionMessage("CargaIAVEAction.imeiEnc"));
        }
        if (GenericValidator.isBlankOrNull(getTagEnc())) {
          errors.add("tagEnc", new ActionMessage("CargaIAVEAction.tagEnc"));
        }
        if (GenericValidator.isBlankOrNull(getNombreTagEnc())) {
          errors.add("nombreTagEnc", new ActionMessage("CargaIAVEAction.nombreTagEnc"));
        }
        if (GenericValidator.isBlankOrNull(getPinEnc())) {
          errors.add("pinEnc", new ActionMessage("CargaIAVEAction.pinEnc"));
        }
        if (errors.size() == 0)
        {
          setIduser(AesCtr.decrypt(getIduserEnc(), keyResponse, 256));
          setDn(AesCtr.decrypt(getDnEnc(), keyResponse, 256));
          setEmail(AesCtr.decrypt(getEmailEnc(), keyResponse, 256));
          setImei(AesCtr.decrypt(getImeiEnc(), keyResponse, 256));
          setTag(AesCtr.decrypt(getTagEnc(), keyResponse, 256));
          setNombreTag(AesCtr.decrypt(getNombreTagEnc(), keyResponse, 256));
          setPin(AesCtr.decrypt(getPinEnc(), keyResponse, 256));
          if (GenericValidator.isBlankOrNull(getIduser())) {
            errors.add("idUserEnc", new ActionMessage("CargaIAVEAction.idUserEnc"));
          }
          if (GenericValidator.isBlankOrNull(getDn())) {
            errors.add("dnEnc", new ActionMessage("CargaIAVEAction.dnEnc"));
          }
          if (GenericValidator.isBlankOrNull(getEmail())) {
            errors.add("emailEnc", new ActionMessage("CargaIAVEAction.emailEnc"));
          }
          if (GenericValidator.isBlankOrNull(getImei())) {
            errors.add("imeiEnc", new ActionMessage("CargaIAVEAction.imeiEnc"));
          }
          if (GenericValidator.isBlankOrNull(getTag())) {
            errors.add("tagEnc", new ActionMessage("CargaIAVEAction.tagEnc"));
          }
          if (GenericValidator.isBlankOrNull(getNombreTag())) {
            errors.add("nombreTagEnc", new ActionMessage("CargaIAVEAction.nombreTagEnc"));
          }
          if (GenericValidator.isBlankOrNull(getPin())) {
            errors.add("pinEnc", new ActionMessage("CargaIAVEAction.pinEnc"));
          }
          if (errors.size() == 0)
          {
            if (!getIduser().matches("^[0-9]*$")) {
              errors.add("idUserEnc", new ActionMessage("CargaIAVEAction.pinEnc.format"));
            }
            if (!getDn().matches("^\\d{10}$")) {
              errors.add("dnEnc", new ActionMessage("CargaIAVEAction.dnEnc.format"));
            }
            if (!GenericValidator.isEmail(getEmail())) {
              errors.add("emailEnc", new ActionMessage("CargaIAVEAction.emailEnc.format"));
            }
            if (!getTag().matches("^[0-9]*$")) {
              errors.add("tagEnc", new ActionMessage("CargaIAVEAction.tagEnc.format"));
            }
            if (!getPin().matches("^\\d{1}$")) {
              errors.add("pinEnc", new ActionMessage("CargaIAVEAction.pinEnc.format"));
            }
          }
        }
      }
      if (errors.size() != 0)
      {
        request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
        StringBuilder sb = new StringBuilder();
        Iterator<ActionMessage> iter = errors.get();
        PropertyMessageResources p = (PropertyMessageResources)request.getAttribute("org.apache.struts.action.MESSAGE");
        while (iter.hasNext())
        {
          ActionMessage msg = (ActionMessage)iter.next();
          sb.append(StrutsMensajes.getErrorMessage(p, msg));
          sb.append("<br/>");
        }
        this.log.info(sb);
        this.log.info(toString());
        request.setAttribute("errorMensaje", AesCtr.encrypt(sb.toString(), keyResponse, 256));
      }
    }
    catch (Exception e)
    {
      this.log.error("Exeption en validacion CargaIAVEForm : ", e);
      if (request.getAttribute("llavePublica") != null) {
        request.removeAttribute("llavePublica");
      }
      request.setAttribute("errorMensaje", "Error inesperado en la validaci&oacute;n");
    }
    return errors;
  }
  
  public String getIduserEnc()
  {
    return this.iduserEnc;
  }
  
  public void setIduserEnc(String iduserEnc)
  {
    this.iduserEnc = iduserEnc;
  }
  
  public String getDnEnc()
  {
    return this.dnEnc;
  }
  
  public void setDnEnc(String dnEnc)
  {
    this.dnEnc = dnEnc;
  }
  
  public String getEmailEnc()
  {
    return this.emailEnc;
  }
  
  public void setEmailEnc(String emailEnc)
  {
    this.emailEnc = emailEnc;
  }
  
  public String getImeiEnc()
  {
    return this.imeiEnc;
  }
  
  public void setImeiEnc(String imeiEnc)
  {
    this.imeiEnc = imeiEnc;
  }
  
  public String getTagEnc()
  {
    return this.tagEnc;
  }
  
  public void setTagEnc(String tagEnc)
  {
    this.tagEnc = tagEnc;
  }
  
  public String getNombreTagEnc()
  {
    return this.nombreTagEnc;
  }
  
  public void setNombreTagEnc(String nombreTagEnc)
  {
    this.nombreTagEnc = nombreTagEnc;
  }
  
  public String getPinEnc()
  {
    return this.pinEnc;
  }
  
  public void setPinEnc(String pinEnc)
  {
    this.pinEnc = pinEnc;
  }
  
  public String getIduser()
  {
    return this.iduser;
  }
  
  public void setIduser(String iduser)
  {
    this.iduser = iduser;
  }
  
  public String getDn()
  {
    return this.dn;
  }
  
  public void setDn(String dn)
  {
    this.dn = dn;
  }
  
  public String getEmail()
  {
    return this.email;
  }
  
  public void setEmail(String email)
  {
    this.email = email;
  }
  
  public String getImei()
  {
    return this.imei;
  }
  
  public void setImei(String imei)
  {
    this.imei = imei;
  }
  
  public String getTag()
  {
    return this.tag;
  }
  
  public void setTag(String tag)
  {
    this.tag = tag;
  }
  
  public String getNombreTag()
  {
    return this.nombreTag;
  }
  
  public void setNombreTag(String nombreTag)
  {
    this.nombreTag = nombreTag;
  }
  
  public String getPin()
  {
    return this.pin;
  }
  
  public void setPin(String pin)
  {
    this.pin = pin;
  }
}


/* Location:              /home/rhtrejo/workspace/RecargaIAVE/RecargaIAVE.war!/WEB-INF/classes/mx/addcel/ServiceIAVE/form/CargaIAVEForm.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */