package mx.addcel.ServiceIAVE.form;

import java.util.Iterator;
import javacryption.aes.AesCtr;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import mx.addcel.ServiceIAVE.service.utils.StrutsMensajes;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.util.PropertyMessageResources;

public class CambiaPassForm
  extends MyActionForm
{
  private Logger log = Logger.getLogger(CambiaPassForm.class);
  private String loginEnc;
  private String passwordActualEnc;
  private String passwordNuevoEnc;
  private String login;
  private String pass;
  private String nuevoPass;
  
  public String getLoginEnc()
  {
    return this.loginEnc;
  }
  
  public void setLoginEnc(String loginEnc)
  {
    this.loginEnc = loginEnc;
  }
  
  public String getLogin()
  {
    return this.login;
  }
  
  public void setLogin(String login)
  {
    this.login = login;
  }
  
  public String getPass()
  {
    return this.pass;
  }
  
  public void setPass(String pass)
  {
    this.pass = pass;
  }
  
  public String getNuevoPass()
  {
    return this.nuevoPass;
  }
  
  public void setNuevoPass(String nuevoPass)
  {
    this.nuevoPass = nuevoPass;
  }
  
  public void reset(ActionMapping mapping, HttpServletRequest request)
  {
    setLoginEnc(null);
    setPasswordActualEnc(null);
    setPasswordNuevoEnc(null);
    setLogin(null);
    setPass(null);
    setNuevoPass(null);
  }
  
  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request)
  {
    ActionErrors errors = new ActionErrors();
    String keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
    String keyRequest = (String)request.getSession().getAttribute("jCryptionKey_");
    this.log.info("validate CambiaPassForm");
    try
    {
      String nextPage = (String)request.getAttribute("nextPage");
      this.log.info("CambiaPassForm validate");
      if ((nextPage == null) || (!nextPage.equals("goToCambiaPass"))) {
        if ((GenericValidator.isBlankOrNull(keyResponse)) || (GenericValidator.isBlankOrNull(keyRequest)))
        {
          errors.add(null, new ActionMessage("Application.llaveInexistente"));
        }
        else
        {
          if (GenericValidator.isBlankOrNull(getLoginEnc())) {
            errors.add("loginEnc", new ActionMessage("CambiaPassAction.loginEnc"));
          }
          if (GenericValidator.isBlankOrNull(getPasswordActualEnc())) {
            errors.add("passwordEnc", new ActionMessage("CambiaPassAction.passwordEnc"));
          }
          if (GenericValidator.isBlankOrNull(getPasswordNuevoEnc())) {
            errors.add("nuevoPassEnc", new ActionMessage("CambiaPassAction.nuevoPassEnc"));
          }
          if (errors.size() == 0)
          {
            setLogin(AesCtr.decrypt(getLoginEnc(), keyRequest, 256));
            setPass(AesCtr.decrypt(getPasswordActualEnc(), keyRequest, 256));
            setNuevoPass(AesCtr.decrypt(getPasswordNuevoEnc(), keyRequest, 256));
            if (GenericValidator.isBlankOrNull(getLogin())) {
              errors.add("loginEnc", new ActionMessage("CambiaPassAction.loginEnc"));
            }
            if (GenericValidator.isBlankOrNull(getPass())) {
              errors.add("passwordEnc", new ActionMessage("CambiaPassAction.passwordEnc"));
            }
            if (GenericValidator.isBlankOrNull(getNuevoPass())) {
              errors.add("nuevoPassEnc", new ActionMessage("CambiaPassAction.nuevoPassEnc"));
            }
          }
          if (errors.size() != 0)
          {
            request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
            StringBuilder sb = new StringBuilder();
            Iterator<ActionMessage> iter = errors.get();
            PropertyMessageResources p = (PropertyMessageResources)request.getAttribute("org.apache.struts.action.MESSAGE");
            while (iter.hasNext())
            {
              ActionMessage msg = (ActionMessage)iter.next();
              sb.append(StrutsMensajes.getErrorMessage(p, msg));
              sb.append("<br/>");
            }
            this.log.info(sb);
            this.log.info(toString());
            request.setAttribute("errorMensaje", AesCtr.encrypt(sb.toString(), keyResponse, 256));
          }
        }
      }
    }
    catch (Exception e)
    {
      this.log.error("Exeption en validacion CambiaPassForm : ", e);
      if (request.getAttribute("llavePublica") != null) {
        request.removeAttribute("llavePublica");
      }
      request.setAttribute("errorMensaje", "Error inesperado en la validaci&oacute;n");
    }
    return errors;
  }
  
  public String getPasswordActualEnc()
  {
    return this.passwordActualEnc;
  }
  
  public void setPasswordActualEnc(String passwordActualEnc)
  {
    this.passwordActualEnc = passwordActualEnc;
  }
  
  public String getPasswordNuevoEnc()
  {
    return this.passwordNuevoEnc;
  }
  
  public void setPasswordNuevoEnc(String passwordNuevoEnc)
  {
    this.passwordNuevoEnc = passwordNuevoEnc;
  }
}


/* Location:              /home/rhtrejo/workspace/RecargaIAVE/RecargaIAVE.war!/WEB-INF/classes/mx/addcel/ServiceIAVE/form/CambiaPassForm.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */