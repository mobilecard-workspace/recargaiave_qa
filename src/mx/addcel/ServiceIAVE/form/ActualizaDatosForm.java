package mx.addcel.ServiceIAVE.form;

import java.util.Iterator;
import javacryption.aes.AesCtr;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import mx.addcel.ServiceIAVE.service.utils.StrutsMensajes;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.util.PropertyMessageResources;

public class ActualizaDatosForm
  extends MyActionForm
{
  private Logger log = Logger.getLogger(ActualizaDatosForm.class);
  private String estadoEnc;
  private String ciudadEnc;
  private String calleEnc;
  private String numExtEnc;
  private String numIntEnc;
  private String coloniaEnc;
  private String codigoPostalEnc;
  private String numTarjetaEnc;
  private String tipoTarjetaEnc;
  private String codigoPostalAMEXEnc;
  private String domicilioAMEXEnc;
  private String vigenciaTarjetaEnc;
  private String terminosEnc;
  private String plataformaEnc;
  private String estado;
  private String ciudad;
  private String calle;
  private String numExt;
  private String numInt;
  private String colonia;
  private String codigoPostal;
  private String numTarjeta;
  private String tipoTarjeta;
  private String codigoPostalAMEX;
  private String domicilioAMEX;
  private String vigenciaTarjeta;
  private String terminos;
  private String plataforma;
  private String numCelularEnc;
  private String proveedorEnc;
  private String nombreEnc;
  private String apellidoPEnc;
  private String apellidoMEnc;
  private String fechaNacEnc;
  private String sexoEnc;
  private String emailEnc;
  private String telefonoCasaEnc;
  private String telefonoOficinaEnc;
  private String nombreUsuario;
  private String numCelular;
  private String proveedor;
  private String nombre;
  private String apellidoP;
  private String apellidoM;
  private String fechaNac;
  private String sexo;
  private String email;
  private String telefonoCasa;
  private String telefonoOficina;
  
  public String getNumCelularEnc()
  {
    return this.numCelularEnc;
  }
  
  public void setNumCelularEnc(String numCelularEnc)
  {
    this.numCelularEnc = numCelularEnc;
  }
  
  public String getProveedorEnc()
  {
    return this.proveedorEnc;
  }
  
  public void setProveedorEnc(String proveedorEnc)
  {
    this.proveedorEnc = proveedorEnc;
  }
  
  public String getNombreEnc()
  {
    return this.nombreEnc;
  }
  
  public void setNombreEnc(String nombreEnc)
  {
    this.nombreEnc = nombreEnc;
  }
  
  public String getApellidoPEnc()
  {
    return this.apellidoPEnc;
  }
  
  public void setApellidoPEnc(String apellidoPEnc)
  {
    this.apellidoPEnc = apellidoPEnc;
  }
  
  public String getApellidoMEnc()
  {
    return this.apellidoMEnc;
  }
  
  public void setApellidoMEnc(String apellidoMEnc)
  {
    this.apellidoMEnc = apellidoMEnc;
  }
  
  public String getFechaNacEnc()
  {
    return this.fechaNacEnc;
  }
  
  public void setFechaNacEnc(String fechaNacEnc)
  {
    this.fechaNacEnc = fechaNacEnc;
  }
  
  public String getSexoEnc()
  {
    return this.sexoEnc;
  }
  
  public void setSexoEnc(String sexoEnc)
  {
    this.sexoEnc = sexoEnc;
  }
  
  public String getNumCelular()
  {
    return this.numCelular;
  }
  
  public void setNumCelular(String numCelular)
  {
    this.numCelular = numCelular;
  }
  
  public String getProveedor()
  {
    return this.proveedor;
  }
  
  public void setProveedor(String proveedor)
  {
    this.proveedor = proveedor;
  }
  
  public String getNombre()
  {
    return this.nombre;
  }
  
  public void setNombre(String nombre)
  {
    this.nombre = nombre;
  }
  
  public String getApellidoP()
  {
    return this.apellidoP;
  }
  
  public void setApellidoP(String apellidoP)
  {
    this.apellidoP = apellidoP;
  }
  
  public String getApellidoM()
  {
    return this.apellidoM;
  }
  
  public void setApellidoM(String apellidoM)
  {
    this.apellidoM = apellidoM;
  }
  
  public String getFechaNac()
  {
    return this.fechaNac;
  }
  
  public void setFechaNac(String fechaNac)
  {
    this.fechaNac = fechaNac;
  }
  
  public String getSexo()
  {
    return this.sexo;
  }
  
  public void setSexo(String sexo)
  {
    this.sexo = sexo;
  }
  
  public void reset(ActionMapping mapping, HttpServletRequest request)
  {
    setEstadoEnc(null);
    setCiudadEnc(null);
    setCalleEnc(null);
    setNumExtEnc(null);
    setNumIntEnc(null);
    setColoniaEnc(null);
    setCodigoPostalEnc(null);
    setNumTarjetaEnc(null);
    setTipoTarjetaEnc(null);
    setCodigoPostalAMEXEnc(null);
    setDomicilioAMEXEnc(null);
    setVigenciaTarjetaEnc(null);
    setTerminosEnc(null);
    setPlataformaEnc(null);
    setEstado(null);
    setCiudad(null);
    setCalle(null);
    setNumExt(null);
    setNumInt(null);
    setColonia(null);
    setCodigoPostal(null);
    setNumTarjeta(null);
    setTipoTarjeta(null);
    setCodigoPostalAMEX(null);
    setDomicilioAMEX(null);
    setVigenciaTarjeta(null);
    setTerminos(null);
    setPlataforma(null);
    setNumCelularEnc(null);
    setProveedorEnc(null);
    setNombreEnc(null);
    setApellidoPEnc(null);
    setApellidoMEnc(null);
    setFechaNacEnc(null);
    setSexoEnc(null);
    setEmailEnc(null);
    setTelefonoCasaEnc(null);
    setTelefonoOficinaEnc(null);
    setNombreUsuario(null);
    setNumCelular(null);
    setProveedor(null);
    setNombre(null);
    setApellidoP(null);
    setApellidoM(null);
    setFechaNac(null);
    setSexo(null);
    setEmail(null);
    setTelefonoCasa(null);
    setTelefonoOficina(null);
  }
  
  public String getTelefonoCasaEnc()
  {
    return this.telefonoCasaEnc;
  }
  
  public void setTelefonoCasaEnc(String telefonoCasaEnc)
  {
    this.telefonoCasaEnc = telefonoCasaEnc;
  }
  
  public String getTelefonoOficinaEnc()
  {
    return this.telefonoOficinaEnc;
  }
  
  public void setTelefonoOficinaEnc(String telefonoOficinaEnc)
  {
    this.telefonoOficinaEnc = telefonoOficinaEnc;
  }
  
  public String getTelefonoCasa()
  {
    return this.telefonoCasa;
  }
  
  public void setTelefonoCasa(String telefonoCasa)
  {
    this.telefonoCasa = telefonoCasa;
  }
  
  public String getTelefonoOficina()
  {
    return this.telefonoOficina;
  }
  
  public void setTelefonoOficina(String telefonoOficina)
  {
    this.telefonoOficina = telefonoOficina;
  }
  
  public String getNombreUsuario()
  {
    return this.nombreUsuario;
  }
  
  public void setNombreUsuario(String nombreUsuario)
  {
    this.nombreUsuario = nombreUsuario;
  }
  
  public String getEmailEnc()
  {
    return this.emailEnc;
  }
  
  public void setEmailEnc(String emailEnc)
  {
    this.emailEnc = emailEnc;
  }
  
  public String getEmail()
  {
    return this.email;
  }
  
  public void setEmail(String email)
  {
    this.email = email;
  }
  
  public String getEstadoEnc()
  {
    return this.estadoEnc;
  }
  
  public void setEstadoEnc(String estadoEnc)
  {
    this.estadoEnc = estadoEnc;
  }
  
  public String getCiudadEnc()
  {
    return this.ciudadEnc;
  }
  
  public void setCiudadEnc(String ciudadEnc)
  {
    this.ciudadEnc = ciudadEnc;
  }
  
  public String getCalleEnc()
  {
    return this.calleEnc;
  }
  
  public void setCalleEnc(String calleEnc)
  {
    this.calleEnc = calleEnc;
  }
  
  public String getNumExtEnc()
  {
    return this.numExtEnc;
  }
  
  public void setNumExtEnc(String numExtEnc)
  {
    this.numExtEnc = numExtEnc;
  }
  
  public String getNumIntEnc()
  {
    return this.numIntEnc;
  }
  
  public void setNumIntEnc(String numIntEnc)
  {
    this.numIntEnc = numIntEnc;
  }
  
  public String getColoniaEnc()
  {
    return this.coloniaEnc;
  }
  
  public void setColoniaEnc(String coloniaEnc)
  {
    this.coloniaEnc = coloniaEnc;
  }
  
  public String getCodigoPostalEnc()
  {
    return this.codigoPostalEnc;
  }
  
  public void setCodigoPostalEnc(String codigoPostalEnc)
  {
    this.codigoPostalEnc = codigoPostalEnc;
  }
  
  public String getNumTarjetaEnc()
  {
    return this.numTarjetaEnc;
  }
  
  public void setNumTarjetaEnc(String numTarjetaEnc)
  {
    this.numTarjetaEnc = numTarjetaEnc;
  }
  
  public String getTipoTarjetaEnc()
  {
    return this.tipoTarjetaEnc;
  }
  
  public void setTipoTarjetaEnc(String tipoTarjetaEnc)
  {
    this.tipoTarjetaEnc = tipoTarjetaEnc;
  }
  
  public String getCodigoPostalAMEXEnc()
  {
    return this.codigoPostalAMEXEnc;
  }
  
  public void setCodigoPostalAMEXEnc(String codigoPostalAMEXEnc)
  {
    this.codigoPostalAMEXEnc = codigoPostalAMEXEnc;
  }
  
  public String getDomicilioAMEXEnc()
  {
    return this.domicilioAMEXEnc;
  }
  
  public void setDomicilioAMEXEnc(String domicilioAMEXEnc)
  {
    this.domicilioAMEXEnc = domicilioAMEXEnc;
  }
  
  public String getVigenciaTarjetaEnc()
  {
    return this.vigenciaTarjetaEnc;
  }
  
  public void setVigenciaTarjetaEnc(String vigenciaTarjetaEnc)
  {
    this.vigenciaTarjetaEnc = vigenciaTarjetaEnc;
  }
  
  public String getTerminosEnc()
  {
    return this.terminosEnc;
  }
  
  public void setTerminosEnc(String terminosEnc)
  {
    this.terminosEnc = terminosEnc;
  }
  
  public String getPlataformaEnc()
  {
    return this.plataformaEnc;
  }
  
  public void setPlataformaEnc(String plataformaEnc)
  {
    this.plataformaEnc = plataformaEnc;
  }
  
  public String getEstado()
  {
    return this.estado;
  }
  
  public void setEstado(String estado)
  {
    this.estado = estado;
  }
  
  public String getCiudad()
  {
    return this.ciudad;
  }
  
  public void setCiudad(String ciudad)
  {
    this.ciudad = ciudad;
  }
  
  public String getCalle()
  {
    return this.calle;
  }
  
  public void setCalle(String calle)
  {
    this.calle = calle;
  }
  
  public String getNumExt()
  {
    return this.numExt;
  }
  
  public void setNumExt(String numExt)
  {
    this.numExt = numExt;
  }
  
  public String getNumInt()
  {
    return this.numInt;
  }
  
  public void setNumInt(String numInt)
  {
    this.numInt = numInt;
  }
  
  public String getColonia()
  {
    return this.colonia;
  }
  
  public void setColonia(String colonia)
  {
    this.colonia = colonia;
  }
  
  public String getCodigoPostal()
  {
    return this.codigoPostal;
  }
  
  public void setCodigoPostal(String codigoPostal)
  {
    this.codigoPostal = codigoPostal;
  }
  
  public String getNumTarjeta()
  {
    return this.numTarjeta;
  }
  
  public void setNumTarjeta(String numTarjeta)
  {
    this.numTarjeta = numTarjeta;
  }
  
  public String getTipoTarjeta()
  {
    return this.tipoTarjeta;
  }
  
  public void setTipoTarjeta(String tipoTarjeta)
  {
    this.tipoTarjeta = tipoTarjeta;
  }
  
  public String getCodigoPostalAMEX()
  {
    return this.codigoPostalAMEX;
  }
  
  public void setCodigoPostalAMEX(String codigoPostalAMEX)
  {
    this.codigoPostalAMEX = codigoPostalAMEX;
  }
  
  public String getDomicilioAMEX()
  {
    return this.domicilioAMEX;
  }
  
  public void setDomicilioAMEX(String domicilioAMEX)
  {
    this.domicilioAMEX = domicilioAMEX;
  }
  
  public String getVigenciaTarjeta()
  {
    return this.vigenciaTarjeta;
  }
  
  public void setVigenciaTarjeta(String vigenciaTarjeta)
  {
    this.vigenciaTarjeta = vigenciaTarjeta;
  }
  
  public String getTerminos()
  {
    return this.terminos;
  }
  
  public void setTerminos(String terminos)
  {
    this.terminos = terminos;
  }
  
  public String getPlataforma()
  {
    return this.plataforma;
  }
  
  public void setPlataforma(String plataforma)
  {
    this.plataforma = plataforma;
  }
  
  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request)
  {
    ActionErrors errors = new ActionErrors();
    String keyResponse = null;
    String keyRequest = null;
    try
    {
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      keyRequest = (String)request.getSession().getAttribute("jCryptionKey_");
      String nextPage = (String)request.getAttribute("nextPage");
      this.log.info("ActualizaDatosForm validate");
      if ((nextPage == null) || (!nextPage.equals("goToActualizaDatos"))) {
        if ((GenericValidator.isBlankOrNull(keyResponse)) || (GenericValidator.isBlankOrNull(keyRequest)))
        {
          errors.add(null, new ActionMessage("Application.llaveInexistente"));
        }
        else
        {
          if (GenericValidator.isBlankOrNull(getEstadoEnc())) {
            errors.add("estadoEnc", new ActionMessage("ActualizaDatosForm.estadoEnc"));
          }
          if (GenericValidator.isBlankOrNull(getCiudadEnc())) {
            errors.add("ciudadEnc", new ActionMessage("ActualizaDatosForm.ciudadEnc"));
          }
          if (GenericValidator.isBlankOrNull(getCalleEnc())) {
            errors.add("calleEnc", new ActionMessage("ActualizaDatosForm.calleEnc"));
          }
          if (GenericValidator.isBlankOrNull(getNumExtEnc())) {
            errors.add("numExtEnc", new ActionMessage("ActualizaDatosForm.numExtEnc"));
          }
          if (GenericValidator.isBlankOrNull(getColoniaEnc())) {
            errors.add("coloniaEnc", new ActionMessage("ActualizaDatosForm.coloniaEnc"));
          }
          if (GenericValidator.isBlankOrNull(getNumTarjetaEnc())) {
            errors.add("numTarjetaEnc", new ActionMessage("ActualizaDatosForm.numTarjetaEnc"));
          }
          if (GenericValidator.isBlankOrNull(getTipoTarjetaEnc())) {
            errors.add("tipoTarjetaEnc", new ActionMessage("ActualizaDatosForm.tipoTarjetaEnc"));
          }
          if (GenericValidator.isBlankOrNull(getVigenciaTarjetaEnc())) {
            errors.add("vigenciaTarjetaEnc", new ActionMessage("ActualizaDatosForm.vigenciaTarjetaEnc"));
          }
          if (GenericValidator.isBlankOrNull(getTerminosEnc())) {
            errors.add("terminosEnc", new ActionMessage("ActualizaDatosForm.terminosEnc"));
          }
          if (GenericValidator.isBlankOrNull(getPlataformaEnc())) {
            errors.add("plataformaEnc", new ActionMessage("ActualizaDatosForm.plataformaEnc"));
          }
          if (GenericValidator.isBlankOrNull(getNumCelularEnc())) {
            errors.add("numCelularEnc", new ActionMessage("ActualizaDatosForm.numCelularEnc"));
          }
          if (GenericValidator.isBlankOrNull(getProveedorEnc())) {
            errors.add("proveedorEnc", new ActionMessage("ActualizaDatosForm.proveedorEnc"));
          }
          if (GenericValidator.isBlankOrNull(getNombreEnc())) {
            errors.add("nombreEnc", new ActionMessage("ActualizaDatosForm.nombreEnc"));
          }
          if (GenericValidator.isBlankOrNull(getApellidoPEnc())) {
            errors.add("apellidoPEnc", new ActionMessage("ActualizaDatosForm.apellidoPEnc"));
          }
          if (GenericValidator.isBlankOrNull(getFechaNacEnc())) {
            errors.add("fechaNacEnc", new ActionMessage("ActualizaDatosForm.fechaNacEnc"));
          }
          if (GenericValidator.isBlankOrNull(getSexoEnc())) {
            errors.add("sexoEnc", new ActionMessage("ActualizaDatosForm.sexoEnc"));
          }
          if (GenericValidator.isBlankOrNull(getEmailEnc())) {
            errors.add("emailEnc", new ActionMessage("ActualizaDatosForm.emailEnc"));
          }
          if (errors.size() == 0)
          {
            setEstado(AesCtr.decrypt(getEstadoEnc(), keyRequest, 256));
            setCiudad(AesCtr.decrypt(getCiudadEnc(), keyRequest, 256));
            setCalle(AesCtr.decrypt(getCalleEnc(), keyRequest, 256));
            setNumExt(AesCtr.decrypt(getNumExtEnc(), keyRequest, 256));
            setNumInt(AesCtr.decrypt(getNumIntEnc(), keyRequest, 256));
            setColonia(AesCtr.decrypt(getColoniaEnc(), keyRequest, 256));
            setCodigoPostal(AesCtr.decrypt(getCodigoPostalEnc(), keyRequest, 256));
            setNumTarjeta(AesCtr.decrypt(getNumTarjetaEnc(), keyRequest, 256));
            setTipoTarjeta(AesCtr.decrypt(getTipoTarjetaEnc(), keyRequest, 256));
            setDomicilioAMEX(AesCtr.decrypt(getDomicilioAMEXEnc(), keyRequest, 256));
            setVigenciaTarjeta(AesCtr.decrypt(getVigenciaTarjetaEnc(), keyRequest, 256));
            setTerminos(AesCtr.decrypt(getTerminosEnc(), keyRequest, 256));
            setPlataforma(AesCtr.decrypt(getPlataformaEnc(), keyRequest, 256));
            setNumCelular(AesCtr.decrypt(getNumCelularEnc(), keyRequest, 256));
            setProveedor(AesCtr.decrypt(getProveedorEnc(), keyRequest, 256));
            setNombre(AesCtr.decrypt(getNombreEnc(), keyRequest, 256));
            setApellidoP(AesCtr.decrypt(getApellidoPEnc(), keyRequest, 256));
            setApellidoM(AesCtr.decrypt(getApellidoMEnc(), keyRequest, 256));
            setFechaNac(AesCtr.decrypt(getFechaNacEnc(), keyRequest, 256));
            setSexo(AesCtr.decrypt(getSexoEnc(), keyRequest, 256));
            setTelefonoCasa(AesCtr.decrypt(getTelefonoCasaEnc(), keyRequest, 256));
            setTelefonoOficina(AesCtr.decrypt(getTelefonoOficinaEnc(), keyRequest, 256));
            setEmail(AesCtr.decrypt(getEmailEnc(), keyRequest, 256));
            if (GenericValidator.isBlankOrNull(getNombreUsuario())) {
              errors.add("nombreUsuarioEnc", new ActionMessage("ActualizaDatosForm.nombreUsuarioEnc"));
            }
            if (GenericValidator.isBlankOrNull(getNumCelular())) {
              errors.add("numCelularEnc", new ActionMessage("ActualizaDatosForm.numCelularEnc"));
            }
            if (GenericValidator.isBlankOrNull(getProveedor())) {
              errors.add("proveedorEnc", new ActionMessage("ActualizaDatosForm.proveedorEnc"));
            }
            if (GenericValidator.isBlankOrNull(getNombre())) {
              errors.add("nombreEnc", new ActionMessage("ActualizaDatosForm.nombreEnc"));
            }
            if (GenericValidator.isBlankOrNull(getApellidoP())) {
              errors.add("apellidoPEnc", new ActionMessage("ActualizaDatosForm.apellidoPEnc"));
            }
            if (GenericValidator.isBlankOrNull(getFechaNac())) {
              errors.add("fechaNacEnc", new ActionMessage("ActualizaDatosForm.fechaNacEnc"));
            }
            if (GenericValidator.isBlankOrNull(getSexo())) {
              errors.add("sexoEnc", new ActionMessage("ActualizaDatosForm.sexoEnc"));
            }
            if (GenericValidator.isBlankOrNull(getEmail())) {
              errors.add("emailEnc", new ActionMessage("ActualizaDatosForm.emailEnc"));
            }
            if (GenericValidator.isBlankOrNull(getEstado())) {
              errors.add("estadoEnc", new ActionMessage("ActualizaDatosForm.estadoEnc"));
            }
            if (GenericValidator.isBlankOrNull(getCiudad())) {
              errors.add("ciudadEnc", new ActionMessage("ActualizaDatosForm.ciudadEnc"));
            }
            if (GenericValidator.isBlankOrNull(getCalle())) {
              errors.add("calleEnc", new ActionMessage("ActualizaDatosForm.calleEnc"));
            }
            if (GenericValidator.isBlankOrNull(getNumExt())) {
              errors.add("numExtEnc", new ActionMessage("ActualizaDatosForm.numExtEnc"));
            }
            if (GenericValidator.isBlankOrNull(getColonia())) {
              errors.add("coloniaEnc", new ActionMessage("ActualizaDatosForm.coloniaEnc"));
            }
            if (GenericValidator.isBlankOrNull(getNumTarjeta())) {
              errors.add("numTarjetaEnc", new ActionMessage("ActualizaDatosForm.numTarjetaEnc"));
            }
            if (GenericValidator.isBlankOrNull(getTipoTarjeta())) {
              errors.add("tipoTarjetaEnc", new ActionMessage("ActualizaDatosForm.tipoTarjetaEnc"));
            }
            if ((getTipoTarjeta() != null) && (getTipoTarjeta().equals("3")) && 
              (GenericValidator.isBlankOrNull(getDomicilioAMEX()))) {
              errors.add("domicilioAMEXEnc", new ActionMessage("ActualizaDatosForm.domicilioAMEXEnc"));
            }
            if ((getTipoTarjeta() != null) && (getTipoTarjeta().equals("3")) && 
              (GenericValidator.isBlankOrNull(getCodigoPostalAMEX()))) {
              errors.add("codigoPostalAMEXEnc", new ActionMessage("ActualizaDatosForm.codigoPostalAMEXEnc"));
            }
            if (GenericValidator.isBlankOrNull(getVigenciaTarjeta())) {
              errors.add("vigenciaTarjetaEnc", new ActionMessage("ActualizaDatosForm.vigenciaTarjetaEnc"));
            }
            if (GenericValidator.isBlankOrNull(getTerminos())) {
              errors.add("terminosEnc", new ActionMessage("ActualizaDatosForm.terminosEnc"));
            }
            if (GenericValidator.isBlankOrNull(getPlataforma())) {
              errors.add("plataformaEnc", new ActionMessage("ActualizaDatosForm.plataformaEnc"));
            }
          }
          if (errors.size() == 0)
          {
            if (!getEstado().matches("^\\d{1,2}$")) {
              errors.add("estadoEnc", new ActionMessage("ActualizaDatosForm.estadoEnc.format"));
            }
            if (!getTipoTarjeta().matches("^\\d{1}$")) {
              errors.add("tipoTarjetaEnc", new ActionMessage("ActualizaDatosForm.tipoTarjetaEnc.format"));
            }
            if ((getTipoTarjeta() != null) && (getTipoTarjeta().equals("3")))
            {
              if (!getNumTarjeta().matches("^\\d{15}$")) {
                errors.add("numTarjetaEnc", new ActionMessage("ActualizaDatosForm.numTarjetaEnc.format"));
              }
            }
            else if (!getNumTarjeta().matches("^\\d{16}$")) {
              errors.add("numTarjetaEnc", new ActionMessage("ActualizaDatosForm.numTarjetaEnc.format"));
            }
            if (!getVigenciaTarjeta().matches("^\\d{2}/\\d{2}$")) {
              errors.add("vigenciaTarjetaEnc", new ActionMessage("ActualizaDatosForm.vigenciaTarjetaEnc.format"));
            }
            if (!getTerminos().matches("^[S]$")) {
              errors.add("terminosEnc", new ActionMessage("ActualizaDatosForm.terminosEnc.format"));
            }
            if (!getNombreUsuario().matches("^[0-9a-zA-Z_.-]{0,35}$")) {
              errors.add("nombreUsuarioEnc", new ActionMessage("ActualizaDatosForm.nombreUsuarioEnc.format"));
            }
            if (!getNumCelular().matches("^\\d{10}$")) {
              errors.add("numCelularEnc", new ActionMessage("ActualizaDatosForm.numCelularEnc.format"));
            }
            if (!getProveedor().matches("^\\d$")) {
              errors.add("proveedorEnc", new ActionMessage("ActualizaDatosForm.proveedorEnc.format"));
            }
            if (!getNombre().matches("^([a-zA-Z ÑÁÉÍÓÚñáéíóú]{2,35})$")) {
              errors.add("nombreEnc", new ActionMessage("ActualizaDatosForm.nombreEnc.format"));
            }
            if (!getApellidoP().matches("^([a-zA-Z ÑÁÉÍÓÚñáéíóú]{2,45})$")) {
              errors.add("apellidoP", new ActionMessage("ActualizaDatosForm.apellidoPEnc.format"));
            }
            if ((!GenericValidator.isBlankOrNull(getApellidoM())) && (!getApellidoM().matches("^([a-zA-Z ÑÁÉÍÓÚñáéíóú]{2,45})$"))) {
              errors.add("apellidoM", new ActionMessage("ActualizaDatosForm.apellidoMEnc.format"));
            }
            if (!getFechaNac().matches("^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$")) {
              errors.add("fechaNac", new ActionMessage("ActualizaDatosForm.fechaNac.format"));
            }
            if (!getSexo().matches("^[FM]{1}$")) {
              errors.add("sexoEnc", new ActionMessage("ActualizaDatosForm.sexoEnc.format"));
            }
            if (!GenericValidator.isEmail(getEmail())) {
              errors.add("sexoEnc", new ActionMessage("ActualizaDatosForm.emailEnc.format"));
            }
          }
          if (errors.size() != 0)
          {
            request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
            StringBuilder sb = new StringBuilder();
            Iterator<ActionMessage> iter = errors.get();
            PropertyMessageResources p = (PropertyMessageResources)request.getAttribute("org.apache.struts.action.MESSAGE");
            while (iter.hasNext())
            {
              ActionMessage msg = (ActionMessage)iter.next();
              sb.append(StrutsMensajes.getErrorMessage(p, msg));
              sb.append("<br/>");
            }
            this.log.info(sb);
            this.log.info(toString());
            request.setAttribute("errorMensaje", AesCtr.encrypt(sb.toString(), keyResponse, 256));
          }
        }
      }
    }
    catch (Exception e)
    {
      this.log.error("Exeption en validacion ActualizaDatosForm : ", e);
      if (request.getAttribute("llavePublica") != null) {
        request.removeAttribute("llavePublica");
      }
      request.setAttribute("errorMensaje", "Error inesperado en la validaci&oacute;n");
    }
    return errors;
  }
}


/* Location:              /home/rhtrejo/workspace/RecargaIAVE/RecargaIAVE.war!/WEB-INF/classes/mx/addcel/ServiceIAVE/form/ActualizaDatosForm.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */