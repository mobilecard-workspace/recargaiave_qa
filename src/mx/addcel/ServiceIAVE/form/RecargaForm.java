package mx.addcel.ServiceIAVE.form;

import java.util.Iterator;
import javacryption.aes.AesCtr;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import mx.addcel.ServiceIAVE.service.utils.StrutsMensajes;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.util.PropertyMessageResources;

public class RecargaForm
  extends MyActionForm
{
  private Logger log = Logger.getLogger(RecargaForm.class);
  private String tagAliasEnc;
  private String tagEnc;
  private String montoEnc;
  private String passMobileCardEnc;
  private String cvv2Enc;
  private String plataformaEnc;
  private String tagAlias;
  private String tag;
  private String monto;
  private String passMobileCard;
  private String cvv2;
  private String plataforma;
  
  public void reset(ActionMapping mapping, HttpServletRequest request)
  {
    setMontoEnc(null);
    setPassMobileCardEnc(null);
    setCvv2Enc(null);
    setTagAliasEnc(null);
    setTagEnc(null);
    setMonto(null);
    setPassMobileCard(null);
    setCvv2(null);
    setTagAlias(null);
    setTag(null);
    setPlataformaEnc(null);
    setPlataforma(null);
  }
  
  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request)
  {
    ActionErrors errors = new ActionErrors();
    String keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
    String keyRequest = (String)request.getSession().getAttribute("jCryptionKey_");
    String nextPage = (String)request.getAttribute("nextPage");
    try
    {
      if ((nextPage == null) || (!nextPage.equals("goToRecargaIAVE"))) {
        if ((GenericValidator.isBlankOrNull(keyResponse)) || (GenericValidator.isBlankOrNull(keyRequest)))
        {
          errors.add(null, new ActionMessage("Application.llaveInexistente"));
        }
        else
        {
          if (GenericValidator.isBlankOrNull(getTagAliasEnc())) {
            errors.add("tagAliasEnc", new ActionMessage("RecargaAction.tagAliasEnc"));
          }
          if (GenericValidator.isBlankOrNull(getTagEnc())) {
            errors.add("tagEnc", new ActionMessage("RecargaAction.tagEnc"));
          }
          if (GenericValidator.isBlankOrNull(getMontoEnc())) {
            errors.add("montoEnc", new ActionMessage("RecargaAction.montoEnc"));
          }
          if (GenericValidator.isBlankOrNull(getPassMobileCardEnc())) {
            errors.add("passMobileCardEnc", new ActionMessage("RecargaAction.passMobileCardEnc"));
          }/*
          if (GenericValidator.isBlankOrNull(getCvv2Enc())) {
            errors.add("cvv2Enc", new ActionMessage("RecargaAction.cvv2Enc"));
          }*/
          this.log.info("Termino validacion 1");
          if (errors.size() == 0)
          {
            setTag(AesCtr.decrypt(getTagEnc(), keyRequest, 256));
            setTagAlias(AesCtr.decrypt(getTagAliasEnc(), keyRequest, 256));
            setMonto(AesCtr.decrypt(getMontoEnc(), keyRequest, 256));
            //setCvv2(AesCtr.decrypt(getCvv2Enc(), keyRequest, 256));
            setPassMobileCard(AesCtr.decrypt(getPassMobileCardEnc(), keyRequest, 256));
            setPlataforma(AesCtr.decrypt(getPlataformaEnc(), keyRequest, 256));
            this.log.info("Inicio validacion 2");
            if (GenericValidator.isBlankOrNull(getTagAlias())) {
              errors.add("tagAliasEnc", new ActionMessage("RecargaAction.tagAliasEnc"));
            }
            if (GenericValidator.isBlankOrNull(getTag())) {
              errors.add("tagEnc", new ActionMessage("RecargaAction.tagEnc"));
            }
            if (GenericValidator.isBlankOrNull(getMonto())) {
              errors.add("montoEnc", new ActionMessage("RecargaAction.montoEnc"));
            }
            if (GenericValidator.isBlankOrNull(getPassMobileCard())) {
              errors.add("passMobileCardEnc", new ActionMessage("RecargaAction.passMobileCardEnc"));
            }/*
            if (GenericValidator.isBlankOrNull(getCvv2())) {
              errors.add("cvv2Enc", new ActionMessage("RecargaAction.cvv2Enc"));
            }*/
            if (errors.size() == 0)
            {
              if (!getTag().matches("^\\d+$")) {
                errors.add("tagEnc", new ActionMessage("RecargaAction.tagEnc.format"));
              }
              if (!getMonto().matches("^\\d{3}$")) {
                errors.add("montoEnc", new ActionMessage("RecargaAction.montoEnc.format"));
              }/*
              if (!getCvv2().matches("^\\d{3,4}$")) {
                errors.add("cvv2Enc", new ActionMessage("RecargaAction.cvv2Enc.format"));
              }*/
            }
          }
          if (errors.size() != 0)
          {
            request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
            StringBuilder sb = new StringBuilder();
            Iterator<ActionMessage> iter = errors.get();
            PropertyMessageResources p = (PropertyMessageResources)request.getAttribute("org.apache.struts.action.MESSAGE");
            while (iter.hasNext())
            {
              ActionMessage msg = (ActionMessage)iter.next();
              sb.append(StrutsMensajes.getErrorMessage(p, msg));
              sb.append("<br/>");
            }
            this.log.info(sb);
            this.log.info(toString());
            request.setAttribute("errorMensaje", AesCtr.encrypt(sb.toString(), keyResponse, 256));
          }
        }
      }
    }
    catch (Exception e)
    {
      this.log.error("Exeption en validacion CargaIAVEForm : ", e);
      if (request.getAttribute("llavePublica") != null) {
        request.removeAttribute("llavePublica");
      }
      request.setAttribute("errorMensaje", "Error inesperado en la validaci&oacute;n");
    }
    return errors;
  }
  
  public String getTagAliasEnc()
  {
    return this.tagAliasEnc;
  }
  
  public void setTagAliasEnc(String tagAliasEnc)
  {
    this.tagAliasEnc = tagAliasEnc;
  }
  
  public String getTagEnc()
  {
    return this.tagEnc;
  }
  
  public void setTagEnc(String tagEnc)
  {
    this.tagEnc = tagEnc;
  }
  
  public String getMontoEnc()
  {
    return this.montoEnc;
  }
  
  public void setMontoEnc(String montoEnc)
  {
    this.montoEnc = montoEnc;
  }
  
  public String getPassMobileCardEnc()
  {
    return this.passMobileCardEnc;
  }
  
  public void setPassMobileCardEnc(String passMobileCardEnc)
  {
    this.passMobileCardEnc = passMobileCardEnc;
  }
  
  public String getCvv2Enc()
  {
    return this.cvv2Enc;
  }
  
  public void setCvv2Enc(String cvv2Enc)
  {
    this.cvv2Enc = cvv2Enc;
  }
  
  public String getPlataformaEnc()
  {
    return this.plataformaEnc;
  }
  
  public void setPlataformaEnc(String plataformaEnc)
  {
    this.plataformaEnc = plataformaEnc;
  }
  
  public String getTagAlias()
  {
    return this.tagAlias;
  }
  
  public void setTagAlias(String tagAlias)
  {
    this.tagAlias = tagAlias;
  }
  
  public String getTag()
  {
    return this.tag;
  }
  
  public void setTag(String tag)
  {
    this.tag = tag;
  }
  
  public String getMonto()
  {
    return this.monto;
  }
  
  public void setMonto(String monto)
  {
    this.monto = monto;
  }
  
  public String getPassMobileCard()
  {
    return this.passMobileCard;
  }
  
  public void setPassMobileCard(String passMobileCard)
  {
    this.passMobileCard = passMobileCard;
  }
  
  public String getCvv2()
  {
    return this.cvv2;
  }
  
  public void setCvv2(String cvv2)
  {
    this.cvv2 = cvv2;
  }
  
  public String getPlataforma()
  {
    return this.plataforma;
  }
  
  public void setPlataforma(String plataforma)
  {
    this.plataforma = plataforma;
  }
}