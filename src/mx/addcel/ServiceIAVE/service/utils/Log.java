package mx.addcel.ServiceIAVE.service.utils;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.log4j.PropertyConfigurator;

public class Log
  implements ServletContextListener
{
  public void contextInitialized(ServletContextEvent arg0)
  {
    String file = "/WEB-INF/log4j.properties";
    PropertyConfigurator.configure(file);
  }
  
  public void contextDestroyed(ServletContextEvent arg0) {}
}


/* Location:              /home/rhtrejo/workspace/RecargaIAVE/RecargaIAVE.war!/WEB-INF/classes/mx/addcel/ServiceIAVE/service/utils/Log.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */