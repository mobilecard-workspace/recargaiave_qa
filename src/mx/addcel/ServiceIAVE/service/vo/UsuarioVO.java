package mx.addcel.ServiceIAVE.service.vo;

public class UsuarioVO extends AbstractVO {
	
	long idUsuario;
	private String nombreUsuario;
	private String numCelular;
	private String proveedor;
	private String nombre;
	private String apellidoP;
	private String apellidoM;
	private String fechaNac;
	private String sexo;
	private String telefonoCasa;
	private String telefonoOficina;
	private String email;
	private String estado;
	private String ciudad;
	private String calle;
	private String numExt;
	private String numInt;
	private String colonia;
	private String codigoPostal;
	private String numTarjeta;
	private String tipoTarjeta;
	private String codigoPostalAMEX;
	private String domicilioAMEX;
	private String vigenciaTarjeta;
	private String terminos;
	private String plataforma;
	private String monto;
	private String passMobileCard;
	private String cvv2;

	public String getNombreUsuario() {
		return this.nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getNumCelular() {
		return this.numCelular;
	}

	public void setNumCelular(String numCelular) {
		this.numCelular = numCelular;
	}

	public String getProveedor() {
		return this.proveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoP() {
		return this.apellidoP;
	}

	public void setApellidoP(String apellidoP) {
		this.apellidoP = apellidoP;
	}

	public String getApellidoM() {
		return this.apellidoM;
	}

	public void setApellidoM(String apellidoM) {
		this.apellidoM = apellidoM;
	}

	public String getFechaNac() {
		return this.fechaNac;
	}

	public void setFechaNac(String fechaNac) {
		this.fechaNac = fechaNac;
	}

	public String getSexo() {
		return this.sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getTelefonoCasa() {
		return this.telefonoCasa;
	}

	public void setTelefonoCasa(String telefonoCasa) {
		this.telefonoCasa = telefonoCasa;
	}

	public String getTelefonoOficina() {
		return this.telefonoOficina;
	}

	public void setTelefonoOficina(String telefonoOficina) {
		this.telefonoOficina = telefonoOficina;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCiudad() {
		return this.ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getCalle() {
		return this.calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNumExt() {
		return this.numExt;
	}

	public void setNumExt(String numExt) {
		this.numExt = numExt;
	}

	public String getNumInt() {
		return this.numInt;
	}

	public void setNumInt(String numInt) {
		this.numInt = numInt;
	}

	public String getColonia() {
		return this.colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getCodigoPostal() {
		return this.codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getNumTarjeta() {
		return this.numTarjeta;
	}

	public void setNumTarjeta(String numTarjeta) {
		this.numTarjeta = numTarjeta;
	}

	public String getTipoTarjeta() {
		return this.tipoTarjeta;
	}

	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	public String getCodigoPostalAMEX() {
		return this.codigoPostalAMEX;
	}

	public void setCodigoPostalAMEX(String codigoPostalAMEX) {
		this.codigoPostalAMEX = codigoPostalAMEX;
	}

	public String getDomicilioAMEX() {
		return this.domicilioAMEX;
	}

	public void setDomicilioAMEX(String domicilioAMEX) {
		this.domicilioAMEX = domicilioAMEX;
	}

	public String getVigenciaTarjeta() {
		return this.vigenciaTarjeta;
	}

	public void setVigenciaTarjeta(String vigenciaTarjeta) {
		this.vigenciaTarjeta = vigenciaTarjeta;
	}

	public String getTerminos() {
		return this.terminos;
	}

	public void setTerminos(String terminos) {
		this.terminos = terminos;
	}

	public String getPlataforma() {
		return this.plataforma;
	}

	public void setPlataforma(String plataforma) {
		this.plataforma = plataforma;
	}

	public String getMonto() {
		return this.monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public String getPassMobileCard() {
		return this.passMobileCard;
	}

	public void setPassMobileCard(String passMobileCard) {
		this.passMobileCard = passMobileCard;
	}

	public String getCvv2() {
		return this.cvv2;
	}

	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}

	/**
	 * @return the idUsuario
	 */
	public long getIdUsuario() {
		return idUsuario;
	}

	/**
	 * @param idUsuario the idUsuario to set
	 */
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	
}