package mx.addcel.ServiceIAVE.service.vo;

public class Producto
{
  private String clave;
  private String claveWS;
  private String proveedor;
  private String descripcion;
  private String monto;
  private String path;
  private String nombre;
  
  public String getNombre()
  {
    return this.nombre;
  }
  
  public void setNombre(String nombre)
  {
    this.nombre = nombre;
  }
  
  public String getClave()
  {
    return this.clave;
  }
  
  public void setClave(String clave)
  {
    this.clave = clave;
  }
  
  public String getClaveWS()
  {
    return this.claveWS;
  }
  
  public void setClaveWS(String claveWS)
  {
    this.claveWS = claveWS;
  }
  
  public String getProveedor()
  {
    return this.proveedor;
  }
  
  public void setProveedor(String proveedor)
  {
    this.proveedor = proveedor;
  }
  
  public String getDescripcion()
  {
    return this.descripcion;
  }
  
  public void setDescripcion(String descripcion)
  {
    this.descripcion = descripcion;
  }
  
  public String getMonto()
  {
    return this.monto;
  }
  
  public void setMonto(String monto)
  {
    this.monto = monto;
  }
  
  public String getPath()
  {
    return this.path;
  }
  
  public void setPath(String path)
  {
    this.path = path;
  }
}


/* Location:              /home/rhtrejo/workspace/RecargaIAVE/RecargaIAVE.war!/WEB-INF/classes/mx/addcel/ServiceIAVE/service/vo/Producto.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */