package mx.addcel.test;

import com.google.gson.Gson;
import com.ironbit.mc.system.crypto.Crypto;

import javacryption.hash.SHA;
import mx.addcel.ServiceIAVE.service.utils.Constantes;
import mx.addcel.ServiceIAVE.service.utils.Utils;
import mx.addcel.ServiceIAVE.service.vo.TransactionProcomVO;

public class Test{
	 

	public static void main(String[] args) {
		/*
		ApplicationContext cxt = new ClassPathXmlApplicationContext("mx/addcel/test/applicationContext.xml");
        ConsultasService consultasService = (ConsultasService) cxt.getBean("consultasService");		
		*/
		
		System.out.println(Crypto.aesEncrypt(Utils.parsePass("5525963513"), "1234567890123456"));	
		System.out.println(Crypto.aesDecrypt(Utils.parsePass("5525963513"), "RMtsi3EabFIRv27reCTXbQ=="));
		System.out.println(Crypto.aesDecrypt(Utils.parsePass("5525963513"), "0k3WM0FF1JBlbu/cTsQGaw=="));
		System.out.println(SHA.sha1("12345678"));
		
		System.out.println(SHA.sha1(Constantes.MERCHANT + Constantes.STORE + Constantes.TERM + 100 + Constantes.CURRENCY + 1289603));
			
		
		//pruebas SHA1 request PROSA
		//803915912340011004841289603
		String cadProsa = Constantes.MERCHANT + Constantes.STORE + Constantes.TERM + "100" + Constantes.CURRENCY + "155587";
		System.out.println(cadProsa);
		System.out.println(SHA.sha1(cadProsa));
		
		/*
		//test consultas
		String comision = consultasService.obtenComision("IAVE_TuTag");
		String montoPesos = consultasService.obtenMontoRecarga("951");
		montoPesos = (Double.parseDouble(montoPesos) + Double.parseDouble(comision))+"";
		System.out.println("monto+comision=" + montoPesos);
		*/
		
		//test gson
		TransactionProcomVO transactionProcomVO = new TransactionProcomVO();
		TransactionProcomVO fromJsonTpVO; 
		
		transactionProcomVO.setEmDigest("c9f6d49026493ec4f8ab3b33a24894524182c976");
		transactionProcomVO.setEmOrderID("155590");
		transactionProcomVO.setEmTerm("001");
		transactionProcomVO.setEmMerchant("8039159");
		transactionProcomVO.setEmResponse("denied");
		transactionProcomVO.setEmStore("1234");
		transactionProcomVO.setEmRefNum("000000000043");
		transactionProcomVO.setEmAuth("000000");
		transactionProcomVO.setEmTotal("100");

		Gson gson = new Gson();		
		String json = gson.toJson(transactionProcomVO);		
		System.out.println(json);
		
		fromJsonTpVO = gson.fromJson(json, TransactionProcomVO.class);
		System.out.println(fromJsonTpVO.getEmOrderID());

	}

}
