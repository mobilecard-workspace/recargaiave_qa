var confirmacion;
function unMaskForm(){
    datosAdicionales.setMasked(false);
}
Ext.application({
    launch: function() {
		Ext.override(Ext.util.SizeMonitor, {
			constructor: function(config) {
			  var namespace = Ext.util.sizemonitor;

			  if (Ext.browser.is.Firefox) {
				return new namespace.OverflowChange(config);
			  } else if (Ext.browser.is.WebKit) {
				if (!Ext.browser.is.Silk && Ext.browser.engineVersion.gtEq('535') && !Ext.browser.engineVersion.ltEq('537.36')) {
				  return new namespace.OverflowChange(config);
				} else {
				  return new namespace.Scroll(config);
				}
			  } else if (Ext.browser.is.IE11) {
				return new namespace.Scroll(config);
			  } else {
				return new namespace.Scroll(config);
			  }
			}
		});
		Ext.override(Ext.util.PaintMonitor, {
			constructor: function(config) {
			  if (Ext.browser.is.Firefox || (Ext.browser.is.WebKit && Ext.browser.engineVersion.gtEq('536') && !Ext.browser.engineVersion.ltEq('537.36') && !Ext.os.is.Blackberry)) {
				return new Ext.util.paintmonitor.OverflowChange(config);
			  } else {
				return new Ext.util.paintmonitor.CssAnimation(config);
			  }
			}
		});
		
        confirmacion = Ext.create('Ext.form.Panel', {
            fullscreen: true,
            itemId: 'confirmacionPago',
            scrollable: 'vertical',
            items: [{
                    xtype: 'titlebar',
                    docked: 'top',
                    title: 'Recarga exitosa',
                    style: 'background:#09569b',
                    items: [{
                            iconCls:"delete",
                            iconMask:!0,
                            ui:"plain",
                            id: 'btnCancelar',
                            handler: function(btn, evt) {
                                location.href = "http://www.mobilecard.mx:8080/RecargaIAVE_test/close.html";
                            }
                        }]
                }, {
                    xtype: 'fieldset',
                    items: [
                        {
                            xtype: 'textfield',
                            label: 'Alias Tag',
                            labelAlign: 'top',
                            value: aliasTag,
                            readOnly: true
                        },
                        {
                            xtype: 'textfield',
                            label: 'Tag',
                            labelAlign: 'top',
                            readOnly: true,
                            value: tag
                        },
                        {
                            xtype: 'textfield',
                            label: 'Monto de recarga',
                            labelAlign: 'top',
                            readOnly: true,
                            value: monto
                        },
                        {
                            xtype: 'textfield',
                            label: 'Mensaje',
                            labelAlign: 'top',
                            readOnly: true,
                            value: mensageRecarga
                        },
                        {
                            xtype: 'textfield',
                            label: 'Autorizaci&oacute;n',
                            labelAlign: 'top',
                            readOnly: true,
                            value: autorizacion
                        }, {
                            xtype: 'button',
                            text: 'Finalizar',
                            handler: function(btn, evt) {                            	
                            	location.href = "http://www.mobilecard.mx:8080/RecargaIAVE_test/end.html";
                            } // handler
                        }
                    ]}, {
                    xtype: 'toolbar',
                    docked: 'bottom',
                    styleHtmlContent: true,
                    title: '<div style="margin:-10px 0px 0px 0px;font-size: 10px; color: white;">Powered by Mobilecard &reg;</div>',
                    minHeight: '1.3em',
                    height: '1.3em',
                    style: 'background:#09569b'
                }]
        }); // create()
    } // launch
}); // application()

