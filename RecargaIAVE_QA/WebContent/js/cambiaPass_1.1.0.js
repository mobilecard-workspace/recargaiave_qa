var recuperaForm;

function unMaskForm(){
    recuperaForm.setMasked(false);
    actualizaDatosForm.doComponentLayout();
}
Ext.application({
    launch: function() {
           Ext.applyIf(Ext.data.Validations, {
            passValidation: function(config, value) {
                if (arguments.length === 1) {
                    value = config;
                }
                return value === Ext.getCmp("passConfirm").getValue();
            },
            presenceMejora:function(config, value) {
                if (arguments.length === 1) {
                    value = config;
                }
                if(value.trim().length===0){
                    return false;
                }else{
                    return true;
                }
            }
        });
        Ext.define('recuperaModel', {
            extend: 'Ext.data.Model',
            config: {
                fields: [{
                        name: 'login',
                        type: 'string'
                    }, {
                        name: 'pass',
                        type: 'string'
                    }, {
                        name: 'nuevoPass',
                        type: 'string'
                    }, {
                        name: 'passConfirm',
                        type: 'string'
                    }], // fields
                validations: [{
                        field: 'login',
                        type: 'presenceMejora',
                        message: 'El login es requerido'
                    }, {
                        field: 'pass',
                        type: 'presenceMejora',
                        message: 'El password actual es requerido'
                    }, {
                        field: 'pass',
                        type: 'length',
                        min: 8,
                        max: 13,
                        message: 'Password actual no es valido.'
                    }, {
                        field: 'nuevoPass',
                        type: 'presenceMejora',
                        message: 'El password nuevo es requerido'
                    }, {
                        field: 'nuevoPass',
                        type: 'length',
                        min: 8,
                        max: 13,
                        message: 'Password nuevo no es valido.'
                    }, {
                        field: 'nuevoPass',
                        type: 'passValidation',
                        message: 'El password nuevo no coincide con la confirmaci&oacue;n'
                    }
                ] // validations
            } // config
        }); // define()
        recuperaForm = Ext.create('Ext.form.FormPanel', {
            fullscreen: true,
            itemId: 'loginUsuario',
            scrollable: 'vertical',
            standardSubmit: true,
            masked: {
                xtype: 'loadmask',
                message: 'Dando seguridad al canal... ' +
                        '<hr>Espere por favor...'
            },
            instructions: '<code>(*)</code> REQUERIDO',
            items: [{
                    xtype: 'titlebar',
                    title: 'Cambia Password',
                    docked: 'top',
                    style: 'background:#09569b',
                            items: [{
                                    iconCls:"delete",
                                    iconMask:!0,
                                    ui:"plain",
                                    id: 'btnCancelar',
                                    handler: function(btn, evt) {
                                        location.href = "http://www.mobilecard.mx:8080/RecargaIAVE_test/close.html";
                                    }    
                                } 
                            ]
                }, {
                    xtype: 'fieldset',
                    items: [{
                            xtype: 'textfield',
                            name: 'login',
                            id: 'login',
                            labelAlign: 'top',
                            label: 'Login:',
                            required: true,
                            value: login,
                            readOnly: true
                        }, {
                            xtype: 'passwordfield',
                            name: 'pass',
                            id: 'pass',
                            labelAlign: 'top',
                            label: 'Password Actual:',
                            required: true
                        }, {
                            xtype: 'passwordfield',
                            name: 'nuevoPass',
                            id: 'nuevoPass',
                            labelAlign: 'top',
                            label: 'Password nuevo:',
                            required: true
                        }, {
                            xtype: 'passwordfield',
                            name: 'passConfirm',
                            id: 'passConfirm',
                            labelAlign: 'top',
                            label: 'Password confirmaci&oacute;n:',
                            required: true
                        }] // items
                }, {
                    xtype: 'toolbar',
                    layout: {
                        pack: 'center'
                    }, // layout
                    ui: 'plain',
                    items: [{
                            xtype: 'button',
                            text: 'Cambiar password',
                            id: 'btnCambiar',
                            width: '90%',
                            handler: function(btn, evt) {
                                Ext.getCmp("btnCambiar").disable();
                                Ext.getCmp("btnCancelar").disable();
                                var recuperaModel = Ext.create('recuperaModel');
                                var errors, errorMessage = '';
                                recuperaForm.updateRecord(recuperaModel);
                                errors = recuperaModel.validate();
                                if (!errors.isValid()) {
                                    errors.each(function(err) {
                                        errorMessage += err.getMessage() + '<br/>';
                                    }); // each()
                                    Ext.Msg.alert('Datos incorrectos', errorMessage);
                                    Ext.getCmp("btnCambiar").enable();
                                    Ext.getCmp("btnCancelar").enable();
                                } else {
                                    recuperaEnc.setValues({loginEnc: $.jCryption.encrypt(Ext.getCmp("login").getValue(), password_),
                                        passwordActualEnc: $.jCryption.encrypt(Ext.getCmp("pass").getValue(), password_),
                                        passwordNuevoEnc: $.jCryption.encrypt(Ext.getCmp("nuevoPass").getValue(), password_)
                                    });
                                    recuperaEnc.submit({url: 'actualizacionPass.do?method=cambiaPassword', method: 'POST'});
                                } // if
                            } // handler
                        }] // items (toolbar)
                }, {
                            xtype: 'titlebar',
                            docked: 'bottom',
                            styleHtmlContent: true,
                            title: '<div style="margin:-10px 0px 0px 0px;font-size: 10px; color: white;">Powered by Mobilecard &reg;</div>',
                            //html: '<font style="padding:-10px 0px 0px 0px;font-size:10px;color:white"><center>Powered by Mobilecard</center></font>',
                            minHeight: '1.3em',
                            height: '1.3em',
                            style: 'background:#09569b'
                        }]
        }); // create()
        var recuperaEnc = Ext.create('Ext.form.FormPanel', {
            standardSubmit: true,
            items: [
                {
                    xtype: 'hiddenfield',
                    name: 'loginEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'passwordActualEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'passwordNuevoEnc'
                }] // items (formpanel)
        }); // create()
        if (typeof error !== 'undefined') {
            Ext.Msg.alert(titulo, error);
        }
        if (typeof mensajeCambioPass !== "undefined") {
            Ext.Msg.alert("Cambio de password", mensajeCambioPass);
        }
    } // launch
}); // application()
