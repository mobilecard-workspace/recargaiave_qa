var loginForm;
function unMaskForm() {
    loginForm.setMasked(false);
}
Ext.application({
    launch: function() {
        Ext.applyIf(Ext.data.Validations, {
            presenceMejora: function(config, value) {
                if (arguments.length === 1) {
                    value = config;
                }
                if (value.trim().length === 0) {
                    return false;
                } else {
                    return true;
                }
            }
        });
        Ext.define('loginModel', {
            extend: 'Ext.data.Model',
            config: {
                fields: [{
                        name: 'login',
                        type: 'string'
                    }, {
                        name: 'pass',
                        type: 'string'
                    }, {
                        name: 'flujo',
                        type: 'string'
                    }], // fields
                validations: [{
                        field: 'login',
                        type: 'presenceMejora',
                        message: 'El login es requerido'
                    }, {
                        field: 'pass',
                        type: 'presenceMejora',
                        message: 'El password es requerido'
                    }, {
                        field: 'pass',
                        type: 'length',
                        min: 8,
                        max: 13,
                        message: 'Password invalido.'
                    }
                ] // validations
            } // config
        }); // define()
        loginForm = Ext.create('Ext.form.FormPanel', {
            fullscreen: true,
            itemId: 'loginUsuario',
            scrollable: 'vertical',
            standardSubmit: true,
            masked: {
                xtype: 'loadmask',
                message: 'Dando seguridad al canal... ' +
                        '<hr>Espere por favor...'
            },
            instructions: '<code>(*)</code> REQUERIDO',
            layout:'vbox',
            items: [{
                    xtype: 'titlebar',
                    title: 'Login MobileCard',
                    docked: 'top',
                    style: 'background:#09569b',
                    flex: 0,
                    items: [{
                            iconCls: "delete",
                            iconMask: !0,
                            ui: "plain",
                            id: 'btnCancelar',
                            handler: function(btn, evt) {
                                location.href = "http://www.mobilecard.mx:8080/RecargaIAVE_test/close.html";
                            }
                        }
                    ]
                }, {
                    xtype: 'fieldset',
                    flex: 1,
                    items: [{
                            xtype: 'textfield',
                            name: 'login',
                            id: 'login',
                            label: 'Login:',
                            required: true,
                            value: login
                        }, {
                            xtype: 'passwordfield',
                            name: 'pass',
                            id: 'pass',
                            label: 'Password:',
                            required: true
                        }, {
                            xtype: 'hiddenfield',
                            id: 'flujo',
                            name: 'flujo',
                            value: flujo
                        }] // items
                }, {
                    xtype: 'toolbar',
                    layout: {
                        pack: 'center'
                    }, // layout
                    ui: 'plain',
                    items: [{
                            xtype: 'button',
                            text: 'Enviar',
                            id: 'btnEnviar',
                            width: '45%',
                            handler: function(btn, evt) {
                                Ext.getCmp("btnEnviar").disable();
                                Ext.getCmp("btnCancelar").disable();
                                Ext.getCmp("btnRecuperar").disable();
                                var loginModel = Ext.create('loginModel');
                                var errors, errorMessage = '';
                                loginForm.updateRecord(loginModel);
                                errors = loginModel.validate();
                                if (!errors.isValid()) {
                                    errors.each(function(err) {
                                        errorMessage += err.getMessage() + '<br/>';
                                    }); // each()
                                    Ext.Msg.alert('Datos incorrectos', errorMessage);
                                    Ext.getCmp("btnRecuperar").enable();
                                    Ext.getCmp("btnEnviar").enable();
                                    Ext.getCmp("btnCancelar").enable();
                                } else {
                                    loginEnc.setValues({loginEnc: $.jCryption.encrypt(Ext.getCmp("login").getValue(), password_),
                                        passwordEnc: $.jCryption.encrypt(Ext.getCmp("pass").getValue(), password_),
                                        flujoEnc: $.jCryption.encrypt(Ext.getCmp("flujo").getValue(), password_)});
                                    loginEnc.submit({url: 'login.do?method=loginUsuario', method: 'POST'});
                                } // if
                            } // handler
                        }, {
                            xtype: 'button',
                            text: 'Recuperar Password',
                            width: '45%',
                            id: 'btnRecuperar',
                            handler: function(btn, evt) {
                                Ext.getCmp("btnRecuperar").disable();
                                Ext.getCmp("btnCancelar").disable();
                                Ext.getCmp("btnEnviar").disable();
                                if (Ext.getCmp("login").getValue().trim().length !== 0) {
                                    Ext.Msg.confirm('Confirmar', 'Desea recuperar su password?', function(btn) {
                                        if (btn === 'yes') {
                                            Ext.Viewport.mask({xtype: 'loadmask', message: 'Validando nombre de usuario'});
                                            Ext.Ajax.request({
                                                url: 'consultasAjax.do?method=recuperaPass',
                                                method: 'POST',
                                                params: {login: $.jCryption.encrypt(Ext.getCmp("login").getValue(), password_)},
                                                success: function(xhr) {
                                                    var data = Ext.JSON.decode($.jCryption.decrypt(xhr.responseText, password));
                                                    if (data.error === '0') {
                                                        if (data.valido === '0') {
                                                            Ext.Msg.alert("Login no existe", "El nombre de usuario no se encontro");
                                                            Ext.getCmp("btnRecuperar").enable();
                                                            Ext.getCmp("btnCancelar").enable();
                                                            Ext.getCmp("btnEnviar").enable();
                                                        } else {
                                                            Ext.Msg.alert("Se reseteo su password", "Se envio por correo su nuevo password, es necesario que lo cambie", function(btn) {
                                                                Ext.getCmp("btnRecuperar").disable();
                                                                Ext.getCmp("btnEnviar").enable();
                                                                location.href = "welcome.do?method=cambiaPass";
                                                            });
                                                        }
                                                    } else {
                                                        Ext.Msg.alert("Error de comunicaci&oacute;n", "No fue posible validar el nombre de usuario");
                                                        Ext.getCmp("btnRecuperar").enable();
                                                    }
                                                    Ext.Viewport.unmask();
                                                    Ext.getCmp("btnCancelar").enable();
                                                },
                                                failure: function(response, opts) {
                                                    Ext.Msg.alert("Error de comunicaci&oacute;n", "No fue posible validar el nombre de usuario");
                                                    Ext.Viewport.unmask();
                                                    Ext.getCmp("btnRecuperar").enable();
                                                    Ext.getCmp("btnCancelar").enable();
                                                    Ext.getCmp("btnEnviar").enable();
                                                }
                                            });
                                        } else {
                                            Ext.getCmp("btnRecuperar").enable();
                                            Ext.getCmp("btnCancelar").enable();
                                            Ext.getCmp("btnEnviar").enable();
                                        }
                                    });
                                } else {
                                    Ext.Msg.alert("Login no valido", "El login no es valido, ingrese su login por favor");
                                    Ext.getCmp("btnRecuperar").enable();
                                    Ext.getCmp("btnCancelar").enable();
                                    Ext.getCmp("btnEnviar").enable();
                                }

                            }
                        }] // items (toolbar)
                }, {
                    xtype: 'titlebar',
                    docked: 'bottom',
                    styleHtmlContent: true,
                    flex: 0,
                    title: '<div style="margin:-10px 0px 0px 0px;font-size: 10px; color: white;">Powered by Mobilecard &reg;</div>',
                    //html: '<font style="padding:-10px 0px 0px 0px;font-size:10px;color:white"><center>Powered by Mobilecard</center></font>',
                    minHeight: '1.3em',
                    height: '1.3em',
                    style: 'background:#09569b'
                }]
        }); // create()
        var loginEnc = Ext.create('Ext.form.FormPanel', {
            standardSubmit: true,
            items: [
                {
                    xtype: 'hiddenfield',
                    name: 'loginEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'passwordEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'flujoEnc',
                }] // items (formpanel)
        }); // create()
        if (typeof mensajeDesvinculado !== "undefined") {
            Ext.Msg.alert("Cuenta desvinculada", mensajeDesvinculado, function(btn) {
                if (typeof error !== "undefined") {
                    Ext.Msg.alert(titulo, error);
                }
            });
        }
        else if (typeof error !== "undefined") {
            Ext.Msg.alert(titulo, error, function(btn) {
                if (typeof mensajeDesvinculado !== "undefined") {
                    Ext.Msg.alert("Cuenta desvinculada", mensajeDesvinculado);
                }
            });
        }

    } // launch
}); // application()
