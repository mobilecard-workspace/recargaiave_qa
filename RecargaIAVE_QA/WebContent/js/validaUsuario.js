Ext.application({
    launch: function() {
        var contactForm = Ext.create('Ext.form.FormPanel', {
            fullscreen: true,
            itemId: 'loginUsuario',
            scrollable: 'vertical',
            items: [{
                    xtype: 'titlebar',
                    title: 'Login MobileCard',                    
                    docked: 'top',
                    items: [{
                            xtype: 'image',
                            align: 'right',
                            height: 55,
                            width: 55,
                            mode: 'foreground',
                            src: 'images/img_mobilecardlogo_1.1.0.png'
                        }]
                }, {
                    xtype: 'fieldset',
                    items: [{
                            xtype: 'textfield',
                            name: 'login',
                            label: 'Login:'
                        }, {
                            xtype: 'passwordfield',
                            name: 'pass',
                            label: 'Password:'
                        }] // items
                }, {
                    xtype: 'toolbar',
                    layout: {
                        pack: 'center'
                    }, // layout
                    ui: 'plain',
                    items: [{
                            xtype: 'button',
                            text: 'Regresar',
                            ui: 'decline-small',
                            handler: function(btn, evt) {
                                Ext.Msg.confirm('', 'Are you sure you want to reset this form?', function(btn) {
                                    if (btn === 'yes') {
                                        contactForm.setValues({
                                            fname: '',
                                            lname: ''
                                        }); // contactForm()
                                    } // switch
                                }); // confirm()
                            }
                        }, {
                            xtype: 'button',
                            text: 'Entrar',
                            ui: 'confirm',
                            handler: function(btn, evt) {
                                var values = contactForm.getValues();
                                Ext.Msg.alert('Welcome', Ext.String.format('{0} {1}', values.fname, values.lname));
                            } // handler
                        }] // items (toolbar)
                }] // items (formpanel)
        }); // create()
    } // launch
}); // application()
