var pago;

function unMaskForm() {
    pago.setMasked(false);
}

Ext.application({
    launch: function() {
    	
		Ext.override(Ext.util.SizeMonitor, {
			constructor: function(config) {
			  var namespace = Ext.util.sizemonitor;

			  if (Ext.browser.is.Firefox) {
				return new namespace.OverflowChange(config);
			  } else if (Ext.browser.is.WebKit) {
				if (!Ext.browser.is.Silk && Ext.browser.engineVersion.gtEq('535') && !Ext.browser.engineVersion.ltEq('537.36')) {
				  return new namespace.OverflowChange(config);
				} else {
				  return new namespace.Scroll(config);
				}
			  } else if (Ext.browser.is.IE11) {
				return new namespace.Scroll(config);
			  } else {
				return new namespace.Scroll(config);
			  }
			}
		});
		
		Ext.override(Ext.util.PaintMonitor, {
			constructor: function(config) {
			  if (Ext.browser.is.Firefox || (Ext.browser.is.WebKit && Ext.browser.engineVersion.gtEq('536') && !Ext.browser.engineVersion.ltEq('537.36') && !Ext.os.is.Blackberry)) {
				return new Ext.util.paintmonitor.OverflowChange(config);
			  } else {
				return new Ext.util.paintmonitor.CssAnimation(config);
			  }
			}
		});
		
        Ext.applyIf(Ext.data.Validations, {
            presenceMejora: function(config, value) {
                if (arguments.length === 1) {
                    value = config;
                }
                if (value.trim().length === 0) {
                    return false;
                } else {
                    return true;
                }
            }
        });
        
        Ext.define('loginModel', {
            extend: 'Ext.data.Model',
            config: {
                fields: [{
                        name: 'login',
                        type: 'string'
                    }, {
                        name: 'pass',
                        type: 'string'
                    },{
                        name: 'flujo',
                        type: 'string'
                    }], // fields
                validations: [{
                        field: 'login',
                        type: 'presenceMejora',
                        message: 'El login es requerido'
                    }, {
                        field: 'pass',
                        type: 'presenceMejora',
                        message: 'El password es requerido'
                    }, {
                        field: 'pass',
                        type: 'length',
                        min: 8,
                        max: 13,
                        message: 'Password invalido.'
                    }
                ] // validations
            } // config
        }); // define()
        
        Ext.define('recargaModel', {
            extend: 'Ext.data.Model',
            config: {
                fields: [{
                        name: 'tagAlias',
                        type: 'string'
                    }, {
                        name: 'tag',
                        type: 'string'
                    }, {
                        name: 'monto',
                        type: 'string'
                    }, {
                        name: 'passMobile',
                        type: 'string'
                    }/*, {
                        name: 'cvv2',
                        type: 'string'
                    }*/], // fields
                validations: [{
                        field: 'tagAlias',
                        type: 'presenceMejora',
                        message: 'El alias del tag es requerido'
                    }, {
                        field: 'tag',
                        type: 'presenceMejora',
                        message: 'El tag es requerido'
                    }, {
                        field: 'monto',
                        type: 'presenceMejora',
                        message: 'El monto de la recarga es requerido'
                    }, {
                        field: 'passMobile',
                        type: 'presenceMejora',
                        message: 'El password MobileCard es requerido'
                    }, {
                        field: 'passMobile',
                        type: 'length',
                        min: 8,
                        max: 13,
                        message: 'El password MobileCard no es valido'
                    }/*, {
                        field: 'cvv2',
                        type: 'presenceMejora',
                        message: 'El cvv2 es requerido'
                    }, {
                        field: 'cvv2',
                        type: 'length',
                        min: 3,
                        max: 4,
                        message: 'El cvv2 no es valido.'
                    }*/
                ] // validations
            } // config
        }); // define()
        
        pago = Ext.create('Ext.form.Panel', {
            fullscreen: true,
            itemId: 'datosPago',
            scrollable: 'vertical',
            masked: {
                xtype: 'loadmask',
                message: 'Dando seguridad al canal... ' +
                        '<hr>Espere por favor...'
            },
            items: [{
                    xtype: 'titlebar',
                    docked: 'top',
                    title: 'Pago',
                    style: 'background:#09569b',
                    items: [{
                            iconCls: "delete",
                            iconMask: !0,
                            ui: "plain",
                            id: 'btnCancelar',
                            handler: function(btn, evt) {
                                location.href = "http://www.mobilecard.mx:8080/RecargaIAVE_test/close.html";
                            }
                        }, {
                            iconCls: "settings",
                            iconMask: !0,
                            align: 'right',
                            ui: "plain",
                            id: 'btnEditar',
                            handler: function(btn, evt) {
                                Ext.getCmp("btnComprar").disable();
                                Ext.getCmp("btnCancelar").disable();
                                Ext.getCmp("btnEditar").disable();
                                location.href = "welcome.do?method=actualizaDatos";
                            }
                        }]
                }, {
                    xtype: 'fieldset',
                    items: [
                        {
                            xtype: 'panel',
                            html: '<b><i><font size="3" color="#0000FF">Todas tus recargas prepago de TAG I+D tendr&aacute;n una comisi&oacute;n de $ ' + comision + ' con IVA INCLUIDO</font></i></b>'
                        },
                        {
                            xtype: 'textfield',
                            label: 'Alias del TAG:',
                            labelAlign: 'top',
                            name: 'tagAlias',
                            id: 'tagAlias',
                            readOnly: true,
                            value: new String(aliasTag)
                        },
                        {
                            xtype: 'textfield',
                            label: 'TAG:',
                            labelAlign: 'top',
                            name: 'tag',
                            id: 'tag',
                            readOnly: true,
                            value: new String(tag)
                        },
                        {
                            xtype: 'selectfield',
                            label: 'Monto:',
                            labelAlign: 'top',
                            name: 'monto',
                            id: 'monto',
                            displayField: 'descripcion',
                            valueField: 'claveWS',
                            options: productos.productos
                        },
                        {
                            xtype: 'passwordfield',
                            label: 'Password MobileCard:',
                            labelAlign: 'top',
                            name: 'passMobile',
                            id: 'passMobile'
                        },
                        {
                            xtype: 'passwordfield',
                            label: 'C&oacute;digo de seguridad de la tarjeta:',
                            labelAlign: 'top',
                            name: 'cvv2',
                            id: 'cvv2',
                            hidden : (tipoTDC == 3?false:true)
                        },/*
                        {
                            xtype: 'textfield',
                            label: 'tipo tarjeta:',
                            labelAlign: 'top',
                            name: 'tipo',
                            id: 'tipo',
                            value: new String(tipoTDC)
                        },*/
                        {
                            xtype: 'button',
                            text: 'Comprar',
                            id: 'btnComprar',
                            handler: function(btn, evt) {
                            	//Ext.Msg.alert('Comprar');
                                Ext.getCmp("btnComprar").disable();
                                Ext.getCmp("btnCancelar").disable();
                                var recargaModel = Ext.create('recargaModel');
                                var errors, errorMessage = '';
                                pago.updateRecord(recargaModel);
                                errors = recargaModel.validate();
                                if (!errors.isValid()) {
                                    errors.each(function(err) {
                                        errorMessage += err.getMessage() + '<br/>';
                                    }); // each()
                                    Ext.Msg.alert('Datos incorrectos', errorMessage, function(btn) {
                                        Ext.getCmp("btnComprar").enable();
                                        Ext.getCmp("btnCancelar").enable();
                                    });
                                } else {
                                    pagoEnc.setValues({tagAliasEnc: $.jCryption.encrypt(Ext.getCmp("tagAlias").getValue(), password_),
                                        tagEnc: $.jCryption.encrypt(Ext.getCmp("tag").getValue(), password_),
                                        montoEnc: $.jCryption.encrypt(Ext.getCmp("monto").getValue(), password_),
                                        passMobileCardEnc: $.jCryption.encrypt(Ext.getCmp("passMobile").getValue(), password_),
                                        //cvv2Enc: $.jCryption.encrypt(Ext.getCmp("cvv2").getValue(), password_),
                                        plataformaEnc: $.jCryption.encrypt(navigator.platform, password_)
                                    });
                                    pagoEnc.submit({url: 'recarga.do?method=realizaRecarga', method: 'POST'});
                                } // if
                            } // handler
                        }
                    ]
                }, {
                    xtype: 'toolbar',
                    docked: 'bottom',
                    styleHtmlContent: true,
                    title: '<div style="margin:-10px 0px 0px 0px;font-size: 10px; color: white;">Powered by Mobilecard &reg;</div>',
                    minHeight: '1.3em',
                    height: '1.3em',
                    style: 'background:#09569b'
                }]
        }); // create()
        
        var pagoEnc = Ext.create('Ext.form.FormPanel', {
            standardSubmit: true,
            items: [
                {
                    xtype: 'hiddenfield',
                    name: 'tagAliasEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'tagEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'montoEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'passMobileCardEnc'
                },/* {
                    xtype: 'hiddenfield',
                    name: 'cvv2Enc'
                },*/ {
                    xtype: 'hiddenfield',
                    name: 'plataformaEnc'
                }] // items (formpanel)
        }); // create()
        
        var popup = Ext.create('Ext.form.FormPanel', {
            floating: true,
            centered: true,
            modal: true,
            width: '98%',
            height: 250,
            standardSubmit: true,
            instructions: '<code>(*)</code> REQUERIDO',
            items: [{
                    xtype: 'titlebar',
                    title: 'Login MobileCard',
                    docked: 'top',
                    style: 'background:#09569b'
                }, {
                    xtype: 'fieldset',
                    items: [{
                            xtype: 'textfield',
                            name: 'login',
                            id: 'login',
                            label: 'Login:',
                            labelWidth: 150,
                            required: true,
                            readOnly: true,
                            value:login
                        }, {
                            xtype: 'passwordfield',
                            name: 'pass',
                            id: 'pass',
                            label: 'Password:',
                            labelWidth: 150,
                            required: true
                        }, {
                            xtype: 'hiddenfield',
                            name: 'flujo',
                            id: 'flujo',
                            value: 'UPDATE'
                        }] // items
                }, {
                    xtype: 'toolbar',
                    layout: {
                        pack: 'center'
                    }, // layout
                    ui: 'plain',
                    items: [{
                            xtype: 'button',
                            text: 'Regresar',
                            width: '35%',
                            handler: function(btn, evt) {
                                popup.hide();
                                Ext.getCmp("btnComprar").enable();
                                Ext.getCmp("btnCancelar").enable();
                                Ext.getCmp("btnEditar").enable();
                            }
                        }, {
                            xtype: 'button',
                            text: 'Enviar',
                            width: '35%',
                            id: 'btnEnviar',
                            handler: function(btn, evt) {
                                Ext.getCmp("btnEnviar").disable();
                                var loginModel = Ext.create('loginModel');
                                var errors, errorMessage = '';
                                popup.updateRecord(loginModel);
                                errors = loginModel.validate();
                                if (!errors.isValid()) {
                                    errors.each(function(err) {
                                        errorMessage += err.getMessage() + '<br/>';
                                    }); // each()
                                    Ext.Msg.alert('Datos incorrectos', errorMessage);
                                    Ext.getCmp("btnEnviar").enable();
                                } else {

                                    loginEnc.setValues({loginEnc: $.jCryption.encrypt(Ext.getCmp("login").getValue(), password_),
                                        passwordEnc: $.jCryption.encrypt(Ext.getCmp("pass").getValue(), password_),
                                        flujoEnc: $.jCryption.encrypt(Ext.getCmp("flujo").getValue(), password_)});
                                    loginEnc.submit({url: 'login.do?method=loginUsuario', method: 'POST'});
                                } // if
                            } // handler
                        }] // items (toolbar)
                }]
        }); // create()
        
        var loginEnc = Ext.create('Ext.form.FormPanel', {
            standardSubmit: true,
            items: [
                {
                    xtype: 'hiddenfield',
                    name: 'loginEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'passwordEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'flujoEnc'
                }] // items (formpanel)
        }); // create()
        if (typeof mensajeExito !== "undefined") {
            Ext.Msg.alert("Registro usuario", mensajeExito);
        }
        if (typeof mensajeError !== "undefined") {
            Ext.Msg.alert(tituloError, mensajeError);
        }
        if (typeof mensajeCambioPass !== "undefined") {
            Ext.Msg.alert("Cambio de password", mensajeCambioPass);
        }
    } // launch
}); // application()

