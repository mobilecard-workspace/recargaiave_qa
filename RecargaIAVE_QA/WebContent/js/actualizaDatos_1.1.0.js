var actualizaDatosForm;

function unMaskForm() {
    actualizaDatosForm.setMasked(false);
    actualizaDatosForm.doComponentLayout();
}
function margenElemento(porCont, tamElementoY) {
    var height = $(window).height();
    var tamPanel = height * (porCont / 100);
    return (tamPanel - tamElementoY) / 2;
}

function centraElementoX(tamx, inicioX, finX) {
    var x = finX - inicioX;
    alert(inicioX + ((x - tamx) / 2));
    return inicioX + ((x - tamx) / 2);
}
function centraElementoY(tamy, inicioY, finY) {
    var y = finY - inicioY;
    return inicioY + ((y - tamy) / 2);
}


Ext.application({
    launch: function() {
		Ext.override(Ext.util.SizeMonitor, {
			constructor: function(config) {
			  var namespace = Ext.util.sizemonitor;

			  if (Ext.browser.is.Firefox) {
				return new namespace.OverflowChange(config);
			  } else if (Ext.browser.is.WebKit) {
				if (!Ext.browser.is.Silk && Ext.browser.engineVersion.gtEq('535') && !Ext.browser.engineVersion.ltEq('537.36')) {
				  return new namespace.OverflowChange(config);
				} else {
				  return new namespace.Scroll(config);
				}
			  } else if (Ext.browser.is.IE11) {
				return new namespace.Scroll(config);
			  } else {
				return new namespace.Scroll(config);
			  }
			}
		});
		Ext.override(Ext.util.PaintMonitor, {
			constructor: function(config) {
			  if (Ext.browser.is.Firefox || (Ext.browser.is.WebKit && Ext.browser.engineVersion.gtEq('536') && !Ext.browser.engineVersion.ltEq('537.36') && !Ext.os.is.Blackberry)) {
				return new Ext.util.paintmonitor.OverflowChange(config);
			  } else {
				return new Ext.util.paintmonitor.CssAnimation(config);
			  }
			}
		});
		
        Ext.applyIf(Ext.data.Validations, {
            presenceMejora: function(config, value) {
                if (arguments.length === 1) {
                    value = config;
                }
                if (value.trim().length === 0) {
                    return false;
                } else {
                    return true;
                }
            }
        });
        Ext.define('loginModel', {
            extend: 'Ext.data.Model',
            config: {
                fields: [{
                        name: 'login',
                        type: 'string'
                    }, {
                        name: 'pass',
                        type: 'string'
                    }, {
                        name: 'flujo',
                        type: 'string'
                    }], // fields
                validations: [{
                        field: 'login',
                        type: 'presenceMejora',
                        message: 'El login es requerido'
                    }, {
                        field: 'pass',
                        type: 'presenceMejora',
                        message: 'El password es requerido'
                    }, {
                        field: 'pass',
                        type: 'length',
                        min: 8,
                        max: 13,
                        message: 'Password invalido.'
                    }
                ] // validations
            } // config
        }); // define()
        actualizaDatosForm = Ext.create(
                'Ext.form.FormPanel',
                {
                    fullscreen: true,
                    itemId: 'actualizaDatos',
                    scrollable: 'vertical',
                    standardSubmit: true,
                    masked: {
                        xtype: 'loadmask',
                        message: 'Dando seguridad al canal... ' +
                                '<hr>Espere por favor...'
                    }, // masked
                    items: [{
                            xtype: 'titlebar',
                            docked: 'top',
                            title: 'Editar Datos',
                            style: 'background:#09569b',
                            items: [{
                                    iconCls: "delete",
                                    iconMask: !0,
                                    ui: "plain",
                                    id: 'btnCancelar',
                                    handler: function(btn, evt) {
                                        location.href = "http://www.mobilecard.mx:8080/RecargaIAVE_test/close.html";
                                    }
                                }, {
                                    iconCls: "arrow_left",
                                    iconMask: !0,
                                    ui: "plain",
                                    id: 'btnRegresar',
                                    align: 'right',
                                    handler: function(btn, evt) {
                                        location.href = "welcome.do?method=goToRecarga";
                                    }
                                }
                            ]
                        }, {
                            xtype: 'image',
                            margin: new String(margenElemento(40, 144) + 'px auto'),
                            width: '144px',
                            height: '144px',
                            mode: 'foreground',
                            src: 'images/mobilecard.png'
                        }, {
                            xtype: 'button',
                            text: 'Desligar cuentas',
                            id: 'btnDesvincularMC',
                            width: '80%',
                            margin: new String(margenElemento(20, 70) + 'px auto'),
                            handler: function() {
                                Ext.getCmp("btnCancelar").disable();
                                Ext.getCmp("btnDesvincularMC").disable();
                                Ext.getCmp("btnCambiarPass").disable();
                                Ext.getCmp("btnReseteaPass").disable();
                                Ext.getCmp("btnUpdateNuevo").disable();
                                Ext.Viewport.add(popup);
                                popup.setValues({flujo: "DESVINCULAR"});
                                popup.show();
                                actualizaDatosForm.doComponentLayout();
                                popup.doComponentLayout();
                            }
                        }, {
                            xtype: 'button',
                            text: 'Cambiar password',
                            id: 'btnCambiarPass',
                            width: '80%',
                            margin: new String(margenElemento(20, 70) + 'px auto'),
                            handler: function(btn, evt) {
                                Ext.getCmp("btnCancelar").disable();
                                Ext.getCmp("btnDesvincularMC").disable();
                                Ext.getCmp("btnCambiarPass").disable();
                                Ext.getCmp("btnReseteaPass").disable();
                                Ext.getCmp("btnUpdateNuevo").disable();
                                Ext.Msg.confirm("Cambiar password ?", "Desea cambiar el password MobileCard?", function(btn) {
                                    if (btn === 'yes') {
                                        location.href = "welcome.do?method=cambiaPass";
                                    } else {
                                        Ext.getCmp("btnCancelar").enable();
                                        Ext.getCmp("btnDesvincularMC").enable();
                                        Ext.getCmp("btnCambiarPass").enable();
                                        Ext.getCmp("btnReseteaPass").enable();
                                        Ext.getCmp("btnUpdateNuevo").enable();
                                    }
                                });
                            }
                        }, {
                            xtype: 'button',
                            text: 'Resetear password',
                            id: 'btnReseteaPass',
                            width: '80%',
                            margin: new String(margenElemento(20, 70) + 'px auto'),
                            handler: function(btn, evt) {
                                Ext.getCmp("btnCancelar").disable();
                                Ext.getCmp("btnDesvincularMC").disable();
                                Ext.getCmp("btnCambiarPass").disable();
                                Ext.getCmp("btnReseteaPass").disable();
                                Ext.getCmp("btnUpdateNuevo").disable();
                                Ext.Viewport.add(popup2);
                                popup2.show();
                                actualizaDatosForm.doComponentLayout();
                                popup2.doComponentLayout();
                            }
                        }, {
                            xtype: 'button',
                            text: 'Actualizar registro',
                            id: 'btnUpdateNuevo',
                            width: '80%',
                            margin: new String(margenElemento(20, 70) + 'px auto'),
                            handler: function(btn, evt) {
                                Ext.getCmp("btnCancelar").disable();
                                Ext.getCmp("btnDesvincularMC").disable();
                                Ext.getCmp("btnCambiarPass").disable();
                                Ext.getCmp("btnReseteaPass").disable();
                                Ext.getCmp("btnUpdateNuevo").disable();
                                Ext.Viewport.add(popup);
                                popup.setValues({flujo: "UPDATE"});
                                popup.show();
                                actualizaDatosForm.doComponentLayout();
                                popup.doComponentLayout();
                            }
                        }, {
                            xtype: 'toolbar',
                            docked: 'bottom',
                            styleHtmlContent: true,
                            title: '<div style="margin:-10px 0px 0px 0px;font-size: 10px; color: white;">Powered by Mobilecard &reg;</div>',
                            minHeight: '1.3em',
                            height: '1.3em',
                            style: 'background:#09569b'
                        }
                    ]
                }

        );
        var popup = Ext.create('Ext.form.FormPanel', {
            floating: true,
            centered: true,
            modal: true,
            width: '98%',
            height: 250,
            standardSubmit: true,
            instructions: '<code>(*)</code> REQUERIDO',
            items: [{
                    xtype: 'titlebar',
                    title: 'Login MobileCard',
                    docked: 'top',
                    style: 'background:#09569b'
                }, {
                    xtype: 'fieldset',
                    items: [{
                            xtype: 'textfield',
                            name: 'login',
                            id: 'login',
                            label: 'Login:',
                            labelWidth: 150,
                            required: true,
                            value: login
                        }, {
                            xtype: 'passwordfield',
                            name: 'pass',
                            id: 'pass',
                            label: 'Password:',
                            labelWidth: 150,
                            required: true
                        }, {
                            xtype: 'hiddenfield',
                            name: 'flujo',
                            id: 'flujo'
                        }] // items
                }, {
                    xtype: 'toolbar',
                    layout: {
                        pack: 'center'
                    }, // layout
                    ui: 'plain',
                    items: [{
                            xtype: 'button',
                            text: 'Regresar',
                            width: '35%',
                            handler: function(btn, evt) {
                                popup.hide();
                                actualizaDatosForm.doComponentLayout();
                                Ext.getCmp("btnCancelar").enable();
                                Ext.getCmp("btnDesvincularMC").enable();
                                Ext.getCmp("btnCambiarPass").enable();
                                Ext.getCmp("btnReseteaPass").enable();
                                Ext.getCmp("btnUpdateNuevo").enable();

                            }
                        }, {
                            xtype: 'button',
                            text: 'Enviar',
                            width: '35%',
                            id: 'btnEnviar',
                            handler: function(btn, evt) {
                                Ext.getCmp("btnEnviar").disable();
                                var loginModel = Ext.create('loginModel');
                                var errors, errorMessage = '';
                                popup.updateRecord(loginModel);
                                errors = loginModel.validate();
                                if (!errors.isValid()) {
                                    errors.each(function(err) {
                                        errorMessage += err.getMessage() + '<br/>';
                                    }); // each()
                                    Ext.Msg.alert('Datos incorrectos', errorMessage);
                                    Ext.getCmp("btnEnviar").enable();
                                } else {
                                    var t, m;
                                    if (Ext.getCmp("flujo").getValue() === 'DESVINCULAR') {
                                        t = 'Desvincular cuentas ?';
                                        m = 'Desea desvincular sus cuentas ?';
                                    } else if (Ext.getCmp("flujo").getValue() === 'UPDATE') {
                                        t = 'Actualizar datos ?';
                                        m = 'Desea actualizar su registro MobileCard ?';
                                    }
                                    Ext.Msg.confirm(t, m, function(btn) {
                                        if (btn === 'yes') {
                                            loginEnc.setValues({loginEnc: $.jCryption.encrypt(Ext.getCmp("login").getValue(), password_),
                                                passwordEnc: $.jCryption.encrypt(Ext.getCmp("pass").getValue(), password_),
                                                flujoEnc: $.jCryption.encrypt(Ext.getCmp("flujo").getValue(), password_)});
                                            loginEnc.submit({url: "login.do?method=loginUsuario", method: 'POST'});
                                        } else {
                                            Ext.getCmp("btnCancelar").enable();
                                            Ext.getCmp("btnDesvincularMC").enable();
                                            Ext.getCmp("btnCambiarPass").enable();
                                            Ext.getCmp("btnUpdateNuevo").enable();
                                            Ext.getCmp("btnEnviar").enable();
                                        }
                                    })
                                } // if
                            } // handler
                        }] // items (toolbar)
                }]
        }); // create()
        var popup2 = Ext.create('Ext.form.FormPanel', {
            floating: true,
            centered: true,
            modal: true,
            width: '98%',
            height: 250,
            standardSubmit: true,
            instructions: '<code>(*)</code> REQUERIDO',
            items: [{
                    xtype: 'titlebar',
                    title: 'Login MobileCard',
                    docked: 'top',
                    style: 'background:#09569b'
                }, {
                    xtype: 'fieldset',
                    items: [{
                            xtype: 'textfield',
                            name: 'loginRecupera',
                            id: 'loginRecupera',
                            label: 'Login:',
                            labelWidth: 150,
                            required: true,
                            value: login
                        }] // items
                }, {
                    xtype: 'toolbar',
                    layout: {
                        pack: 'center'
                    }, // layout
                    ui: 'plain',
                    items: [{
                            xtype: 'button',
                            text: 'Regresar',
                            width: '35%',
                            handler: function(btn, evt) {
                                popup2.hide();
                                actualizaDatosForm.doComponentLayout();
                                Ext.getCmp("btnCancelar").enable();
                                Ext.getCmp("btnDesvincularMC").enable();
                                Ext.getCmp("btnCambiarPass").enable();
                                Ext.getCmp("btnReseteaPass").enable();
                                Ext.getCmp("btnUpdateNuevo").enable();
                            }
                        }, {
                            xtype: 'button',
                            text: 'Recuperar Password',
                            width: '35%',
                            id: 'btnRecuperar',
                            handler: function(btn, evt) {
                                Ext.getCmp("btnRecuperar").disable();
                                if (Ext.getCmp("loginRecupera").getValue().trim().length !== 0) {
                                    Ext.Msg.confirm('Confirmar', 'Desea recuperar su password?', function(btn) {
                                        if (btn === 'yes') {
                                            Ext.Viewport.mask({xtype: 'loadmask', message: 'Validando nombre de usuario'});
                                            Ext.Ajax.request({
                                                url: 'consultasAjax.do?method=recuperaPass',
                                                method: 'POST',
                                                params: {login: $.jCryption.encrypt(Ext.getCmp("loginRecupera").getValue(), password_)},
                                                success: function(xhr) {
                                                    var data = Ext.JSON.decode($.jCryption.decrypt(xhr.responseText, password));
                                                    if (data.error === '0') {
                                                        if (data.valido === '0') {
                                                            Ext.Msg.alert("Login no existe", "El nombre de usuario no se encontro");
                                                            Ext.getCmp("btnRecuperar").enable();
                                                            Ext.getCmp("btnCancelar").enable();
                                                            Ext.getCmp("btnRegresar").enable();
                                                        } else {
                                                            Ext.Msg.alert("Se reseteo su password", "Se envi&oacute; por correo su nuevo password, es necesario que lo cambie", function(btn) {
                                                                Ext.getCmp("btnRecuperar").disable();
                                                                location.href = "welcome.do?method=cambiaPass";
                                                            });
                                                        }
                                                    } else {
                                                        Ext.Msg.alert("Error de comunicaci&oacute;n", "No fue posible validar el nombre de usuario");
                                                        Ext.getCmp("btnRecuperar").enable();
                                                    }
                                                    Ext.Viewport.unmask();
                                                    Ext.getCmp("btnCancelar").enable();
                                                },
                                                failure: function(response, opts) {
                                                    Ext.Msg.alert("Error de comunicaci&oacute;n", "No fue posible validar el nombre de usuario");
                                                    Ext.Viewport.unmask();
                                                    Ext.getCmp("btnRecuperar").enable();
                                                    Ext.getCmp("btnCancelar").enable();
                                                    Ext.getCmp("btnRegresar").enable();
                                                }
                                            });
                                        } else {
                                            Ext.getCmp("btnRecuperar").enable();
                                            Ext.getCmp("btnCancelar").enable();
                                            Ext.getCmp("btnRegresar").enable();
                                        }
                                    });
                                } else {
                                    Ext.Msg.alert("Login no valido", "El login no es v&aacute;lido, ingrese su login por favor");
                                    Ext.getCmp("btnRecuperar").enable();
                                    Ext.getCmp("btnCancelar").enable();
                                    Ext.getCmp("btnRegresar").enable();
                                }
                            } // handler
                        }] // items (toolbar)
                }]
        }); // create()
        var loginEnc = Ext.create('Ext.form.FormPanel', {
            standardSubmit: true,
            items: [
                {
                    xtype: 'hiddenfield',
                    name: 'loginEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'passwordEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'flujoEnc'
                }] // items (formpanel)
        });
        if (typeof error !== 'undefined') {
            Ext.Msg.alert(titulo, error);
        }// create()
    } // launch
}); // application()