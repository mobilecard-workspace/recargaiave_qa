<!DOCTYPE html>
<html>
    <head>
        <%request.setAttribute("recursoJS", "login_1.1.1.js");%>
        <%@ include file="/jsp/genInclude.jsp" %>
        <%@ include file="/jsp/commonsInclude.jsp" %>
        <%String idPage = "goToLogin";
            if (!idPage.equals(nextPage)) {
                request.removeAttribute("llavePublica");
                request.setAttribute("errorMensaje", "Acceso denegado");
                response.sendRedirect(redirectErrorURL);
            }%>
        <title>Recarga</title>
        <script>
            var login = $.jCryption.decrypt('<%=request.getAttribute("login")%>', password);
            var flujo = $.jCryption.decrypt('<%=request.getAttribute("flujo")%>', password);
            <%if (request.getAttribute("errorMensaje") != null) {%>
            var titulo = $.jCryption.decrypt('<%=request.getAttribute("titulo")%>', password);
            var error = $.jCryption.decrypt('<%=request.getAttribute("errorMensaje")%>', password);
            <%}%>
            <%if (request.getAttribute("mensajeDesvinculado") != null) {%>
            var mensajeDesvinculado = $.jCryption.decrypt('<%=request.getAttribute("mensajeDesvinculado")%>', password);
            <%}%>
        </script>
    </head>
    <body></body>
</html>