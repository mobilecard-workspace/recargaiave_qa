<!DOCTYPE html>
<html>
    <head>
        <%request.setAttribute("recursoJS", "complementarios2_1.1.1.js");%>
        <%@ include file="/jsp/genInclude.jsp" %>
        <%@ include file="/jsp/commonsInclude.jsp" %>
        <%String idPage = "goToDatosComplementariosDos";
            if (!idPage.equals(nextPage)) {
                request.removeAttribute("llavePublica");
                request.setAttribute("errorMensaje", "Acceso denegado");
                response.sendRedirect(redirectErrorURL);
            }%>
        <title>Datos Complementarios</title>
        <script>
            <%if (request.getAttribute("flujo").equals("REGISTRO")) {%>
                var estados = Ext.JSON.decode($.jCryption.decrypt('<%=request.getAttribute("estados")%>', password));
                var tarjetas = Ext.JSON.decode($.jCryption.decrypt('<%=request.getAttribute("tarjetas")%>', password));
                var yearFrom = $.jCryption.decrypt('<%=request.getAttribute("yearFrom")%>', password);
                var yearTo = $.jCryption.decrypt('<%=request.getAttribute("yearTo")%>', password);
                var curMonth = $.jCryption.decrypt('<%=request.getAttribute("curMonth")%>', password);
                var flujo = '<%=request.getAttribute("flujo")%>';
                var estado;
                var ciudad;
                var calle;
                var numExt;
                var numInt;
                var colonia;
                var codigoPostal;
                var tipoTarjeta;
                var numTarjeta;
                var domicilioAMEX;
                var codigoPostalAMEX;
                var vigenciaTarjeta;
            <%} else if (request.getAttribute("flujo").equals("UPDATE")) {%>
                
                var estado = $.jCryption.decrypt('<%=request.getAttribute("estado")%>', password);
                var ciudad = $.jCryption.decrypt('<%=request.getAttribute("ciudad")%>', password);
                var calle = $.jCryption.decrypt('<%=request.getAttribute("calle")%>', password);
                var numExt = $.jCryption.decrypt('<%=request.getAttribute("numExt")%>', password);
                var numInt = $.jCryption.decrypt('<%=request.getAttribute("numInt")%>', password);
                var colonia = $.jCryption.decrypt('<%=request.getAttribute("colonia")%>', password);
                var codigoPostal = $.jCryption.decrypt('<%=request.getAttribute("codigoPostal")%>', password);
                var tipoTarjeta = $.jCryption.decrypt('<%=request.getAttribute("tipoTarjeta")%>', password);
                var numTarjeta = $.jCryption.decrypt('<%=request.getAttribute("numTarjeta")%>', password);
                var domicilioAMEX = $.jCryption.decrypt('<%=request.getAttribute("domicilioAMEX")%>', password);
                var codigoPostalAMEX = $.jCryption.decrypt('<%=request.getAttribute("codigoPostalAMEX")%>', password);
                var vigenciaTarjeta = Ext.JSON.decode($.jCryption.decrypt('<%=request.getAttribute("vigenciaTarjeta")%>', password));

                var estados = Ext.JSON.decode($.jCryption.decrypt('<%=request.getAttribute("estados")%>', password));
                var tarjetas = Ext.JSON.decode($.jCryption.decrypt('<%=request.getAttribute("tarjetas")%>', password));
                var yearFrom = $.jCryption.decrypt('<%=request.getAttribute("yearFrom")%>', password);
                var yearTo = $.jCryption.decrypt('<%=request.getAttribute("yearTo")%>', password);
                var curMonth = $.jCryption.decrypt('<%=request.getAttribute("curMonth")%>', password);
                var flujo = '<%=request.getAttribute("flujo")%>';
            <%}%>
        </script>
    </head>
    <body></body>
</html>