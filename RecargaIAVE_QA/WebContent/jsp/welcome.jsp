<!DOCTYPE html>

<html>
    <head>
        <%request.setAttribute("recursoJS", "welcome_1.1.1.js");%>
        <%@ include file="/jsp/genInclude.jsp" %>
        <%@ include file="/jsp/commonsInclude.jsp" %>
        <%String idPage = "goToWelcome";
            if (!idPage.equals(nextPage)) {
                request.removeAttribute("llavePublica");
                request.setAttribute("errorMensaje", "Acceso denegado");
                response.sendRedirect(redirectErrorURL);
            }%>
        <!--<script type="text/javascript" src="<%=request.getContextPath()%>/js/welcome_1.1.1.js"></script> --> 
        <title>Recarga</title>

        <style>
            .parent {
                position : relative;
                float : left;
                display: table-cell;
                width : 50%;
                height : 50%;
                overflow : hidden;
                background-color: white;
                text-align: center;
                vertical-align: middle;
            }
            .parent img
            {
                max-height :144px;
                max-width : 144px;
            }
            .parent *{
                vertical-align: middle;
            }
        </style>
        <script>
            <%if (request.getAttribute("mensajeDesvinculado") != null) {%>
            var mensajeDesvinculado = $.jCryption.decrypt('<%=request.getAttribute("mensajeDesvinculado")%>', password);
            <%}%>
        </script>
    </head>
    <body></body>
</html>