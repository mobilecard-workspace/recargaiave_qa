<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Cache-Control" content="no-cache">
        <meta http-equiv="Expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
        <title>String Encryption</title>
        <style type="text/css">
            html,body {
                margin:0;
                padding:0;
                font-family:Tahoma;
                font-size:12px;
            }
            input,textarea,select {
                font-family:Tahoma;
                font-size:12px;
            }
        </style>
        <script src="<%=request.getContextPath()%>/js/sencha-touch-all_2.2.1_v1.1.1.js"></script>
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/sencha-touch.2.2.1_v1.0.0.css">
            <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.min_1.1.0.js"></script>
            <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.jcryption.min_1.1.0.js"></script>
            <script>
                var passGlobal;
                $(function() {
                    var pass = '';
                    for (var i = 0; i < 20; i++) {
                        var numI = getRandomNum();
                        while (checkPunc(numI)) {
                            numI = getRandomNum();
                        }
                        pass = pass + String.fromCharCode(numI);
                    }
                    var hashObj = new jsSHA(pass, "ASCII");
                    var password = hashObj.getHash("SHA-512", "HEX");
                    $.jCryption.authenticate(password, "<%=request.getContextPath()%>/encrypt?callback=?&generateKeyPair=true&publicKey=" + pass, "<%=request.getContextPath()%>/encrypt?handshake=true",
                            function(AESKey) {
                                functionSucessfull(password);
                            }, function() {
                        functionError();
                    });
                });
                function getRandomNum() {
                    return (parseInt(Math.random() * 1000) % 94) + 33;
                }

                function checkPunc(num) {
                    if ((num >= 33) && (num <= 47))
                        return true;
                    else if ((num >= 58) && (num <= 64))
                        return true;
                    else if ((num >= 91) && (num <= 96))
                        return true;
                    else if ((num >= 123) && (num <= 126))
                        return true;
                    return false;
                }
                function functionSucessfull(password) {
                    passGlobal = password;
                }
                function functionError() {
                    alert("generado con error");
                }
                function enviar() {
                    document.myForm.iduserEnc.value = $.jCryption.encrypt($("#iduserEnc").val(), passGlobal);
                    document.myForm.dnEnc.value = $.jCryption.encrypt($("#dnEnc").val(), passGlobal);
                    document.myForm.emailEnc.value = $.jCryption.encrypt($("#emailEnc").val(), passGlobal);
                    document.myForm.imeiEnc.value = $.jCryption.encrypt($("#imeiEnc").val(), passGlobal);
                    document.myForm.tagEnc.value = $.jCryption.encrypt($("#tagEnc").val(), passGlobal);
                    document.myForm.nombreTagEnc.value = $.jCryption.encrypt($("#nombreTagEnc").val(), passGlobal);
                    document.myForm.pinEnc.value = $.jCryption.encrypt($("#pinEnc").val(), passGlobal);
                    document.myForm.submit();
                }
            </script>
    </head>

    <body>
        <form name="myForm" action="<%=request.getContextPath()%>/servicioIAVE.do?method=validaUsuario" method="post">
            <table>
                <tr>
                    <td>
                        IdUser IAVE
                    </td>
                    <td>
                        <input type="text" name="iduserEnc" id="iduserEnc" value="123456"></input>
                    </td>
                </tr>
                <tr>
                    <td>
                        Telefono
                    </td>
                    <td>
                        <input type="text" name="dnEnc" id="dnEnc" value="5518424322"></input>
                    </td>
                </tr>
                <tr>
                    <td>
                        Email
                    </td>
                    <td>
                        <input type="text" name="emailEnc" id="emailEnc" value="scanfernan17@gmail.com"></input>
                    </td>
                </tr>
                <tr>
                    <td>
                        IMEI
                    </td>
                    <td>
                        <input type="text" name="imeiEnc" id="imeiEnc" value="123456783648101"></input>
                    </td>
                </tr>
                <tr>
                    <td>
                        TAG
                    </td>
                    <td>
                        <input type="text" name="tagEnc" id="tagEnc" value="1234567890"></input>
                    </td>
                </tr>
                <tr>
                    <td>
                        Pin                        
                    </td>
                    <td>
                        <input type="text" name="pinEnc" id="pinEnc" value="2"></input>
                    </td>
                </tr>
                <tr>
                    <td>
                        Alias Tag                        
                    </td>
                    <td>
                        <input type="text" name="nombreTagEnc" id="nombreTagEnc" value="coche rojo"></input>
                    </td>
                </tr>
                <tr>
                    <td>                                             
                    </td>
                    <td>
                        <input type="button" onclick="enviar()" value="Enviar">
                    </td>
                </tr>
            </table>
        </form> 
    </body>
</html>
