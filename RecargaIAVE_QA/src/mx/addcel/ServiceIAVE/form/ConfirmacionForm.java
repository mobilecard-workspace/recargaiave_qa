package mx.addcel.ServiceIAVE.form;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class ConfirmacionForm
  extends MyActionForm
{
  private Logger log = Logger.getLogger(ConfirmacionForm.class);
  
  public void reset(ActionMapping mapping, HttpServletRequest request) {}
  
  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request)
  {
    ActionErrors errors = new ActionErrors();
    String keyResponse = null;
    String keyRequest = null;
    try
    {
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      keyRequest = (String)request.getSession().getAttribute("jCryptionKey_");
      String nextPage = (String)request.getAttribute("nextPage");
      this.log.info("ConfirmacionForm validate");
      if (((nextPage == null) || (!nextPage.equals("goToConfirmacionPago"))) && (
        (GenericValidator.isBlankOrNull(keyResponse)) || (GenericValidator.isBlankOrNull(keyRequest)))) {
        errors.add(null, new ActionMessage("Application.llaveInexistente"));
      }
    }
    catch (Exception e)
    {
      this.log.error("Exeption en validacion ConfirmacionForm : ", e);
      if (request.getAttribute("llavePublica") != null) {
        request.removeAttribute("llavePublica");
      }
      request.setAttribute("errorMensaje", "Error inesperado en la validaci&oacute;n");
    }
    return errors;
  }
}


/* Location:              /home/rhtrejo/workspace/RecargaIAVE/RecargaIAVE.war!/WEB-INF/classes/mx/addcel/ServiceIAVE/form/ConfirmacionForm.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */