package mx.addcel.ServiceIAVE.form;

import java.util.Iterator;
import javacryption.aes.AesCtr;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import mx.addcel.ServiceIAVE.service.utils.StrutsMensajes;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.util.PropertyMessageResources;

public class DatosComplementariosUnoForm
  extends MyActionForm
{
  private Logger log = Logger.getLogger(DatosComplementariosUnoForm.class);
  private String nombreUsuarioEnc;
  private String numCelularEnc;
  private String proveedorEnc;
  private String nombreEnc;
  private String apellidoPEnc;
  private String apellidoMEnc;
  private String fechaNacEnc;
  private String sexoEnc;
  private String emailEnc;
  private String telefonoCasaEnc;
  private String telefonoOficinaEnc;
  private String flujoEnc;
  private String nombreUsuario;
  private String numCelular;
  private String proveedor;
  private String nombre;
  private String apellidoP;
  private String apellidoM;
  private String fechaNac;
  private String sexo;
  private String email;
  private String telefonoCasa;
  private String telefonoOficina;
  private String flujo;
  
  public String getNumCelularEnc()
  {
    return this.numCelularEnc;
  }
  
  public void setNumCelularEnc(String numCelularEnc)
  {
    this.numCelularEnc = numCelularEnc;
  }
  
  public String getProveedorEnc()
  {
    return this.proveedorEnc;
  }
  
  public void setProveedorEnc(String proveedorEnc)
  {
    this.proveedorEnc = proveedorEnc;
  }
  
  public String getNombreEnc()
  {
    return this.nombreEnc;
  }
  
  public void setNombreEnc(String nombreEnc)
  {
    this.nombreEnc = nombreEnc;
  }
  
  public String getApellidoPEnc()
  {
    return this.apellidoPEnc;
  }
  
  public void setApellidoPEnc(String apellidoPEnc)
  {
    this.apellidoPEnc = apellidoPEnc;
  }
  
  public String getApellidoMEnc()
  {
    return this.apellidoMEnc;
  }
  
  public void setApellidoMEnc(String apellidoMEnc)
  {
    this.apellidoMEnc = apellidoMEnc;
  }
  
  public String getFechaNacEnc()
  {
    return this.fechaNacEnc;
  }
  
  public void setFechaNacEnc(String fechaNacEnc)
  {
    this.fechaNacEnc = fechaNacEnc;
  }
  
  public String getSexoEnc()
  {
    return this.sexoEnc;
  }
  
  public void setSexoEnc(String sexoEnc)
  {
    this.sexoEnc = sexoEnc;
  }
  
  public String getNumCelular()
  {
    return this.numCelular;
  }
  
  public void setNumCelular(String numCelular)
  {
    this.numCelular = numCelular;
  }
  
  public String getProveedor()
  {
    return this.proveedor;
  }
  
  public void setProveedor(String proveedor)
  {
    this.proveedor = proveedor;
  }
  
  public String getNombre()
  {
    return this.nombre;
  }
  
  public void setNombre(String nombre)
  {
    this.nombre = nombre;
  }
  
  public String getApellidoP()
  {
    return this.apellidoP;
  }
  
  public void setApellidoP(String apellidoP)
  {
    this.apellidoP = apellidoP;
  }
  
  public String getApellidoM()
  {
    return this.apellidoM;
  }
  
  public void setApellidoM(String apellidoM)
  {
    this.apellidoM = apellidoM;
  }
  
  public String getFechaNac()
  {
    return this.fechaNac;
  }
  
  public void setFechaNac(String fechaNac)
  {
    this.fechaNac = fechaNac;
  }
  
  public String getSexo()
  {
    return this.sexo;
  }
  
  public void setSexo(String sexo)
  {
    this.sexo = sexo;
  }
  
  public void reset(ActionMapping mapping, HttpServletRequest request)
  {
    setNombreUsuarioEnc(null);
    setNumCelularEnc(null);
    setProveedorEnc(null);
    setNombreEnc(null);
    setApellidoPEnc(null);
    setApellidoMEnc(null);
    setFechaNacEnc(null);
    setSexoEnc(null);
    setEmailEnc(null);
    setTelefonoCasaEnc(null);
    setTelefonoOficinaEnc(null);
    setNombreUsuario(null);
    setNumCelular(null);
    setProveedor(null);
    setNombre(null);
    setApellidoP(null);
    setApellidoM(null);
    setFechaNac(null);
    setSexo(null);
    setEmail(null);
    setTelefonoCasa(null);
    setTelefonoOficina(null);
    setFlujoEnc(null);
    setFlujo(null);
  }
  
  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request)
  {
    ActionErrors errors = new ActionErrors();
    String keyResponse = null;
    String keyRequest = null;
    try
    {
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      keyRequest = (String)request.getSession().getAttribute("jCryptionKey_");
      String nextPage = (String)request.getAttribute("nextPage");
      this.log.info("DatosComplementariosUnoForm validate");
      if ((nextPage == null) || (!nextPage.equals("goToDatosComplementariosUno"))) {
        if ((GenericValidator.isBlankOrNull(keyResponse)) || (GenericValidator.isBlankOrNull(keyRequest)))
        {
          errors.add(null, new ActionMessage("Application.llaveInexistente"));
        }
        else
        {
          if (GenericValidator.isBlankOrNull(getNombreUsuarioEnc())) {
            errors.add("nombreUsuarioEnc", new ActionMessage("ComplementariosUnoAction.nombreUsuarioEnc"));
          }
          if (GenericValidator.isBlankOrNull(getNumCelularEnc())) {
            errors.add("numCelularEnc", new ActionMessage("ComplementariosUnoAction.numCelularEnc"));
          }
          if (GenericValidator.isBlankOrNull(getProveedorEnc())) {
            errors.add("proveedorEnc", new ActionMessage("ComplementariosUnoAction.proveedorEnc"));
          }
          if (GenericValidator.isBlankOrNull(getNombreEnc())) {
            errors.add("nombreEnc", new ActionMessage("ComplementariosUnoAction.nombreEnc"));
          }
          if (GenericValidator.isBlankOrNull(getApellidoPEnc())) {
            errors.add("apellidoPEnc", new ActionMessage("ComplementariosUnoAction.apellidoPEnc"));
          }
          if (GenericValidator.isBlankOrNull(getFechaNacEnc())) {
            errors.add("fechaNacEnc", new ActionMessage("ComplementariosUnoAction.fechaNacEnc"));
          }
          if (GenericValidator.isBlankOrNull(getSexoEnc())) {
            errors.add("sexoEnc", new ActionMessage("ComplementariosUnoAction.sexoEnc"));
          }
          if (GenericValidator.isBlankOrNull(getEmailEnc())) {
            errors.add("emailEnc", new ActionMessage("ComplementariosUnoAction.emailEnc"));
          }
          if (errors.size() == 0)
          {
            setNombreUsuario(AesCtr.decrypt(getNombreUsuarioEnc(), keyRequest, 256));
            setNumCelular(AesCtr.decrypt(getNumCelularEnc(), keyRequest, 256));
            setProveedor(AesCtr.decrypt(getProveedorEnc(), keyRequest, 256));
            setNombre(AesCtr.decrypt(getNombreEnc(), keyRequest, 256));
            setApellidoP(AesCtr.decrypt(getApellidoPEnc(), keyRequest, 256));
            setApellidoM(AesCtr.decrypt(getApellidoMEnc(), keyRequest, 256));
            setFechaNac(AesCtr.decrypt(getFechaNacEnc(), keyRequest, 256));
            setSexo(AesCtr.decrypt(getSexoEnc(), keyRequest, 256));
            setTelefonoCasa(AesCtr.decrypt(getTelefonoCasaEnc(), keyRequest, 256));
            setTelefonoOficina(AesCtr.decrypt(getTelefonoOficinaEnc(), keyRequest, 256));
            setEmail(AesCtr.decrypt(getEmailEnc(), keyRequest, 256));
            setFlujo(AesCtr.decrypt(getFlujoEnc(), keyRequest, 256));
            if (GenericValidator.isBlankOrNull(getNombreUsuario())) {
              errors.add("nombreUsuarioEnc", new ActionMessage("ComplementariosUnoAction.nombreUsuarioEnc"));
            }
            if (GenericValidator.isBlankOrNull(getNumCelular())) {
              errors.add("numCelularEnc", new ActionMessage("ComplementariosUnoAction.numCelularEnc"));
            }
            if (GenericValidator.isBlankOrNull(getProveedor())) {
              errors.add("proveedorEnc", new ActionMessage("ComplementariosUnoAction.proveedorEnc"));
            }
            if (GenericValidator.isBlankOrNull(getNombre())) {
              errors.add("nombreEnc", new ActionMessage("ComplementariosUnoAction.nombreEnc"));
            }
            if (GenericValidator.isBlankOrNull(getApellidoP())) {
              errors.add("apellidoPEnc", new ActionMessage("ComplementariosUnoAction.apellidoPEnc"));
            }
            if (GenericValidator.isBlankOrNull(getFechaNac())) {
              errors.add("fechaNacEnc", new ActionMessage("ComplementariosUnoAction.fechaNacEnc"));
            }
            if (GenericValidator.isBlankOrNull(getSexo())) {
              errors.add("sexoEnc", new ActionMessage("ComplementariosUnoAction.sexoEnc"));
            }
            if (GenericValidator.isBlankOrNull(getEmail())) {
              errors.add("emailEnc", new ActionMessage("ComplementariosUnoAction.emailEnc"));
            }
          }
          if (errors.size() == 0)
          {
            if (!getNombreUsuario().matches("^[0-9a-zA-Z_.-]{0,35}$")) {
              errors.add("nombreUsuarioEnc", new ActionMessage("ComplementariosUnoAction.nombreUsuarioEnc.format"));
            }
            if (!getNumCelular().matches("^\\d{10}$")) {
              errors.add("numCelularEnc", new ActionMessage("ComplementariosUnoAction.numCelularEnc.format"));
            }
            if (!getProveedor().matches("^\\d$")) {
              errors.add("proveedorEnc", new ActionMessage("ComplementariosUnoAction.proveedorEnc.format"));
            }
            if (!getNombre().matches("^([a-zA-Z ÑÁÉÍÓÚñáéíóú]{2,35})$")) {
              errors.add("nombreEnc", new ActionMessage("ComplementariosUnoAction.nombreEnc.format"));
            }
            if (!getApellidoP().matches("^([a-zA-Z ÑÁÉÍÓÚñáéíóú]{2,45})$")) {
              errors.add("apellidoP", new ActionMessage("ComplementariosUnoAction.apellidoPEnc.format"));
            }
            if ((!GenericValidator.isBlankOrNull(getApellidoM())) && (!getApellidoM().matches("^([a-zA-Z ÑÁÉÍÓÚñáéíóú]{2,45})$"))) {
              errors.add("apellidoM", new ActionMessage("ComplementariosUnoAction.apellidoMEnc.format"));
            }
            if (!getFechaNac().matches("^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$")) {
              errors.add("fechaNac", new ActionMessage("ComplementariosUnoAction.fechaNac.format"));
            }
            if (!getSexo().matches("^[FM]{1}$")) {
              errors.add("sexoEnc", new ActionMessage("ComplementariosUnoAction.sexoEnc.format"));
            }
            if (!GenericValidator.isEmail(getEmail())) {
              errors.add("sexoEnc", new ActionMessage("ComplementariosUnoAction.emailEnc.format"));
            }
          }
          if (errors.size() != 0)
          {
            request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
            StringBuilder sb = new StringBuilder();
            Iterator<ActionMessage> iter = errors.get();
            PropertyMessageResources p = (PropertyMessageResources)request.getAttribute("org.apache.struts.action.MESSAGE");
            while (iter.hasNext())
            {
              ActionMessage msg = (ActionMessage)iter.next();
              sb.append(StrutsMensajes.getErrorMessage(p, msg));
              sb.append("<br/>");
            }
            this.log.info(sb);
            this.log.info(toString());
            request.setAttribute("errorMensaje", AesCtr.encrypt(sb.toString(), keyResponse, 256));
          }
        }
      }
    }
    catch (Exception e)
    {
      this.log.error("Exeption en validacion DatosComplementariosUnoForm : ", e);
      if (request.getAttribute("llavePublica") != null) {
        request.removeAttribute("llavePublica");
      }
      request.setAttribute("errorMensaje", "Error inesperado en la validaci&oacute;n");
    }
    return errors;
  }
  
  public String getTelefonoCasaEnc()
  {
    return this.telefonoCasaEnc;
  }
  
  public void setTelefonoCasaEnc(String telefonoCasaEnc)
  {
    this.telefonoCasaEnc = telefonoCasaEnc;
  }
  
  public String getTelefonoOficinaEnc()
  {
    return this.telefonoOficinaEnc;
  }
  
  public void setTelefonoOficinaEnc(String telefonoOficinaEnc)
  {
    this.telefonoOficinaEnc = telefonoOficinaEnc;
  }
  
  public String getTelefonoCasa()
  {
    return this.telefonoCasa;
  }
  
  public void setTelefonoCasa(String telefonoCasa)
  {
    this.telefonoCasa = telefonoCasa;
  }
  
  public String getTelefonoOficina()
  {
    return this.telefonoOficina;
  }
  
  public void setTelefonoOficina(String telefonoOficina)
  {
    this.telefonoOficina = telefonoOficina;
  }
  
  public String getNombreUsuarioEnc()
  {
    return this.nombreUsuarioEnc;
  }
  
  public void setNombreUsuarioEnc(String nombreUsuarioEnc)
  {
    this.nombreUsuarioEnc = nombreUsuarioEnc;
  }
  
  public String getNombreUsuario()
  {
    return this.nombreUsuario;
  }
  
  public void setNombreUsuario(String nombreUsuario)
  {
    this.nombreUsuario = nombreUsuario;
  }
  
  public String getEmailEnc()
  {
    return this.emailEnc;
  }
  
  public void setEmailEnc(String emailEnc)
  {
    this.emailEnc = emailEnc;
  }
  
  public String getEmail()
  {
    return this.email;
  }
  
  public void setEmail(String email)
  {
    this.email = email;
  }
  
  public String getFlujoEnc()
  {
    return this.flujoEnc;
  }
  
  public void setFlujoEnc(String flujoEnc)
  {
    this.flujoEnc = flujoEnc;
  }
  
  public String getFlujo()
  {
    return this.flujo;
  }
  
  public void setFlujo(String flujo)
  {
    this.flujo = flujo;
  }
}


/* Location:              /home/rhtrejo/workspace/RecargaIAVE/RecargaIAVE.war!/WEB-INF/classes/mx/addcel/ServiceIAVE/form/DatosComplementariosUnoForm.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */