package mx.addcel.ServiceIAVE.form;

import java.util.Iterator;
import javacryption.aes.AesCtr;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import mx.addcel.ServiceIAVE.service.utils.StrutsMensajes;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.util.PropertyMessageResources;

public class LoginForm
  extends MyActionForm
{
  private Logger log = Logger.getLogger(ActionForm.class);
  private String loginEnc;
  private String passwordEnc;
  private String flujoEnc;
  private String login;
  private String password;
  private String flujo;
  
  public void reset(ActionMapping mapping, HttpServletRequest request)
  {
    setLoginEnc(null);
    setPasswordEnc(null);
    setLogin(null);
    setPassword(null);
    setFlujoEnc(null);
    setFlujo(null);
  }
  
  public String getLoginEnc()
  {
    return this.loginEnc;
  }
  
  public void setLoginEnc(String loginEnc)
  {
    this.loginEnc = loginEnc;
  }
  
  public String getPasswordEnc()
  {
    return this.passwordEnc;
  }
  
  public void setPasswordEnc(String passwordEnc)
  {
    this.passwordEnc = passwordEnc;
  }
  
  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request)
  {
    ActionErrors errors = new ActionErrors();
    String keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
    String keyRequest = (String)request.getSession().getAttribute("jCryptionKey_");
    this.log.info("validate LoginForm");
    try
    {
      String nextPage = (String)request.getAttribute("nextPage");
      this.log.info("LoginForm validate");
      if ((nextPage == null) || (!nextPage.equals("goToLogin"))) {
        if ((GenericValidator.isBlankOrNull(keyResponse)) || (GenericValidator.isBlankOrNull(keyRequest)))
        {
          errors.add(null, new ActionMessage("Application.llaveInexistente"));
        }
        else
        {
          if (GenericValidator.isBlankOrNull(getLoginEnc())) {
            errors.add("loginEnc", new ActionMessage("LoginAction.loginEnc"));
          }
          if (GenericValidator.isBlankOrNull(getPasswordEnc())) {
            errors.add("passwordEnc", new ActionMessage("LoginAction.passwordEnc"));
          }
          if (errors.size() == 0)
          {
            setLogin(AesCtr.decrypt(getLoginEnc(), keyRequest, 256));
            setPassword(AesCtr.decrypt(getPasswordEnc(), keyRequest, 256));
            setFlujo(AesCtr.decrypt(getFlujoEnc(), keyRequest, 256));
            if (GenericValidator.isBlankOrNull(getLogin())) {
              errors.add("loginEnc", new ActionMessage("LoginAction.loginEnc"));
            }
            if (GenericValidator.isBlankOrNull(getPassword())) {
              errors.add("passwordEnc", new ActionMessage("LoginAction.passwordEnc"));
            }
          }
          if (errors.size() != 0)
          {
            request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
            StringBuilder sb = new StringBuilder();
            Iterator<ActionMessage> iter = errors.get();
            PropertyMessageResources p = (PropertyMessageResources)request.getAttribute("org.apache.struts.action.MESSAGE");
            while (iter.hasNext())
            {
              ActionMessage msg = (ActionMessage)iter.next();
              sb.append(StrutsMensajes.getErrorMessage(p, msg));
              sb.append("<br/>");
            }
            this.log.info(sb);
            this.log.info(toString());
            request.setAttribute("errorMensaje", AesCtr.encrypt(sb.toString(), keyResponse, 256));
          }
        }
      }
    }
    catch (Exception e)
    {
      this.log.error("Exeption en validacion LoginForm : ", e);
      if (request.getAttribute("llavePublica") != null) {
        request.removeAttribute("llavePublica");
      }
      request.setAttribute("errorMensaje", "Error inesperado en la validaci&oacute;n");
    }
    return errors;
  }
  
  public String getLogin()
  {
    return this.login;
  }
  
  public void setLogin(String login)
  {
    this.login = login;
  }
  
  public String getPassword()
  {
    return this.password;
  }
  
  public void setPassword(String password)
  {
    this.password = password;
  }
  
  public String getFlujoEnc()
  {
    return this.flujoEnc;
  }
  
  public void setFlujoEnc(String flujoEnc)
  {
    this.flujoEnc = flujoEnc;
  }
  
  public String getFlujo()
  {
    return this.flujo;
  }
  
  public void setFlujo(String flujo)
  {
    this.flujo = flujo;
  }
}


/* Location:              /home/rhtrejo/workspace/RecargaIAVE/RecargaIAVE.war!/WEB-INF/classes/mx/addcel/ServiceIAVE/form/LoginForm.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */