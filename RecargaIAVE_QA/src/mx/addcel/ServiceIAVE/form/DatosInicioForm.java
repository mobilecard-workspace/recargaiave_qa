package mx.addcel.ServiceIAVE.form;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionMapping;

public class DatosInicioForm
  extends MyActionForm
{
  private String iduserEnc;
  private String dnEnc;
  private String emailEnc;
  private String imeiEnc;
  private String tagEnc;
  private String nombreTagEnc;
  private String nombreUsuarioEnc;
  private String numCelularEnc;
  private String proveedorEnc;
  private String nombreEnc;
  private String apellidoPEnc;
  private String apellidoMEnc;
  private String fechaNacEnc;
  private String sexoEnc;
  private String telefonoCasaEnc;
  private String telefonoOficinaEnc;
  private String estadoEnc;
  private String ciudadEnc;
  private String calleEnc;
  private String numExtEnc;
  private String numIntEnc;
  private String coloniaEnc;
  private String codigoPostalEnc;
  private String numTarjetaEnc;
  private String tipoTarjetaEnc;
  private String codigoPostalAMEXEnc;
  private String domicilioAMEXEnc;
  private String vigenciaTarjetaEnc;
  private String terminosEnc;
  private String loginEnc;
  private String passwordEnc;
  private String plataformaEnc;
  private String montoEnc;
  private String passMobileCardEnc;
  private String cvv2Enc;
  private String pinEnc;
  
  public String getIduserEnc()
  {
    return this.iduserEnc;
  }
  
  public void setIduserEnc(String iduserEnc)
  {
    this.iduserEnc = iduserEnc;
  }
  
  public String getDnEnc()
  {
    return this.dnEnc;
  }
  
  public void setDnEnc(String dnEnc)
  {
    this.dnEnc = dnEnc;
  }
  
  public String getEmailEnc()
  {
    return this.emailEnc;
  }
  
  public void setEmailEnc(String emailEnc)
  {
    this.emailEnc = emailEnc;
  }
  
  public String getImeiEnc()
  {
    return this.imeiEnc;
  }
  
  public void setImeiEnc(String imeiEnc)
  {
    this.imeiEnc = imeiEnc;
  }
  
  public String getTagEnc()
  {
    return this.tagEnc;
  }
  
  public void setTagEnc(String tagEnc)
  {
    this.tagEnc = tagEnc;
  }
  
  public String getNombreTagEnc()
  {
    return this.nombreTagEnc;
  }
  
  public void setNombreTagEnc(String nombreTagEnc)
  {
    this.nombreTagEnc = nombreTagEnc;
  }
  
  public void reset(ActionMapping mapping, HttpServletRequest request)
  {
    setIduserEnc(null);
    setDnEnc(null);
    setEmailEnc(null);
    setImeiEnc(null);
    setTagEnc(null);
    setNombreTagEnc(null);
    setNombreUsuarioEnc(null);
    setNumCelularEnc(null);
    setProveedorEnc(null);
    setNombreEnc(null);
    setApellidoPEnc(null);
    setApellidoMEnc(null);
    setFechaNacEnc(null);
    setSexoEnc(null);
    setTelefonoCasaEnc(null);
    setTelefonoOficinaEnc(null);
    setEstadoEnc(null);
    setCiudadEnc(null);
    setCalleEnc(null);
    setNumExtEnc(null);
    setNumIntEnc(null);
    setColoniaEnc(null);
    setCodigoPostalEnc(null);
    setNumTarjetaEnc(null);
    setTipoTarjetaEnc(null);
    setCodigoPostalAMEXEnc(null);
    setDomicilioAMEXEnc(null);
    setVigenciaTarjetaEnc(null);
    setTerminosEnc(null);
    setLoginEnc(null);
    setPasswordEnc(null);
    setPlataformaEnc(null);
    setMontoEnc(null);
    setPassMobileCardEnc(null);
    setCvv2Enc(null);
  }
  
  public String getNombreUsuarioEnc()
  {
    return this.nombreUsuarioEnc;
  }
  
  public void setNombreUsuarioEnc(String nombreUsuarioEnc)
  {
    this.nombreUsuarioEnc = nombreUsuarioEnc;
  }
  
  public String getNumCelularEnc()
  {
    return this.numCelularEnc;
  }
  
  public void setNumCelularEnc(String numCelularEnc)
  {
    this.numCelularEnc = numCelularEnc;
  }
  
  public String getProveedorEnc()
  {
    return this.proveedorEnc;
  }
  
  public void setProveedorEnc(String proveedorEnc)
  {
    this.proveedorEnc = proveedorEnc;
  }
  
  public String getNombreEnc()
  {
    return this.nombreEnc;
  }
  
  public void setNombreEnc(String nombreEnc)
  {
    this.nombreEnc = nombreEnc;
  }
  
  public String getApellidoPEnc()
  {
    return this.apellidoPEnc;
  }
  
  public void setApellidoPEnc(String apellidoPEnc)
  {
    this.apellidoPEnc = apellidoPEnc;
  }
  
  public String getApellidoMEnc()
  {
    return this.apellidoMEnc;
  }
  
  public void setApellidoMEnc(String apellidoMEnc)
  {
    this.apellidoMEnc = apellidoMEnc;
  }
  
  public String getFechaNacEnc()
  {
    return this.fechaNacEnc;
  }
  
  public void setFechaNacEnc(String fechaNacEnc)
  {
    this.fechaNacEnc = fechaNacEnc;
  }
  
  public String getSexoEnc()
  {
    return this.sexoEnc;
  }
  
  public void setSexoEnc(String sexoEnc)
  {
    this.sexoEnc = sexoEnc;
  }
  
  public String getTelefonoCasaEnc()
  {
    return this.telefonoCasaEnc;
  }
  
  public void setTelefonoCasaEnc(String telefonoCasaEnc)
  {
    this.telefonoCasaEnc = telefonoCasaEnc;
  }
  
  public String getTelefonoOficinaEnc()
  {
    return this.telefonoOficinaEnc;
  }
  
  public void setTelefonoOficinaEnc(String telefonoOficinaEnc)
  {
    this.telefonoOficinaEnc = telefonoOficinaEnc;
  }
  
  public String getEstadoEnc()
  {
    return this.estadoEnc;
  }
  
  public void setEstadoEnc(String estadoEnc)
  {
    this.estadoEnc = estadoEnc;
  }
  
  public String getCiudadEnc()
  {
    return this.ciudadEnc;
  }
  
  public void setCiudadEnc(String ciudadEnc)
  {
    this.ciudadEnc = ciudadEnc;
  }
  
  public String getCalleEnc()
  {
    return this.calleEnc;
  }
  
  public void setCalleEnc(String calleEnc)
  {
    this.calleEnc = calleEnc;
  }
  
  public String getNumExtEnc()
  {
    return this.numExtEnc;
  }
  
  public void setNumExtEnc(String numExtEnc)
  {
    this.numExtEnc = numExtEnc;
  }
  
  public String getNumIntEnc()
  {
    return this.numIntEnc;
  }
  
  public void setNumIntEnc(String numIntEnc)
  {
    this.numIntEnc = numIntEnc;
  }
  
  public String getColoniaEnc()
  {
    return this.coloniaEnc;
  }
  
  public void setColoniaEnc(String coloniaEnc)
  {
    this.coloniaEnc = coloniaEnc;
  }
  
  public String getCodigoPostalEnc()
  {
    return this.codigoPostalEnc;
  }
  
  public void setCodigoPostalEnc(String codigoPostalEnc)
  {
    this.codigoPostalEnc = codigoPostalEnc;
  }
  
  public String getNumTarjetaEnc()
  {
    return this.numTarjetaEnc;
  }
  
  public void setNumTarjetaEnc(String numTarjetaEnc)
  {
    this.numTarjetaEnc = numTarjetaEnc;
  }
  
  public String getTipoTarjetaEnc()
  {
    return this.tipoTarjetaEnc;
  }
  
  public void setTipoTarjetaEnc(String tipoTarjetaEnc)
  {
    this.tipoTarjetaEnc = tipoTarjetaEnc;
  }
  
  public String getCodigoPostalAMEXEnc()
  {
    return this.codigoPostalAMEXEnc;
  }
  
  public void setCodigoPostalAMEXEnc(String codigoPostalAMEXEnc)
  {
    this.codigoPostalAMEXEnc = codigoPostalAMEXEnc;
  }
  
  public String getDomicilioAMEXEnc()
  {
    return this.domicilioAMEXEnc;
  }
  
  public void setDomicilioAMEXEnc(String domicilioAMEXEnc)
  {
    this.domicilioAMEXEnc = domicilioAMEXEnc;
  }
  
  public String getVigenciaTarjetaEnc()
  {
    return this.vigenciaTarjetaEnc;
  }
  
  public void setVigenciaTarjetaEnc(String vigenciaTarjetaEnc)
  {
    this.vigenciaTarjetaEnc = vigenciaTarjetaEnc;
  }
  
  public String getTerminosEnc()
  {
    return this.terminosEnc;
  }
  
  public void setTerminosEnc(String terminosEnc)
  {
    this.terminosEnc = terminosEnc;
  }
  
  public String getLoginEnc()
  {
    return this.loginEnc;
  }
  
  public void setLoginEnc(String loginEnc)
  {
    this.loginEnc = loginEnc;
  }
  
  public String getPasswordEnc()
  {
    return this.passwordEnc;
  }
  
  public void setPasswordEnc(String passwordEnc)
  {
    this.passwordEnc = passwordEnc;
  }
  
  public String getPlataformaEnc()
  {
    return this.plataformaEnc;
  }
  
  public void setPlataformaEnc(String plataformaEnc)
  {
    this.plataformaEnc = plataformaEnc;
  }
  
  public String getMontoEnc()
  {
    return this.montoEnc;
  }
  
  public void setMontoEnc(String montoEnc)
  {
    this.montoEnc = montoEnc;
  }
  
  public String getPassMobileCardEnc()
  {
    return this.passMobileCardEnc;
  }
  
  public void setPassMobileCardEnc(String passMobileCardEnc)
  {
    this.passMobileCardEnc = passMobileCardEnc;
  }
  
  public String getCvv2Enc()
  {
    return this.cvv2Enc;
  }
  
  public void setCvv2Enc(String cvv2Enc)
  {
    this.cvv2Enc = cvv2Enc;
  }
  
  public String getPinEnc()
  {
    return this.pinEnc;
  }
  
  public void setPinEnc(String pinEnc)
  {
    this.pinEnc = pinEnc;
  }
}


/* Location:              /home/rhtrejo/workspace/RecargaIAVE/RecargaIAVE.war!/WEB-INF/classes/mx/addcel/ServiceIAVE/form/DatosInicioForm.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */