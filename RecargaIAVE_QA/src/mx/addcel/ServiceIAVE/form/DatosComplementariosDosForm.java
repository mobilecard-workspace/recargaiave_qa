package mx.addcel.ServiceIAVE.form;

import java.util.Iterator;
import javacryption.aes.AesCtr;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import mx.addcel.ServiceIAVE.service.utils.StrutsMensajes;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.util.PropertyMessageResources;

public class DatosComplementariosDosForm
  extends MyActionForm
{
  private Logger log = Logger.getLogger(DatosComplementariosDosForm.class);
  private String estadoEnc;
  private String ciudadEnc;
  private String calleEnc;
  private String numExtEnc;
  private String numIntEnc;
  private String coloniaEnc;
  private String codigoPostalEnc;
  private String numTarjetaEnc;
  private String tipoTarjetaEnc;
  private String codigoPostalAMEXEnc;
  private String domicilioAMEXEnc;
  private String vigenciaTarjetaEnc;
  private String terminosEnc;
  private String plataformaEnc;
  private String flujoEnc;
  private String estado;
  private String ciudad;
  private String calle;
  private String numExt;
  private String numInt;
  private String colonia;
  private String codigoPostal;
  private String numTarjeta;
  private String tipoTarjeta;
  private String codigoPostalAMEX;
  private String domicilioAMEX;
  private String vigenciaTarjeta;
  private String terminos;
  private String plataforma;
  private String flujo;
  
  public void reset(ActionMapping mapping, HttpServletRequest request)
  {
    setEstadoEnc(null);
    setCiudadEnc(null);
    setCalleEnc(null);
    setNumExtEnc(null);
    setNumIntEnc(null);
    setColoniaEnc(null);
    setCodigoPostalEnc(null);
    setNumTarjetaEnc(null);
    setTipoTarjetaEnc(null);
    setCodigoPostalAMEXEnc(null);
    setDomicilioAMEXEnc(null);
    setVigenciaTarjetaEnc(null);
    setTerminosEnc(null);
    setPlataformaEnc(null);
    setEstado(null);
    setCiudad(null);
    setCalle(null);
    setNumExt(null);
    setNumInt(null);
    setColonia(null);
    setCodigoPostal(null);
    setNumTarjeta(null);
    setTipoTarjeta(null);
    setCodigoPostalAMEX(null);
    setDomicilioAMEX(null);
    setVigenciaTarjeta(null);
    setTerminos(null);
    setPlataforma(null);
    setFlujoEnc(null);
    setFlujo(null);
  }
  
  public String getEstadoEnc()
  {
    return this.estadoEnc;
  }
  
  public void setEstadoEnc(String estadoEnc)
  {
    this.estadoEnc = estadoEnc;
  }
  
  public String getCiudadEnc()
  {
    return this.ciudadEnc;
  }
  
  public void setCiudadEnc(String ciudadEnc)
  {
    this.ciudadEnc = ciudadEnc;
  }
  
  public String getCalleEnc()
  {
    return this.calleEnc;
  }
  
  public void setCalleEnc(String calleEnc)
  {
    this.calleEnc = calleEnc;
  }
  
  public String getNumExtEnc()
  {
    return this.numExtEnc;
  }
  
  public void setNumExtEnc(String numExtEnc)
  {
    this.numExtEnc = numExtEnc;
  }
  
  public String getNumIntEnc()
  {
    return this.numIntEnc;
  }
  
  public void setNumIntEnc(String numIntEnc)
  {
    this.numIntEnc = numIntEnc;
  }
  
  public String getColoniaEnc()
  {
    return this.coloniaEnc;
  }
  
  public void setColoniaEnc(String coloniaEnc)
  {
    this.coloniaEnc = coloniaEnc;
  }
  
  public String getCodigoPostalEnc()
  {
    return this.codigoPostalEnc;
  }
  
  public void setCodigoPostalEnc(String codigoPostalEnc)
  {
    this.codigoPostalEnc = codigoPostalEnc;
  }
  
  public String getNumTarjetaEnc()
  {
    return this.numTarjetaEnc;
  }
  
  public void setNumTarjetaEnc(String numTarjetaEnc)
  {
    this.numTarjetaEnc = numTarjetaEnc;
  }
  
  public String getTipoTarjetaEnc()
  {
    return this.tipoTarjetaEnc;
  }
  
  public void setTipoTarjetaEnc(String tipoTarjetaEnc)
  {
    this.tipoTarjetaEnc = tipoTarjetaEnc;
  }
  
  public String getCodigoPostalAMEXEnc()
  {
    return this.codigoPostalAMEXEnc;
  }
  
  public void setCodigoPostalAMEXEnc(String codigoPostalAMEXEnc)
  {
    this.codigoPostalAMEXEnc = codigoPostalAMEXEnc;
  }
  
  public String getDomicilioAMEXEnc()
  {
    return this.domicilioAMEXEnc;
  }
  
  public void setDomicilioAMEXEnc(String domicilioAMEXEnc)
  {
    this.domicilioAMEXEnc = domicilioAMEXEnc;
  }
  
  public String getVigenciaTarjetaEnc()
  {
    return this.vigenciaTarjetaEnc;
  }
  
  public void setVigenciaTarjetaEnc(String vigenciaTarjetaEnc)
  {
    this.vigenciaTarjetaEnc = vigenciaTarjetaEnc;
  }
  
  public String getTerminosEnc()
  {
    return this.terminosEnc;
  }
  
  public void setTerminosEnc(String terminosEnc)
  {
    this.terminosEnc = terminosEnc;
  }
  
  public String getPlataformaEnc()
  {
    return this.plataformaEnc;
  }
  
  public void setPlataformaEnc(String plataformaEnc)
  {
    this.plataformaEnc = plataformaEnc;
  }
  
  public String getEstado()
  {
    return this.estado;
  }
  
  public void setEstado(String estado)
  {
    this.estado = estado;
  }
  
  public String getCiudad()
  {
    return this.ciudad;
  }
  
  public void setCiudad(String ciudad)
  {
    this.ciudad = ciudad;
  }
  
  public String getCalle()
  {
    return this.calle;
  }
  
  public void setCalle(String calle)
  {
    this.calle = calle;
  }
  
  public String getNumExt()
  {
    return this.numExt;
  }
  
  public void setNumExt(String numExt)
  {
    this.numExt = numExt;
  }
  
  public String getNumInt()
  {
    return this.numInt;
  }
  
  public void setNumInt(String numInt)
  {
    this.numInt = numInt;
  }
  
  public String getColonia()
  {
    return this.colonia;
  }
  
  public void setColonia(String colonia)
  {
    this.colonia = colonia;
  }
  
  public String getCodigoPostal()
  {
    return this.codigoPostal;
  }
  
  public void setCodigoPostal(String codigoPostal)
  {
    this.codigoPostal = codigoPostal;
  }
  
  public String getNumTarjeta()
  {
    return this.numTarjeta;
  }
  
  public void setNumTarjeta(String numTarjeta)
  {
    this.numTarjeta = numTarjeta;
  }
  
  public String getTipoTarjeta()
  {
    return this.tipoTarjeta;
  }
  
  public void setTipoTarjeta(String tipoTarjeta)
  {
    this.tipoTarjeta = tipoTarjeta;
  }
  
  public String getCodigoPostalAMEX()
  {
    return this.codigoPostalAMEX;
  }
  
  public void setCodigoPostalAMEX(String codigoPostalAMEX)
  {
    this.codigoPostalAMEX = codigoPostalAMEX;
  }
  
  public String getDomicilioAMEX()
  {
    return this.domicilioAMEX;
  }
  
  public void setDomicilioAMEX(String domicilioAMEX)
  {
    this.domicilioAMEX = domicilioAMEX;
  }
  
  public String getVigenciaTarjeta()
  {
    return this.vigenciaTarjeta;
  }
  
  public void setVigenciaTarjeta(String vigenciaTarjeta)
  {
    this.vigenciaTarjeta = vigenciaTarjeta;
  }
  
  public String getTerminos()
  {
    return this.terminos;
  }
  
  public void setTerminos(String terminos)
  {
    this.terminos = terminos;
  }
  
  public String getPlataforma()
  {
    return this.plataforma;
  }
  
  public void setPlataforma(String plataforma)
  {
    this.plataforma = plataforma;
  }
  
  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request)
  {
    ActionErrors errors = new ActionErrors();
    String keyResponse = null;
    String keyRequest = null;
    try
    {
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      keyRequest = (String)request.getSession().getAttribute("jCryptionKey_");
      String nextPage = (String)request.getAttribute("nextPage");
      this.log.info("DatosComplementariosDosForm validate");
      if ((nextPage == null) || (!nextPage.equals("goToDatosComplementariosDos"))) {
        if ((GenericValidator.isBlankOrNull(keyResponse)) || (GenericValidator.isBlankOrNull(keyRequest)))
        {
          errors.add(null, new ActionMessage("Application.llaveInexistente"));
        }
        else
        {
          if (GenericValidator.isBlankOrNull(getEstadoEnc())) {
            errors.add("estadoEnc", new ActionMessage("ComplementariosDosAction.estadoEnc"));
          }
          if (GenericValidator.isBlankOrNull(getCiudadEnc())) {
            errors.add("ciudadEnc", new ActionMessage("ComplementariosDosAction.ciudadEnc"));
          }
          if (GenericValidator.isBlankOrNull(getCalleEnc())) {
            errors.add("calleEnc", new ActionMessage("ComplementariosDosAction.calleEnc"));
          }
          if (GenericValidator.isBlankOrNull(getNumExtEnc())) {
            errors.add("numExtEnc", new ActionMessage("ComplementariosDosAction.numExtEnc"));
          }
          if (GenericValidator.isBlankOrNull(getColoniaEnc())) {
            errors.add("coloniaEnc", new ActionMessage("ComplementariosDosAction.coloniaEnc"));
          }
          if (GenericValidator.isBlankOrNull(getNumTarjetaEnc())) {
            errors.add("numTarjetaEnc", new ActionMessage("ComplementariosDosAction.numTarjetaEnc"));
          }
          if (GenericValidator.isBlankOrNull(getTipoTarjetaEnc())) {
            errors.add("tipoTarjetaEnc", new ActionMessage("ComplementariosDosAction.tipoTarjetaEnc"));
          }
          if (GenericValidator.isBlankOrNull(getVigenciaTarjetaEnc())) {
            errors.add("vigenciaTarjetaEnc", new ActionMessage("ComplementariosDosAction.vigenciaTarjetaEnc"));
          }
          if (GenericValidator.isBlankOrNull(getTerminosEnc())) {
            errors.add("terminosEnc", new ActionMessage("ComplementariosDosAction.terminosEnc"));
          }
          if (GenericValidator.isBlankOrNull(getPlataformaEnc())) {
            errors.add("plataformaEnc", new ActionMessage("ComplementariosDosAction.plataformaEnc"));
          }
          if (errors.size() == 0)
          {
            setEstado(AesCtr.decrypt(getEstadoEnc(), keyRequest, 256));
            setCiudad(AesCtr.decrypt(getCiudadEnc(), keyRequest, 256));
            setCalle(AesCtr.decrypt(getCalleEnc(), keyRequest, 256));
            setNumExt(AesCtr.decrypt(getNumExtEnc(), keyRequest, 256));
            setNumInt(AesCtr.decrypt(getNumIntEnc(), keyRequest, 256));
            setColonia(AesCtr.decrypt(getColoniaEnc(), keyRequest, 256));
            setCodigoPostal(AesCtr.decrypt(getCodigoPostalEnc(), keyRequest, 256));
            setNumTarjeta(AesCtr.decrypt(getNumTarjetaEnc(), keyRequest, 256));
            setTipoTarjeta(AesCtr.decrypt(getTipoTarjetaEnc(), keyRequest, 256));
            setCodigoPostalAMEX(AesCtr.decrypt(getCodigoPostalAMEXEnc(), keyRequest, 256));
            setDomicilioAMEX(AesCtr.decrypt(getDomicilioAMEXEnc(), keyRequest, 256));
            setVigenciaTarjeta(AesCtr.decrypt(getVigenciaTarjetaEnc(), keyRequest, 256));
            setTerminos(AesCtr.decrypt(getTerminosEnc(), keyRequest, 256));
            setPlataforma(AesCtr.decrypt(getPlataformaEnc(), keyRequest, 256));
            setFlujo(AesCtr.decrypt(getFlujoEnc(), keyRequest, 256));
            if (GenericValidator.isBlankOrNull(getEstado())) {
              errors.add("estadoEnc", new ActionMessage("ComplementariosDosAction.estadoEnc"));
            }
            if (GenericValidator.isBlankOrNull(getCiudad())) {
              errors.add("ciudadEnc", new ActionMessage("ComplementariosDosAction.ciudadEnc"));
            }
            if (GenericValidator.isBlankOrNull(getCalle())) {
              errors.add("calleEnc", new ActionMessage("ComplementariosDosAction.calleEnc"));
            }
            if (GenericValidator.isBlankOrNull(getNumExt())) {
              errors.add("numExtEnc", new ActionMessage("ComplementariosDosAction.numExtEnc"));
            }
            if (GenericValidator.isBlankOrNull(getColonia())) {
              errors.add("coloniaEnc", new ActionMessage("ComplementariosDosAction.coloniaEnc"));
            }
            if (GenericValidator.isBlankOrNull(getNumTarjeta())) {
              errors.add("numTarjetaEnc", new ActionMessage("ComplementariosDosAction.numTarjetaEnc"));
            }
            if (GenericValidator.isBlankOrNull(getTipoTarjeta())) {
              errors.add("tipoTarjetaEnc", new ActionMessage("ComplementariosDosAction.tipoTarjetaEnc"));
            }
            if ((getTipoTarjeta() != null) && (getTipoTarjeta().equals("3")))
            {
              if (GenericValidator.isBlankOrNull(getDomicilioAMEX())) {
                errors.add("domicilioAMEXEnc", new ActionMessage("ComplementariosDosAction.domicilioAMEXEnc"));
              }
              if (GenericValidator.isBlankOrNull(getCodigoPostalAMEX())) {
                errors.add("codigoPostalAMEXEnc", new ActionMessage("ComplementariosDosAction.codigoPostalAMEXEnc"));
              }
            }
            if (GenericValidator.isBlankOrNull(getVigenciaTarjeta())) {
              errors.add("vigenciaTarjetaEnc", new ActionMessage("ComplementariosDosAction.vigenciaTarjetaEnc"));
            }
            if (GenericValidator.isBlankOrNull(getTerminos())) {
              errors.add("terminosEnc", new ActionMessage("ComplementariosDosAction.terminosEnc"));
            }
            if (GenericValidator.isBlankOrNull(getPlataforma())) {
              errors.add("plataformaEnc", new ActionMessage("ComplementariosDosAction.plataformaEnc"));
            }
          }
          if (errors.size() == 0)
          {
            if (!getEstado().matches("^\\d{1,2}$")) {
              errors.add("estadoEnc", new ActionMessage("ComplementariosDosAction.estadoEnc.format"));
            }
            if (!getTipoTarjeta().matches("^\\d{1}$")) {
              errors.add("tipoTarjetaEnc", new ActionMessage("ComplementariosDosAction.tipoTarjetaEnc.format"));
            }
            if ((getTipoTarjeta() != null) && (getTipoTarjeta().equals("3")))
            {
              if (!getNumTarjeta().matches("^\\d{15}$")) {
                errors.add("numTarjetaEnc", new ActionMessage("ComplementariosDosAction.numTarjetaEnc.format"));
              }
            }
            else if (!getNumTarjeta().matches("^\\d{16}$")) {
              errors.add("numTarjetaEnc", new ActionMessage("ComplementariosDosAction.numTarjetaEnc.format"));
            }
            if (!getVigenciaTarjeta().matches("^\\d{2}/\\d{2}$")) {
              errors.add("vigenciaTarjetaEnc", new ActionMessage("ComplementariosDosAction.vigenciaTarjetaEnc.format"));
            }
            if (!getTerminos().matches("^[S]$")) {
              errors.add("terminosEnc", new ActionMessage("ComplementariosDosAction.terminosEnc.format"));
            }
          }
          if (errors.size() != 0)
          {
            request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
            StringBuilder sb = new StringBuilder();
            Iterator<ActionMessage> iter = errors.get();
            PropertyMessageResources p = (PropertyMessageResources)request.getAttribute("org.apache.struts.action.MESSAGE");
            while (iter.hasNext())
            {
              ActionMessage msg = (ActionMessage)iter.next();
              sb.append(StrutsMensajes.getErrorMessage(p, msg));
              sb.append("<br/>");
            }
            this.log.info(sb);
            this.log.info(toString());
            this.log.info(this.estado);
            this.log.info(this.ciudad);
            this.log.info(this.calle);
            this.log.info(this.numExt);
            this.log.info(this.numInt);
            this.log.info(this.colonia);
            this.log.info(this.codigoPostal);
            this.log.info(this.numTarjeta);
            this.log.info(this.tipoTarjeta);
            this.log.info(this.codigoPostalAMEX);
            this.log.info(this.domicilioAMEX);
            this.log.info(this.vigenciaTarjeta);
            this.log.info(this.terminos);
            this.log.info(this.plataforma);
            request.setAttribute("errorMensaje", AesCtr.encrypt(sb.toString(), keyResponse, 256));
          }
        }
      }
    }
    catch (Exception e)
    {
      this.log.error("Exeption en validacion DatosComplementariosDosForm : ", e);
      if (request.getAttribute("llavePublica") != null) {
        request.removeAttribute("llavePublica");
      }
      request.setAttribute("errorMensaje", "Error inesperado en la validaci&oacute;n");
    }
    return errors;
  }
  
  public String getFlujoEnc()
  {
    return this.flujoEnc;
  }
  
  public void setFlujoEnc(String flujoEnc)
  {
    this.flujoEnc = flujoEnc;
  }
  
  public String getFlujo()
  {
    return this.flujo;
  }
  
  public void setFlujo(String flujo)
  {
    this.flujo = flujo;
  }
}


/* Location:              /home/rhtrejo/workspace/RecargaIAVE/RecargaIAVE.war!/WEB-INF/classes/mx/addcel/ServiceIAVE/form/DatosComplementariosDosForm.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */