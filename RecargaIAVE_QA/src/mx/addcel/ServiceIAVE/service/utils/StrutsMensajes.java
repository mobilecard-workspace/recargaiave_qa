package mx.addcel.ServiceIAVE.service.utils;

import java.util.Locale;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.util.PropertyMessageResources;

public class StrutsMensajes
{
	protected static final Locale defaultLocale = Locale.getDefault();
  private String arg0 = null;
  private String arg1 = null;
  private String arg2 = null;
  private String arg3 = null;
  protected String key = null;
  
  public static String getErrorMessage(PropertyMessageResources p, ActionMessage msg)
  {
    String text = p.getMessage(defaultLocale, msg.getKey(), msg.getValues());
    System.out.println(text);
    return text;
  }
  
  public String getArg0()
  {
    return this.arg0;
  }
  
  public void setArg0(String arg0)
  {
    this.arg0 = arg0;
  }
  
  public String getArg1()
  {
    return this.arg1;
  }
  
  public void setArg1(String arg1)
  {
    this.arg1 = arg1;
  }
  
  public String getArg2()
  {
    return this.arg2;
  }
  
  public void setArg2(String arg2)
  {
    this.arg2 = arg2;
  }
  
  public String getArg3()
  {
    return this.arg3;
  }
  
  public void setArg3(String arg3)
  {
    this.arg3 = arg3;
  }
}


/* Location:              /home/rhtrejo/workspace/RecargaIAVE/RecargaIAVE.war!/WEB-INF/classes/mx/addcel/ServiceIAVE/service/utils/StrutsMensajes.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */