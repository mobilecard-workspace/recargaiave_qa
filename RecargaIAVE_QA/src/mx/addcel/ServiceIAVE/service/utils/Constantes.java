package mx.addcel.ServiceIAVE.service.utils;

public class Constantes
{
  public static final String PASS_ADDCEL = "1234567890ABCDEF0123456789ABCDEF";
  public static final String PASS_NEWUSER = "mCL8m39sJ1";
  public static final String PASS_ENCTDC = "5525963513";
  public static final String key = "1234567890ABCDEF0123456789ABCDEF";
  
  //TIPOS DE TARJETA DE CREDITO
  public static final int VISA = 1;
  public static final int MASTER_CARD = 2;
  public static final int AMEX = 3;
  
  //Constantes PROSA
  public static final String CURRENCY = "484"; //Moneda MXN
  public static final String ADDRESS = "PROSA"; //domicilio del negocio
  public static final String MERCHANT = "8039159"; //afiliacion bancaria  test "7627488";//
  public static final String STORE = "1234"; //tienda
  public static final String TERM = "001"; //terminal
  //public static final String URLBACK = "http://50.57.192.214:8080/RecargaIAVE/servicioIAVE_.do?method=respuestaProsa3Ds"; //url de respuesta QA
 // public static final String URLBACK = "https://www.mobilecard.mx:8443/RecargaIAVE_test/servicioIAVE_.do?method=respuestaProsa3Ds"; //url de respuesta PROD
  public static final String URLBACK = "https://www.mobilecard.mx:8443/RecargaIAVE_test/servicioIAVE_.do?method=respuestaProsa3Ds";
  //public static final String URLBACK = "http://199.231.161.36:8080/RecargaIAVE_test/servicioIAVE_.do?method=respuestaProsa3Ds"; // TODO //url de respuesta PROD
  
}

