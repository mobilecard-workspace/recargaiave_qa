package mx.addcel.ServiceIAVE.service.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.ironbit.mc.system.crypto.Crypto;

import javacryption.aes.AesCtr;
import mx.addcel.ServiceIAVE.service.ConsultasService;
import mx.addcel.ServiceIAVE.service.ConsumeServiciosService;
import mx.addcel.ServiceIAVE.service.vo.CompraResponceVO;
import mx.addcel.ServiceIAVE.service.vo.DatosIAVE_VO;
import mx.addcel.ServiceIAVE.service.vo.Producto;
import mx.addcel.ServiceIAVE.service.vo.ReglasVO;

public class Utils
{
  private static Logger log = Logger.getLogger(Utils.class);
  
  public static String parsePass(String pass)
  {
    int len = pass.length();
    String key = "";
    for (int i = 0; i < 32 / len; i++) {
      key = key + pass;
    }
    int carry = 0;
    while (key.length() < 32)
    {
      key = key + pass.charAt(carry);
      carry++;
    }
    return key;
  }
  
  public static String mergeStr(String first, String second)
  {
    String result = "";
    String other = reverse(second);
    result = result + (Integer.toString(other.length()).length() < 2 ? "0" + other.length() : Integer.toString(other.length()));
    String sub1 = first.substring(0, 19);
    String sub2 = first.substring(19, first.length());
    result = result + sub1;
    for (int i = 0; i < other.length(); i += 2)
    {
      int offset = i + 2 <= other.length() ? i + 2 : i + 1;
      String next = other.substring(i, offset);
      result = result + next;
      result = result + sub2.substring(0, 2);
      sub2 = sub2.substring(2);
    }
    result = result + sub2;
    
    return result;
  }
  
  public static String reverse(String chain)
  {
    String nuevo = "";
    for (int i = chain.length() - 1; i >= 0; i--) {
      nuevo = nuevo + chain.charAt(i);
    }
    return nuevo;
  }
  
  public static String[] validacionLogin(ResourceBundle bundle, ConsumeServiciosService consumeServiciosService, String imei, String login, String password, boolean seguro)
    throws Exception
  {
    String urlBase = null;
    String servicio = null;
    String cadenaOriginal = null;
    String cadenaDesencriptada = null;
    String protocol = "http://";
    JSONObject jsonObj = null;
    String jEnc = null;
    String newEnc = null;
    HashMap parametros = null;
    String[] resultado = null;
    log.info("validacionLogin");
    try
    {
      jsonObj = new JSONObject();
      jsonObj.put("login", login);
      jsonObj.put("passwordS", Crypto.sha1(password));
      jsonObj.put("password", password);
      jsonObj.put("imei", "IAVE" + imei);
      jsonObj.put("tipo", "tuTag_IAVE");
      jsonObj.put("software", "1.1.1");
      jsonObj.put("modelo", "web");
      jsonObj.put("key", imei);
      if (seguro) {
        protocol = "https://";
      }
      log.info(jsonObj);
      jEnc = Crypto.aesEncrypt(parsePass(password), jsonObj.toString());
      newEnc = mergeStr(jEnc, password);
      
      urlBase = bundle.getString("urlbase");
      servicio = bundle.getString("loginUrl");
      log.info(protocol + urlBase + servicio + "?json=" + newEnc);
      parametros = new HashMap();
      parametros.put("json", newEnc);
      cadenaOriginal = consumeServiciosService.loginCorrecto(protocol + urlBase + servicio, parametros);
      if (((cadenaOriginal != null ? 1 : 0) & (cadenaOriginal.length() > 0 ? 1 : 0)) != 0)
      {
        log.info("cadenaOriginal : " + cadenaOriginal);
        cadenaDesencriptada = Crypto.aesDecrypt(parsePass(password), cadenaOriginal);
        log.info("cadenaDesencriptada : " + cadenaDesencriptada);
        jsonObj = new JSONObject(cadenaDesencriptada);
        resultado = new String[2];
        resultado[0] = Integer.toString(((Integer)jsonObj.get("resultado")).intValue());
        resultado[1] = ((String)jsonObj.get("mensaje"));
      }
      else
      {
        resultado = null;
      }
    }
    catch (Exception e)
    {
      log.error("error al validar login : ", e);
      throw e;
    }
    return resultado;
  }
  
  public static String obtenDatosRecargaIAVE(ResourceBundle bundle, ConsumeServiciosService consumeServiciosService, ConsultasService consultasService, String keyResponse, HttpServletRequest request)
    throws Exception
  {
    String urlBase = null;
    String servicio = null;
    String cadenaOriginal = null;
    String cadenaDesencriptada = null;
    String protocol = "http://";
    String forward = null;
    
    Producto p = null;
    JSONObject json = null;
    String jsonStr = null;
    HashMap parametros = null;
    String comision = null;
    String tipoTdc = null; 
    
    try
    {
      urlBase = bundle.getString("urlbase");
      servicio = bundle.getString("servicioCatalogoProductosURL");
      json = new JSONObject();
      json.put("proveedor", "IAVE_TuTag");
      log.info(json.toString());
      jsonStr = Crypto.aesEncrypt("1234567890ABCDEF0123456789ABCDEF", json.toString());
      parametros = new HashMap();
      parametros.put("json", jsonStr);
      
      if (request.isSecure()) {
          protocol = "https://";
        }
      
      cadenaOriginal = consumeServiciosService.getCatalogo(protocol + urlBase + servicio, parametros);
      
      if ((cadenaOriginal != null) && (cadenaOriginal.length() > 0))
      {
        log.info("cadenaOriginal : " + cadenaOriginal);
        cadenaDesencriptada = Crypto.aesDecrypt("1234567890ABCDEF0123456789ABCDEF", cadenaOriginal);
        log.info("cadenaDesencriptada : " + cadenaDesencriptada);
        DatosIAVE_VO datosIAVE = (DatosIAVE_VO)request.getSession().getAttribute("datosIAVE");
        log.info(datosIAVE);
        
        comision = consultasService.obtenComision("IAVE_TuTag");
        tipoTdc = consultasService.tdcTipo((String)request.getSession().getAttribute("loginMobileCard"));
        
        request.setAttribute("aliasTag", AesCtr.encrypt(datosIAVE.getNombreTag(), keyResponse, 256));
        request.setAttribute("tag", AesCtr.encrypt(datosIAVE.getTag(), keyResponse, 256));
        request.setAttribute("login", AesCtr.encrypt((String)request.getSession().getAttribute("loginMobileCard"), keyResponse, 256));
        request.setAttribute("productos", AesCtr.encrypt(cadenaDesencriptada, keyResponse, 256));
        request.setAttribute("comision", AesCtr.encrypt(comision, keyResponse, 256));
        request.setAttribute("tipoTDC", tipoTdc);
        request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
        
        
        forward = "recargaIAVE";
      }
      else
      {
        request.setAttribute("titulo", "Error");
        request.setAttribute("errorMensaje", "Error de comunicación, intentelo nuevamente.");
        forward = "error";
      }
    }
    catch (Exception e)
    {
      log.error("error al obtener datos para recargaIAVE", e);
      throw e;
    }
    return forward;
  }
  
  public static CompraResponceVO validaReglasNegocio(long idUsuario, String tarjeta, int idProducto, ConsultasService consultasService) {
	  
	  log.info("Validando Reglas de negocio");
		
		CompraResponceVO compraResponce = null;
		int numUsuario = 0;
		int montoUsuario = 0;
		int numTarjeta = 0;
		int montoTarjeta = 0;
		boolean isBinNacional = false;

		ReglasVO valoresU = null;
		ReglasVO valoresT = null;
		try {
			log.info("Consultando Reglas de negocio");
			
			numUsuario = Integer.parseInt(consultasService.getParametro("@IMASD_NUM_TRAN_USUARIO"));
			montoUsuario = Integer.parseInt(consultasService.getParametro("@IMASD_MONTO_USUARIO"));
			numTarjeta = Integer.parseInt(consultasService.getParametro("@IMASD_NUM_TRAN_TARJETA"));
			montoTarjeta = Integer.parseInt(consultasService.getParametro("@IMASD_MONTO_TARJETA"));

			valoresU = consultasService.selectRuleU(idUsuario, "5,8,17", "@IMASD_DIAS_TRAN_USUARIO");
			valoresT = consultasService.selectRuleUt(0L, tarjeta, "5,8,17", "@IMASD_DIAS_TRAN_TARJETA");
			
			//validar que el bin sea valido segun la lista recibida de hsbc
			isBinNacional = consultasService.isBinNacional(Long.parseLong(Crypto.aesDecrypt(Utils.parsePass("5525963513"),tarjeta).substring(0, 6)));
			
			if (valoresU.getNumero() >= numUsuario) {
				compraResponce = new CompraResponceVO(0, "920",
						"En el periodo " + valoresU.getFechaInicio() + " a " + valoresU.getFechaFin()
								+ " a superado el número maximo de transacciones permitidas: " + numUsuario);
				
				log.info("En el periodo " + valoresU.getFechaInicio() + " a " + valoresU.getFechaFin()
				+ " a superado el número maximo de transacciones permitidas: " + numUsuario);
				
			} else if (valoresU.getMonto() >= montoUsuario) {
				compraResponce = new CompraResponceVO(0, "921",
						"En el periodo " + valoresU.getFechaInicio() + " a " + valoresU.getFechaFin()
								+ " a superado el Monto maximo permitido: $ " + montoUsuario);
				
				log.info("En el periodo " + valoresU.getFechaInicio() + " a " + valoresU.getFechaFin()
				+ " a superado el Monto maximo permitido: $ " + montoUsuario);
				
			} else if (valoresT.getNumero() >= numTarjeta) {
				compraResponce = new CompraResponceVO(0, "922",
						"En el periodo " + valoresT.getFechaInicio() + " a " + valoresT.getFechaFin()
								+ " a superado el número maximo de transacciones permitidas para una Tarjeta: "
								+ numTarjeta);
				
				log.info("En el periodo " + valoresT.getFechaInicio() + " a " + valoresT.getFechaFin()
				+ " a superado el número maximo de transacciones permitidas para una Tarjeta: "
				+ numTarjeta);
				
			} else if (valoresT.getMonto() >= montoTarjeta) {
				compraResponce = new CompraResponceVO(0, "923",
						"En el periodo " + valoresT.getFechaInicio() + " a " + valoresT.getFechaFin()
								+ " a superado el Monto maximo permitido para una Tarjeta: $ "
								+ montoTarjeta);
				
				log.info("En el periodo " + valoresT.getFechaInicio() + " a " + valoresT.getFechaFin()
								+ " a superado el Monto maximo permitido para una Tarjeta: $ "
								+ montoTarjeta);
				
			} else if (!isBinNacional) {
				compraResponce = new CompraResponceVO(0, "924","Solo se aceptan tarjetas Nacionales");
				
				log.info("Solo se aceptan tarjetas Nacionales");
			}
			
		} catch (Exception e) {
			log.error("Ocurrio un error al validar Reglas de Negocio.", e);

			compraResponce = new CompraResponceVO(0, "920", "Ocurrio un error al validar Reglas de Negocio.");
		}
		return compraResponce;
	}  
  
}