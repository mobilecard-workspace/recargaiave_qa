package mx.addcel.ServiceIAVE.service.vo;

import java.io.Serializable;
import java.lang.reflect.Method;

public abstract class AbstractVO
  implements Serializable
{
  public String toString()
  {
    StringBuffer resultado = new StringBuffer();
    Class myClass = getClass();
    Method[] metodos = myClass.getDeclaredMethods();
    int methodsLength = metodos.length;
    try
    {
      for (int i = 0; i < methodsLength; i++) {
        if (metodos[i].getParameterTypes().length == 0)
        {
          resultado.append(" ");
          resultado.append(metodos[i].getName());
          resultado.append(": ");
          resultado.append(metodos[i].invoke(this, null));
          resultado.append(" \n");
        }
      }
    }
    catch (Exception e) {}
    return resultado.toString();
  }
}


/* Location:              /home/rhtrejo/workspace/RecargaIAVE/RecargaIAVE.war!/WEB-INF/classes/mx/addcel/ServiceIAVE/service/vo/AbstractVO.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */