/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.addcel.ServiceIAVE.service.vo;

/**
 *
 * @author david
 */
public class CompraResponceVO {
    private String folio;
    private int resultado;
    private String mensaje;
    
    private String numAutorizacion; 
    private long referencia;
    private double cargo;
    private double comision;
    
    private String claveRechazo;
    private String descripcionRechazo;
    
    public CompraResponceVO(){
        
    }
    
    public CompraResponceVO(int resultado, String folio, String mensaje){
        this.resultado = resultado;
        this.folio = folio;
        this.mensaje = mensaje;
    }
    
    /**
     * @return the resultado
     */
    public int getResultado() {
        return resultado;
    }

    /**
     * @param resultado the resultado to set
     */
    public void setResultado(int resultado) {
        this.resultado = resultado;
    }

    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /**
     * @return the folio
     */
    public String getFolio() {
        return folio;
    }

    /**
     * @param folio the folio to set
     */
    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getNumAutorizacion() {
        return numAutorizacion;
    }
    public void setNumAutorizacion(String numAutorizacion) {
        this.numAutorizacion = numAutorizacion;
}
    public long getReferencia() {
        return referencia;
    }
    public void setReferencia(long referencia) {
        this.referencia = referencia;
    }
    public double getCargo() {
        return cargo;
    }
    public void setCargo(double cargo) {
        this.cargo = cargo;
    }
    public double getComision() {
        return comision;
    }
    public void setComision(double comision) {
        this.comision = comision;
    }
    
    public String getClaveRechazo() {
        return claveRechazo;
    }

    /**
     * Define el valor de la propiedad claveRechazo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveRechazo(String value) {
        this.claveRechazo = value;
    }

    /**
     * Obtiene el valor de la propiedad descripcionRechazo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcionRechazo() {
        return descripcionRechazo;
    }

    /**
     * Define el valor de la propiedad descripcionRechazo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcionRechazo(String value) {
        this.descripcionRechazo = value;
    }
}
