package mx.addcel.ServiceIAVE.service.vo;

public class DatosIAVE_VO
  extends UsuarioVO
{
  private String iduser;
  private String dn;
  private String email;
  private String imei;
  private String tag;
  private String pin;
  private String nombreTag;
  
  public String getIduser()
  {
    return this.iduser;
  }
  
  public void setIduser(String iduser)
  {
    this.iduser = iduser;
  }
  
  public String getDn()
  {
    return this.dn;
  }
  
  public void setDn(String dn)
  {
    this.dn = dn;
  }
  
  public String getEmail()
  {
    return this.email;
  }
  
  public void setEmail(String email)
  {
    this.email = email;
  }
  
  public String getImei()
  {
    return this.imei;
  }
  
  public void setImei(String imei)
  {
    this.imei = imei;
  }
  
  public String getTag()
  {
    return this.tag;
  }
  
  public void setTag(String tag)
  {
    this.tag = tag;
  }
  
  public String getNombreTag()
  {
    return this.nombreTag;
  }
  
  public void setNombreTag(String nombreTag)
  {
    this.nombreTag = nombreTag;
  }
  
  public String getPin()
  {
    return this.pin;
  }
  
  public void setPin(String pin)
  {
    this.pin = pin;
  }
}

