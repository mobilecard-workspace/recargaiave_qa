package mx.addcel.ServiceIAVE.service;

import java.util.HashMap;
import mx.addcel.ServiceIAVE.dao.ConsumeServiciosDAO;

public class ConsumeServiciosService
{
  private ConsumeServiciosDAO consumeServiciosDAO;
  
  public String getCatalogo(String urlServicio, HashMap parametros)
  {
    return this.consumeServiciosDAO.consumeServicio(urlServicio, parametros);
  }
  
  public String loginCorrecto(String urlServicio, HashMap parametros)
  {
    return this.consumeServiciosDAO.consumeServicio(urlServicio, parametros);
  }
  
  public String registraUsuario(String urlServicio, HashMap parametros)
  {
    return this.consumeServiciosDAO.consumeServicio(urlServicio, parametros);
  }
  
  public String actualizaUsuario(String urlServicio, HashMap parametros)
  {
    return this.consumeServiciosDAO.consumeServicio(urlServicio, parametros);
  }
  
  public String compraIAVE(String urlServicio, HashMap parametros)
  {
    return this.consumeServiciosDAO.consumeServicio(urlServicio, parametros);
  }
  
  public String cambiaPass(String urlServicio, HashMap parametros)
  {
    return this.consumeServiciosDAO.consumeServicio(urlServicio, parametros);
  }
  
  public void setConsumeServiciosDAO(ConsumeServiciosDAO consumeServiciosDAO)
  {
    this.consumeServiciosDAO = consumeServiciosDAO;
  }
}


/* Location:              /home/rhtrejo/workspace/RecargaIAVE/RecargaIAVE.war!/WEB-INF/classes/mx/addcel/ServiceIAVE/service/ConsumeServiciosService.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */