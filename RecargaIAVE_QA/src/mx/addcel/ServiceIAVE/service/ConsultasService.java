package mx.addcel.ServiceIAVE.service;

import java.util.HashMap;

import com.ironbit.mc.system.crypto.Crypto;

import mx.addcel.ServiceIAVE.dao.ConsultasDAO;
import mx.addcel.ServiceIAVE.service.utils.Utils;
import mx.addcel.ServiceIAVE.service.vo.ReglasVO;
import mx.addcel.ServiceIAVE.service.vo.TBitacoraVO;
import mx.addcel.ServiceIAVE.service.vo.UsuarioVO;

public class ConsultasService
{
  private ConsultasDAO consultasDAO;
  
  public void setConsultasDAO(ConsultasDAO consultasDAO)
  {
    this.consultasDAO = consultasDAO;
  }
  
  public String getMapeoIAVE(String idIAVE)
  {
    return this.consultasDAO.existeUsuarioAsociado(idIAVE);
  }
  
  public Integer existeUsuario(String login)
  {
    return this.consultasDAO.existeUsuario(login);
  }
  
  public String existeTDC(String tdc)
  {
    String tdcEnc = Crypto.aesEncrypt(Utils.parsePass("5525963513"), tdc);
    return this.consultasDAO.existeTDC(tdcEnc);
  }
  
  public String existeEmail(String email)
  {
    return this.consultasDAO.existeEmail(email);
  }
  
  public String existeTelefono(String dn)
  {
    return this.consultasDAO.existeTelefono(dn);
  }
  
  public String existeIMEI(String imei)
  {
    return this.consultasDAO.existeIMEI("IAVE" + imei);
  }
  
  public void insertaMapeoUsuario(String loginIAVE, String loginMobileCard)
  {
    HashMap map = new HashMap();
    map.put("id_IAVE", loginIAVE);
    map.put("loginMobileCard", loginMobileCard);
    this.consultasDAO.insertaMapeoIAVE(map);
  }
  
  public String tdcVigencia(String loginIAVE)
  {
    return this.consultasDAO.tdcVigencia(loginIAVE);
  }
  
  public String obtenComision(String proveedor)
  {
    return this.consultasDAO.obtenComision(proveedor);
  }
  
  public String obtenMontoRecarga(String producto)
  {
    return this.consultasDAO.obtenMontoRecarga(producto);
  }
  
  public String validaStatusUsuario(String login)
  {
    return this.consultasDAO.validaStatusUsuario(login);
  }
  
  public void desvinculaCuentas(String idUserIAVE)
  {
    this.consultasDAO.desvinculaCuentas(idUserIAVE);
  }
  
  public UsuarioVO consultaUsuario(String login)
  {
    return this.consultasDAO.consultaUsuario(login);
  }
  
  public UsuarioVO consultaUsuarioByIdBitacora(long idBitacora)
  {
    return this.consultasDAO.consultaUsuarioByIdBitacora(idBitacora);
  }
  
  public String tdcTipo(String login)
  {
    return this.consultasDAO.tdcTipo(login);
  }
  
  public String getParametro(String key){
	  return this.consultasDAO.getParametro(key);
  }
  
  public ReglasVO selectRuleU(long idUsuario, String idProveedores, String parametro)
  {
    HashMap map = new HashMap();
    map.put("id_Usuario", idUsuario);
    map.put("proveedores", idProveedores);
    map.put("key", parametro);
        
    return this.consultasDAO.selectRuleU(map);
  }
  
  public ReglasVO selectRuleUt(long idUsuario, String tarjeta, String idProveedores, String parametro)
  {
    HashMap map = new HashMap();
    map.put("id_Usuario", idUsuario);
    map.put("proveedores", idProveedores);
    map.put("key", parametro);
    map.put("tdc", tarjeta);
    
    return this.consultasDAO.selectRuleUt(map);
  }
  
  public long insertBitacora(TBitacoraVO tBitacoraVO){
	  return this.consultasDAO.insertBitacora(tBitacoraVO);
  }
  
  public double getComisionByIdProveedor(long idProveedor){
	  return this.consultasDAO.getComisionByIdProveedor(idProveedor);
  }
  
  public boolean isBinNacional(long binBajo){
	  return this.consultasDAO.isBinNacional(binBajo);
  }
  
}