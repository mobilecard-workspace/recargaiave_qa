package mx.addcel.ServiceIAVE.action;

import com.ironbit.mc.system.crypto.Crypto;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.ResourceBundle;
import javacryption.aes.AesCtr;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.addcel.ServiceIAVE.form.DatosComplementariosUnoForm;
import mx.addcel.ServiceIAVE.service.ConsultasService;
import mx.addcel.ServiceIAVE.service.ConsumeServiciosService;
import mx.addcel.ServiceIAVE.service.utils.Utils;
import mx.addcel.ServiceIAVE.service.vo.DatosIAVE_VO;
import mx.addcel.ServiceIAVE.service.vo.UsuarioVO;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.json.JSONObject;

public class ComplementariosUnoAction
  extends DispatchAction
{
  private Logger log = Logger.getLogger(ComplementariosUnoAction.class);
  private ConsumeServiciosService consumeServiciosService;
  private ConsultasService consultasService;
  private ResourceBundle bundle = ResourceBundle.getBundle("servicios");
  
  public ActionForward inicio(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    String urlBase = null;
    String servicio = null;
    String cadenaOriginal = null;
    String cadenaDesencriptada = null;
    String protocol = "http://";
    String keyResponse = null;
    String proveedores = null;
    String forward = null;
    DatosIAVE_VO datosIAVE = null;
    Calendar cal = null;
    String nuevoLogin = null;
    String flujo = null;
    String passUserMobilecard = null;
    String loginMobilecard = null;
    JSONObject jsonObj = null;
    String jEnc = null;
    String newEnc = null;
    HashMap parametros = null;
    UsuarioVO usuarioVO = null;
    JSONObject fechaNacimiento = null;
    DateFormat df = null;
    try
    {
      this.log.info("Entro a ComplementariosUnoAction inicio");
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      if ((keyResponse == null) || (keyResponse.length() == 0))
      {
        request.setAttribute("errorMensaje", "Error al validar session");
        forward = "error";
      }
      else
      {
        if (request.isSecure()) {
          protocol = "https://";
        }
        datosIAVE = (DatosIAVE_VO)request.getSession().getAttribute("datosIAVE");
        flujo = (String)request.getAttribute("flujo");
        this.log.info(datosIAVE);
        urlBase = this.bundle.getString("urlbase");
        servicio = this.bundle.getString("servicioCatalogoProvidersURL");
        cadenaOriginal = this.consumeServiciosService.getCatalogo(protocol + urlBase + servicio, null);
        if ((cadenaOriginal != null) && (cadenaOriginal.length() > 0))
        {
          this.log.info("cadenaOriginal : " + cadenaOriginal);
          cadenaDesencriptada = Crypto.aesDecrypt("1234567890ABCDEF0123456789ABCDEF", cadenaOriginal);
          this.log.info("cadenaDesencriptada : " + cadenaDesencriptada);
          proveedores = AesCtr.encrypt(cadenaDesencriptada, keyResponse, 256);
          if ((flujo != null) && (flujo.equalsIgnoreCase("REGISTRO")))
          {
            nuevoLogin = datosIAVE.getEmail().substring(0, datosIAVE.getEmail().indexOf("@"));
            if (this.consultasService.existeUsuario(nuevoLogin).intValue() > 0) {
              nuevoLogin = "";
            }
            cal = Calendar.getInstance();
            request.setAttribute("email", AesCtr.encrypt(datosIAVE.getEmail(), keyResponse, 256));
            request.setAttribute("dn", AesCtr.encrypt(datosIAVE.getDn(), keyResponse, 256));
            request.setAttribute("login", AesCtr.encrypt(nuevoLogin, keyResponse, 256));
            request.setAttribute("yearFrom", AesCtr.encrypt(Integer.toString(cal.get(1) - 90), keyResponse, 256));
            request.setAttribute("yearTo", AesCtr.encrypt(Integer.toString(cal.get(1) - 6), keyResponse, 256));
            request.setAttribute("proveedores", proveedores);
            request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
            request.setAttribute("flujo", "REGISTRO");
            forward = "datosComplementarios";
          }
          else if ((flujo != null) && (flujo.equalsIgnoreCase("UPDATE")))
          {
            urlBase = this.bundle.getString("urlbase");
            servicio = this.bundle.getString("servicioObtenInformacionUsuarioURL");
            loginMobilecard = (String)request.getSession().getAttribute("loginMobileCard");
            passUserMobilecard = (String)request.getSession().getAttribute("passUserMobilecard");
            jsonObj = new JSONObject();
            jsonObj.put("login", loginMobilecard);
            jsonObj.put("password", passUserMobilecard);
            this.log.info(jsonObj);
            jEnc = Crypto.aesEncrypt(Utils.parsePass(passUserMobilecard), jsonObj.toString());
            newEnc = Utils.mergeStr(jEnc, passUserMobilecard);
            parametros = new HashMap();
            parametros.put("json", newEnc);
            cadenaOriginal = this.consumeServiciosService.getCatalogo(protocol + urlBase + servicio, parametros);
            this.log.info("cadenaOriginal : " + cadenaOriginal);
            cadenaDesencriptada = Crypto.aesDecrypt(Utils.parsePass(passUserMobilecard), cadenaOriginal);
            this.log.info("cadenaDesencriptada : " + cadenaDesencriptada);
            jsonObj = new JSONObject(cadenaDesencriptada);
            usuarioVO = new UsuarioVO();
            usuarioVO.setNombreUsuario(loginMobilecard);
            if (jsonObj.has("telefono")) {
              usuarioVO.setNumCelular(jsonObj.getString("telefono"));
            }
            if (jsonObj.has("proveedor")) {
              usuarioVO.setProveedor(jsonObj.getString("proveedor"));
            }
            if (jsonObj.has("nombre")) {
              usuarioVO.setNombre(jsonObj.getString("nombre"));
            }
            if (jsonObj.has("apellido")) {
              usuarioVO.setApellidoP(jsonObj.getString("apellido"));
            }
            if (jsonObj.has("materno")) {
              usuarioVO.setApellidoM(jsonObj.getString("materno"));
            }
            if (jsonObj.has("nacimiento")) {
              usuarioVO.setFechaNac(jsonObj.getString("nacimiento"));
            }
            if (jsonObj.has("sexo")) {
              usuarioVO.setSexo(jsonObj.getString("sexo"));
            }
            if (jsonObj.has("tel_casa")) {
              usuarioVO.setTelefonoCasa(jsonObj.getString("tel_casa"));
            }
            if (jsonObj.has("tel_oficina")) {
              usuarioVO.setTelefonoOficina(jsonObj.getString("tel_oficina"));
            }
            if (jsonObj.has("mail")) {
              usuarioVO.setEmail(jsonObj.getString("mail"));
            }
            if (jsonObj.has("id_estado")) {
              usuarioVO.setEstado(jsonObj.getString("id_estado"));
            }
            if (jsonObj.has("ciudad")) {
              usuarioVO.setCiudad(jsonObj.getString("ciudad"));
            }
            if (jsonObj.has("calle")) {
              usuarioVO.setCalle(jsonObj.getString("calle"));
            }
            if (jsonObj.has("num_ext")) {
              usuarioVO.setNumExt(jsonObj.getString("num_ext"));
            }
            if (jsonObj.has("num_interior")) {
              usuarioVO.setNumInt(jsonObj.getString("num_interior"));
            }
            if (jsonObj.has("colonia")) {
              usuarioVO.setColonia(jsonObj.getString("colonia"));
            }
            if (jsonObj.has("cp")) {
              usuarioVO.setCodigoPostal(jsonObj.getString("cp"));
            }
            if (jsonObj.has("tarjeta")) {
              usuarioVO.setNumTarjeta(jsonObj.getString("tarjeta"));
            }
            if (jsonObj.has("tipotarjeta")) {
              usuarioVO.setTipoTarjeta(jsonObj.getString("tipotarjeta"));
            }
            if (jsonObj.has("cp")) {
              usuarioVO.setCodigoPostalAMEX(jsonObj.getString("cp"));
            }
            if (jsonObj.has("dom_amex")) {
              usuarioVO.setDomicilioAMEX(jsonObj.getString("dom_amex"));
            }
            if (jsonObj.has("vigencia")) {
              usuarioVO.setVigenciaTarjeta(jsonObj.getString("vigencia"));
            }
            if (jsonObj.has("terminos")) {
              usuarioVO.setTerminos(jsonObj.getString("terminos"));
            }
            request.getSession().setAttribute("usuarioVOUpdate", usuarioVO);
            cal = new GregorianCalendar();
            
            fechaNacimiento = new JSONObject();
            df = new SimpleDateFormat("yyyy-MM-dd");
            cal.setTime(df.parse(usuarioVO.getFechaNac()));
            fechaNacimiento.put("day", cal.get(5));
            fechaNacimiento.put("month", cal.get(2) + 1);
            fechaNacimiento.put("year", cal.get(1));
            
            cal = Calendar.getInstance();
            request.setAttribute("email", AesCtr.encrypt(usuarioVO.getEmail() != null ? usuarioVO.getEmail() : "", keyResponse, 256));
            request.setAttribute("dn", AesCtr.encrypt(usuarioVO.getNumCelular() != null ? usuarioVO.getNumCelular() : "", keyResponse, 256));
            request.setAttribute("login", AesCtr.encrypt(usuarioVO.getNombreUsuario() != null ? usuarioVO.getNombreUsuario() : "", keyResponse, 256));
            request.setAttribute("yearFrom", AesCtr.encrypt(Integer.toString(cal.get(1) - 90), keyResponse, 256));
            request.setAttribute("yearTo", AesCtr.encrypt(Integer.toString(cal.get(1) - 6), keyResponse, 256));
            request.setAttribute("fechaNacimiento", AesCtr.encrypt(fechaNacimiento.toString(), keyResponse, 256));
            request.setAttribute("nombre", AesCtr.encrypt(usuarioVO.getNombre() != null ? usuarioVO.getNombre() : "", keyResponse, 256));
            request.setAttribute("apellidoP", AesCtr.encrypt(usuarioVO.getApellidoP() != null ? usuarioVO.getApellidoP() : "", keyResponse, 256));
            request.setAttribute("apellidoM", AesCtr.encrypt(usuarioVO.getApellidoM() != null ? usuarioVO.getApellidoM() : "", keyResponse, 256));
            request.setAttribute("sexo", AesCtr.encrypt(usuarioVO.getSexo() != null ? usuarioVO.getSexo() : "", keyResponse, 256));
            request.setAttribute("telefonoCasa", AesCtr.encrypt(usuarioVO.getTelefonoCasa() != null ? usuarioVO.getTelefonoCasa() : "", keyResponse, 256));
            request.setAttribute("telefonoOficina", AesCtr.encrypt(usuarioVO.getTelefonoOficina() != null ? usuarioVO.getTelefonoOficina() : "", keyResponse, 256));
            request.setAttribute("proveedores", proveedores);
            request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
            request.setAttribute("flujo", "UPDATE");
            forward = "datosComplementarios";
          }
        }
        else
        {
          request.setAttribute("errorMensaje", "Error de comunicaci&oacute;n, intentelo nuevamente.");
          forward = "error";
        }
      }
    }
    catch (Exception e)
    {
      this.log.fatal("Excepcion : ", e);
      request.setAttribute("titulo", "Error");
      request.setAttribute("errorMensaje", "Error al completar las operaciones");
      forward = "error";
    }
    return mapping.findForward(forward);
  }
  
  public ActionForward datosComplementarios(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    String protocol = "http://";
    String keyRequest = null;
    String keyResponse = null;
    String forward = null;
    UsuarioVO usuarioVO = null;
    DatosComplementariosUnoForm datos = null;
    try
    {
      this.log.info("Entro a ComplementariosUnoAction datosComplementarios");
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      keyRequest = (String)request.getSession().getAttribute("jCryptionKey_");
      if ((keyRequest == null) || (keyRequest.length() == 0) || (keyResponse == null) || (keyResponse.length() == 0))
      {
        request.setAttribute("errorMensaje", "Error al validar session");
        forward = "error";
      }
      else
      {
        if (request.isSecure()) {
          protocol = "https://";
        }
        datos = (DatosComplementariosUnoForm)form;
        request.getSession().removeAttribute("usuarioVO");
        usuarioVO = new UsuarioVO();
        usuarioVO.setNombreUsuario(datos.getNombreUsuario());
        usuarioVO.setNumCelular(datos.getNumCelular());
        usuarioVO.setProveedor(datos.getProveedor());
        usuarioVO.setNombre(datos.getNombre());
        usuarioVO.setApellidoP(datos.getApellidoP());
        usuarioVO.setApellidoM(datos.getApellidoM());
        usuarioVO.setFechaNac(datos.getFechaNac());
        usuarioVO.setSexo(datos.getSexo());
        usuarioVO.setTelefonoCasa(datos.getTelefonoCasa());
        usuarioVO.setTelefonoOficina(datos.getTelefonoOficina());
        usuarioVO.setEmail(datos.getEmail());
        this.log.info(usuarioVO);
        request.getSession().setAttribute("usuarioVO", usuarioVO);
        request.setAttribute("flujo", datos.getFlujo());
        forward = "datosComplementarios2";
        request.setAttribute("nextPage", "goToDatosComplementariosDos");
      }
    }
    catch (Exception e)
    {
      this.log.fatal("Excepcion : ", e);
      request.setAttribute("errorMensaje", "Error al completar las operaciones");
      forward = "error";
    }
    return mapping.findForward(forward);
  }
  
  public void setConsumeServiciosService(ConsumeServiciosService consumeServiciosService)
  {
    this.consumeServiciosService = consumeServiciosService;
  }
  
  public void setConsultasService(ConsultasService consultasService)
  {
    this.consultasService = consultasService;
  }
}