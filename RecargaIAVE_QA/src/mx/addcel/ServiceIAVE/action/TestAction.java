/**
 * 
 */
package mx.addcel.ServiceIAVE.action;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import mx.addcel.ServiceIAVE.service.ConsultasService;

	
public class TestAction extends DispatchAction{
	
	private ConsultasService consultasService;
	private Logger log = Logger.getLogger(TestAction.class);
	private ResourceBundle bundle = ResourceBundle.getBundle("servicios");
	
	
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String forward = "error";
		
		log.debug("dentro de execute()");

		//prueba monto + comision
		String comision = consultasService.obtenComision("IAVE_TuTag");
		String montoPesos = consultasService.obtenMontoRecarga("951");
		montoPesos = (Double.parseDouble(montoPesos) + Double.parseDouble(comision))+"";
		request.setAttribute("MontoMasComision", montoPesos);
		
		
		//prueba isBinNacional
		StringBuilder str = new StringBuilder("isBinNacional(286900) = " + consultasService.isBinNacional(286900) + "\n" + "isBinNacional(272800) = " + consultasService.isBinNacional(272800));
		request.setAttribute("isBinNacional", str);
		
		
		forward ="success";
		
		return mapping.findForward(forward);
	}
	
		
	public void setConsultasService(ConsultasService consultasService) {
		this.consultasService = consultasService;
	}
		
}

