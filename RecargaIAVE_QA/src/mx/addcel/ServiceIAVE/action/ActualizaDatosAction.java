package mx.addcel.ServiceIAVE.action;

import com.ironbit.mc.system.crypto.Crypto;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.ResourceBundle;
import javacryption.aes.AesCtr;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.addcel.ServiceIAVE.form.CambiaPassForm;
import mx.addcel.ServiceIAVE.service.ConsultasService;
import mx.addcel.ServiceIAVE.service.ConsumeServiciosService;
import mx.addcel.ServiceIAVE.service.utils.Utils;
import mx.addcel.ServiceIAVE.service.vo.DatosIAVE_VO;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.json.JSONObject;

public class ActualizaDatosAction
  extends DispatchAction
{
  private ConsumeServiciosService consumeServiciosService;
  private ConsultasService consultasService;
  private Logger log = Logger.getLogger(ActualizaDatosAction.class);
  private ResourceBundle bundle = ResourceBundle.getBundle("servicios");
  
  public void setConsultasService(ConsultasService consultasService)
  {
    this.consultasService = consultasService;
  }
  
  public ActionForward inicio(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    String forward = null;
    String keyResponse = null;
    String keyRequest = null;
    String login = null;
    try
    {
      this.log.info("Entro inicio");
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      keyRequest = (String)request.getSession().getAttribute("jCryptionKey_");
      if ((keyRequest == null) || (keyRequest.length() == 0) || (keyResponse == null) || (keyResponse.length() == 0))
      {
        request.setAttribute("errorMensaje", "Error al validar session");
        forward = "error";
      }
      else
      {
        request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
        login = (String)request.getAttribute("login");
        request.setAttribute("login", AesCtr.encrypt(login, keyResponse, 256));
        
        forward = "cambiaPass";
      }
    }
    catch (Exception e)
    {
      this.log.fatal("Error : ", e);
      request.setAttribute("titulo", "Error");
      request.setAttribute("errorMensaje", "Error al realizar las operaciones");
      forward = "error";
    }
    return mapping.findForward(forward);
  }
  
  public ActionForward cambiaPassword(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    String element = null;
    Enumeration en = null;
    String forward = null;
    String keyResponse = null;
    String keyRequest = null;
    String[] resultado = null;
    DatosIAVE_VO datosIAVE = null;
    CambiaPassForm cambiaPassForm = null;
    String urlBase = null;
    String servicio = null;
    String cadenaOriginal = null;
    String cadenaDesencriptada = null;
    String jEnc = null;
    String newEnc = null;
    String protocol = "http://";
    JSONObject jsonObj = null;
    HashMap parametros = null;
    
    this.log.info("Entro cambiaPassword");
    try
    {
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      keyRequest = (String)request.getSession().getAttribute("jCryptionKey_");
      if ((keyResponse == null) || (keyResponse.length() == 0) || (keyRequest == null) || (keyRequest.length() == 0))
      {
        request.setAttribute("titulo", "Error");
        request.setAttribute("errorMensaje", "Error al validar session");
        forward = "error";
      }
      else
      {
        cambiaPassForm = (CambiaPassForm)form;
        if (request.isSecure()) {
          protocol = "https://";
        }
        jsonObj = new JSONObject();
        jsonObj.put("login", cambiaPassForm.getLogin());
        jsonObj.put("password", cambiaPassForm.getPass());
        jsonObj.put("newPassword", cambiaPassForm.getNuevoPass());
        this.log.info(jsonObj);
        urlBase = this.bundle.getString("urlbase");
        servicio = this.bundle.getString("servicioCambiaPassURL");
        jEnc = Crypto.aesEncrypt(Utils.parsePass(cambiaPassForm.getPass()), jsonObj.toString());
        newEnc = Utils.mergeStr(jEnc, cambiaPassForm.getPass());
        
        this.log.info(protocol + urlBase + servicio + "?json=" + newEnc);
        parametros = new HashMap();
        parametros.put("json", newEnc);
        cadenaOriginal = this.consumeServiciosService.cambiaPass(protocol + urlBase + servicio, parametros);
        this.log.info("cadenaOriginal : " + cadenaOriginal);
        cadenaDesencriptada = Crypto.aesDecrypt(Utils.parsePass(cambiaPassForm.getPass()), cadenaOriginal);
        this.log.info("cadenaDesencriptada : " + cadenaDesencriptada);
        jsonObj = new JSONObject(cadenaDesencriptada);
        resultado = new String[2];
        resultado[0] = Integer.toString(((Integer)jsonObj.get("resultado")).intValue());
        resultado[1] = ((String)jsonObj.get("mensaje"));
        if ((resultado != null) && (resultado[0].equals("1")))
        {
          datosIAVE = (DatosIAVE_VO)request.getSession().getAttribute("datosIAVE");
          String loginAsociado = this.consultasService.getMapeoIAVE(datosIAVE.getIduser());
          if ((loginAsociado == null) || (loginAsociado.length() == 0)) {
            this.consultasService.insertaMapeoUsuario(datosIAVE.getIduser(), cambiaPassForm.getLogin());
          }
          request.setAttribute("nextPage", "goToRecargaIAVE");
          request.getSession().setAttribute("loginMobileCard", cambiaPassForm.getLogin());
          request.setAttribute("mensajeCambioPass", resultado[1]);
          forward = "recargaIAVE";
        }
        else
        {
          request.setAttribute("login", cambiaPassForm.getLogin());
          request.setAttribute("nextPage", "goToCambiaPass");
          request.setAttribute("mensajeCambioPass", resultado[1]);
          forward = "cambiaPassRedirect";
        }
      }
    }
    catch (Exception e)
    {
      this.log.fatal("Error al validar login : ", e);
      request.setAttribute("titulo", "Error");
      request.setAttribute("errorMensaje", "Error al realizar las operaciones");
      forward = "error";
    }
    return mapping.findForward(forward);
  }
  
  public void setConsumeServiciosService(ConsumeServiciosService consumeServiciosService)
  {
    this.consumeServiciosService = consumeServiciosService;
  }
}


/* Location:              /home/rhtrejo/workspace/RecargaIAVE/RecargaIAVE.war!/WEB-INF/classes/mx/addcel/ServiceIAVE/action/ActualizaDatosAction.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */