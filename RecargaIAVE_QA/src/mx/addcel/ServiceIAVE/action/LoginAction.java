package mx.addcel.ServiceIAVE.action;

import java.util.Enumeration;
import java.util.ResourceBundle;
import javacryption.aes.AesCtr;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.addcel.ServiceIAVE.form.LoginForm;
import mx.addcel.ServiceIAVE.service.ConsultasService;
import mx.addcel.ServiceIAVE.service.ConsumeServiciosService;
import mx.addcel.ServiceIAVE.service.utils.Utils;
import mx.addcel.ServiceIAVE.service.vo.DatosIAVE_VO;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class LoginAction
  extends DispatchAction
{
  private ConsumeServiciosService consumeServiciosService;
  private ConsultasService consultasService;
  private Logger log = Logger.getLogger(LoginAction.class);
  private ResourceBundle bundle = ResourceBundle.getBundle("servicios");
  
  public void setConsultasService(ConsultasService consultasService)
  {
    this.consultasService = consultasService;
  }
  
  public ActionForward inicio(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    String forward = null;
    String keyResponse = null;
    String keyRequest = null;
    String nextPage = null;
    String login = null;
    try
    {
      this.log.info("Entro inicio");
      nextPage = (String)request.getAttribute("nextPage");
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      keyRequest = (String)request.getSession().getAttribute("jCryptionKey_");
      if ((GenericValidator.isBlankOrNull(keyResponse)) || (GenericValidator.isBlankOrNull(keyRequest)))
      {
        request.setAttribute("titulo", "Error");
        request.setAttribute("errorMensaje", "Error al validar session");
        forward = "error";
      }
      else
      {
        request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
        login = (String)request.getSession().getAttribute("loginTDC");
        if (login != null) {
          request.setAttribute("login", AesCtr.encrypt(login, keyResponse, 256));
        }
        forward = "login";
      }
    }
    catch (Exception e)
    {
      this.log.fatal("Error al validar login : ", e);
      request.setAttribute("titulo", "Error");
      request.setAttribute("errorMensaje", "Error al realizar las operaciones");
      forward = "error";
    }
    return mapping.findForward(forward);
  }
  
  public ActionForward loginUsuario(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
  {
    String element = null;
    Enumeration en = null;
    String forward = null;
    String keyResponse = null;
    String keyRequest = null;
    String[] resultado = null;
    DatosIAVE_VO datosIAVE = null;
    LoginForm loginForm = null;
    this.log.info("Entro loginUsuario");
    try
    {
      keyResponse = (String)request.getSession().getAttribute("jCryptionKey");
      keyRequest = (String)request.getSession().getAttribute("jCryptionKey_");
      if ((keyResponse == null) || (keyResponse.length() == 0) || (keyRequest == null) || (keyRequest.length() == 0))
      {
        request.setAttribute("titulo", "Error");
        request.setAttribute("errorMensaje", "Error al validar session");
        forward = "error";
      }
      else
      {
        loginForm = (LoginForm)form;
        datosIAVE = (DatosIAVE_VO)request.getSession().getAttribute("datosIAVE");
        resultado = Utils.validacionLogin(this.bundle, this.consumeServiciosService, datosIAVE.getImei(), loginForm.getLogin(), loginForm.getPassword(), request.isSecure());
        if ((resultado != null) && (resultado[0].equals("1")))
        {
          datosIAVE = (DatosIAVE_VO)request.getSession().getAttribute("datosIAVE");
          request.getSession().setAttribute("passUserMobilecard", loginForm.getPassword());
          if (loginForm.getFlujo().equalsIgnoreCase("ASOCIA"))
          {
            this.consultasService.insertaMapeoUsuario(datosIAVE.getIduser(), loginForm.getLogin());
            request.setAttribute("nextPage", "goToRecargaIAVE");
            request.getSession().setAttribute("loginMobileCard", loginForm.getLogin());
            forward = "recargaIAVE";
          }
          else if (loginForm.getFlujo().equalsIgnoreCase("DESVINCULAR"))
          {
            request.setAttribute("nextPage", "goToDesvincula");
            request.getSession().setAttribute("loginMobileCard", loginForm.getLogin());
            forward = "desvinculaCuenta";
          }
          else if (loginForm.getFlujo().equalsIgnoreCase("UPDATE"))
          {
            request.setAttribute("nextPage", "goToDatosComplementariosUno");
            request.getSession().setAttribute("loginMobileCard", loginForm.getLogin());
            request.setAttribute("flujo", "UPDATE");
            forward = "actualizaDatos";
          }
        }
        else if ((resultado != null) && (resultado[0].equals("98")))
        {
          request.setAttribute("errorMensaje", AesCtr.encrypt(resultado[1], keyResponse, 256));
          request.setAttribute("nextPage", "goToCambiaPass");
          request.setAttribute("loginMobileCard", loginForm.getLogin());
          request.setAttribute("login", loginForm.getLogin());
          forward = "cambiaPass";
        }
        else
        {
          this.log.info(loginForm.getFlujo());
          if (loginForm.getFlujo().equalsIgnoreCase("ASOCIA"))
          {
            request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
            request.setAttribute("titulo", AesCtr.encrypt("Falla al autenticar usuario", keyResponse, 256));
            request.setAttribute("errorMensaje", AesCtr.encrypt(resultado[1], keyResponse, 256));
            request.setAttribute("login", AesCtr.encrypt(loginForm.getLogin(), keyResponse, 256));
            request.setAttribute("flujo", AesCtr.encrypt("ASOCIA", keyResponse, 256));
            request.setAttribute("nextPage", "goToLogin");
            forward = "loginRedirect";
          }
          else if (loginForm.getFlujo().equalsIgnoreCase("DESVINCULAR"))
          {
            request.setAttribute("titulo", AesCtr.encrypt("Falla al autenticar usuario", keyResponse, 256));
            request.setAttribute("errorMensaje", AesCtr.encrypt(resultado[1], keyResponse, 256));
            request.setAttribute("flujo", AesCtr.encrypt("DESVINCULAR", keyResponse, 256));
            forward = "actualizaDatosInicio";
          }
          else if (loginForm.getFlujo().equalsIgnoreCase("UPDATE"))
          {
            request.setAttribute("titulo", AesCtr.encrypt("Falla al autenticar usuario", keyResponse, 256));
            request.setAttribute("errorMensaje", AesCtr.encrypt(resultado[1], keyResponse, 256));
            request.setAttribute("flujo", AesCtr.encrypt("UPDATE", keyResponse, 256));
            forward = "actualizaDatosInicio";
          }
        }
      }
    }
    catch (Exception e)
    {
      this.log.fatal("Error al validar login : ", e);
      request.setAttribute("titulo", "Error");
      request.setAttribute("errorMensaje", "Error al realizar las operaciones");
      forward = "error";
    }
    return mapping.findForward(forward);
  }
  
  public void setConsumeServiciosService(ConsumeServiciosService consumeServiciosService)
  {
    this.consumeServiciosService = consumeServiciosService;
  }
}