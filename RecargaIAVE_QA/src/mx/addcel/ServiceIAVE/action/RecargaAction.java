package mx.addcel.ServiceIAVE.action;

import java.util.HashMap;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.json.JSONObject;

import com.ironbit.mc.system.crypto.Crypto;

import javacryption.aes.AesCtr;
import javacryption.hash.SHA;
import mx.addcel.ServiceIAVE.form.RecargaForm;
import mx.addcel.ServiceIAVE.service.ConsultasService;
import mx.addcel.ServiceIAVE.service.ConsumeServiciosService;
import mx.addcel.ServiceIAVE.service.utils.Constantes;
import mx.addcel.ServiceIAVE.service.utils.Utils;
import mx.addcel.ServiceIAVE.service.vo.CompraResponceVO;
import mx.addcel.ServiceIAVE.service.vo.DatosIAVE_VO;
import mx.addcel.ServiceIAVE.service.vo.TBitacoraVO;
import mx.addcel.ServiceIAVE.service.vo.UsuarioVO;

public class RecargaAction extends DispatchAction {
	private ConsumeServiciosService consumeServiciosService;
	private ConsultasService consultasService;
	private Logger log = Logger.getLogger(RecargaAction.class);
	private ResourceBundle bundle = ResourceBundle.getBundle("servicios");

	public ActionForward inicio(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String forward = null;
		String keyResponse = null;
		this.log.info("Entro RecargaAction inicio");
		try {
			keyResponse = (String) request.getSession().getAttribute("jCryptionKey");
			if ((keyResponse == null) || (keyResponse.length() == 0)) {
				request.setAttribute("titulo", "Error");
				request.setAttribute("errorMensaje", "Error al validar session");
				forward = "error";
			} else {
				forward = Utils.obtenDatosRecargaIAVE(this.bundle, this.consumeServiciosService, this.consultasService,
						keyResponse, request);
			}
		} catch (Exception e) {
			this.log.fatal("Error al validar login : ", e);
			request.setAttribute("titulo", "Error");
			request.setAttribute("errorMensaje", "Error al realizar las operaciones");
			forward = "error";
		}
		return mapping.findForward(forward);
	}

	public ActionForward realizaRecarga(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		this.log.info("Entro RecargaAction.realizaRecarga");

		String urlBase = null;
		String servicio = null;
		String cadenaOriginal = null;
		String cadenaDesencriptada = null;
		String protocol = "http://";
		String keyResponse = null;
		String keyRequest = null;
		String forward = null;
		String respuesta = null;
		UsuarioVO usuarioVO = null;
		DatosIAVE_VO datosIAVE = null;
		RecargaForm datos = null;
		JSONObject jsonObj = null;
		String jEnc = null;
		String newEnc = null;
		HashMap parametros = null;
		String[] resultado = null;
		String montoRecarga = null;
		UsuarioVO usuario3DsVO = null;

		try {
			keyResponse = (String) request.getSession().getAttribute("jCryptionKey");
			keyRequest = (String) request.getSession().getAttribute("jCryptionKey_");
			if ((keyResponse == null) || (keyResponse.length() == 0) || (keyRequest == null)
					|| (keyRequest.length() == 0)) {
				request.setAttribute("titulo", "Error");
				request.setAttribute("errorMensaje", "Error al validar session");
				forward = "error";
			} else {
				if (request.isSecure()) {
					protocol = "https://";
				}
				datos = (RecargaForm) form;
				usuarioVO = (UsuarioVO) request.getSession().getAttribute("usuarioVO");
				if (usuarioVO == null) {
					usuarioVO = new UsuarioVO();
				}
				datosIAVE = (DatosIAVE_VO) request.getSession().getAttribute("datosIAVE");
				usuarioVO.setPassMobileCard(datos.getPassMobileCard());
				usuarioVO.setPlataforma(datos.getPlataforma());
				usuarioVO.setCvv2(datos.getCvv2());
				usuarioVO.setMonto(datos.getMonto());
				usuarioVO.setNombreUsuario((String) request.getSession().getAttribute("loginMobileCard"));
				usuarioVO.setTipoTarjeta(this.consultasService.tdcTipo(usuarioVO.getNombreUsuario()));
				this.log.info(usuarioVO);
				request.getSession().setAttribute("usuarioVO",usuarioVO);
				

				// validación de usuario
				resultado = Utils.validacionLogin(this.bundle, this.consumeServiciosService, datosIAVE.getImei(),
						usuarioVO.getNombreUsuario(), usuarioVO.getPassMobileCard(), request.isSecure());

				if ((resultado != null) && (resultado[0].equals("1"))) {
					urlBase = this.bundle.getString("urlbase");
					servicio = this.bundle.getString("servicioCompraIAVE");
					jsonObj = new JSONObject();
					jsonObj.put("login", usuarioVO.getNombreUsuario());
					jsonObj.put("password", usuarioVO.getPassMobileCard());
					jsonObj.put("cvv2", usuarioVO.getCvv2());
					jsonObj.put("pin", datosIAVE.getPin());
					jsonObj.put("tarjeta", datosIAVE.getTag());
					String fechaVigencia = this.consultasService.tdcVigencia(usuarioVO.getNombreUsuario());
					jsonObj.put("vigencia", (fechaVigencia != null) && (fechaVigencia.matches("\\d{2}/\\d{2}"))
							? fechaVigencia.replace("/", "") : fechaVigencia);
					jsonObj.put("producto", usuarioVO.getMonto());
					jsonObj.put("imei", "IAVE" + datosIAVE.getImei());
					jsonObj.put("cx", "");
					jsonObj.put("cy", "");
					jsonObj.put("tipo", usuarioVO.getPlataforma());
					jsonObj.put("software", "1.1.1");
					jsonObj.put("modelo", "web");
					jsonObj.put("key", datosIAVE.getImei());
					this.log.info(jsonObj);

					jEnc = Crypto.aesEncrypt(Utils.parsePass(usuarioVO.getPassMobileCard()), jsonObj.toString());
					newEnc = Utils.mergeStr(jEnc, usuarioVO.getPassMobileCard());
					parametros = new HashMap();
					parametros.put("json", newEnc);

					this.log.info(parametros);
					//request.getSession().setAttribute("parametrosJson", parametros);
					// procesar aquí directo los pagos VISA y MASTER; hacer					
					if (Integer.parseInt(usuarioVO.getTipoTarjeta()) != Constantes.AMEX) {
						String montoPesos = null;
						long idBitacora = 1;
						TBitacoraVO tBitacoraVO;

						usuario3DsVO = this.consultasService.consultaUsuario(usuarioVO.getNombreUsuario());
						this.log.info(usuario3DsVO);

						//variables IAVE
						//request.setAttribute("jEnc", jEnc);
						request.setAttribute("newEnc", newEnc);
						
						request.setAttribute("tag", datosIAVE.getTag());
						request.setAttribute("nombreTag", datosIAVE.getNombreTag());												
						request.setAttribute("jCryptionKey", keyResponse);
						request.setAttribute("publicKey", (String)request.getSession().getAttribute("publicKey"));
						request.setAttribute("cc_numberback",Crypto.aesEncrypt(Utils.parsePass("5525963513"), datos.getPassMobileCard())); 
								
						
						//variables que se envian a 3DS
						request.setAttribute("nombre", usuario3DsVO.getNombre() + " " + usuario3DsVO.getApellidoP()
								+ (usuario3DsVO.getApellidoM() == null ?"":" "+usuario3DsVO.getApellidoM()));
						request.setAttribute("TDC",
								Crypto.aesDecrypt(Utils.parsePass("5525963513"), usuario3DsVO.getNumTarjeta()));
						request.setAttribute("mes", usuario3DsVO.getVigenciaTarjeta().trim().split("/")[0]);
						request.setAttribute("anio", "20" + usuario3DsVO.getVigenciaTarjeta().trim().split("/")[1]);

						switch (Integer.parseInt(usuarioVO.getTipoTarjeta())) {
						case Constantes.VISA:
							request.setAttribute("tipoTDC", "Visa");
							break;
						case Constantes.MASTER_CARD:
							request.setAttribute("tipoTDC", "Mastercard");
							break;
						default:
							break;
						}

						//obtener commision
						
						String comision = this.consultasService.obtenComision("IAVE_TuTag");
						montoPesos = this.consultasService.obtenMontoRecarga(datos.getMonto());
						montoPesos = (long)(Double.parseDouble(montoPesos) + Double.parseDouble(comision))+"";
						
						this.log.info("monto+comision=" + montoPesos);
						//montoPesos = "1"; //TODO quitar para pruduccion //valor para pruebas equivalente a un peso

						request.setAttribute("monto", montoPesos + "00");
						
						tBitacoraVO = new TBitacoraVO(
								Long.toString(usuario3DsVO.getIdUsuario()), 
								"Compra IAVE", 
								montoPesos,  
								datosIAVE.getImei(), 
								datosIAVE.getTag(), 
								usuario3DsVO.getNumTarjeta(),
								usuarioVO.getPlataforma(), 
								"1.1.1", 
								"web", 
								datosIAVE.getImei());
						idBitacora = this.consultasService.insertBitacora(tBitacoraVO);
						this.log.info("idBitacora >>>>>>>>>>>>>: " + idBitacora);
						
						request.setAttribute("order_id", idBitacora);
						
						
						// SHA1 (varMerchant + varStore + varTerm + varTotal +varCurrency + varOrder_id)
						request.setAttribute("digest", SHA.sha1(Constantes.MERCHANT + Constantes.STORE + Constantes.TERM + montoPesos +"00"
								+ Constantes.CURRENCY + idBitacora));
						request.setAttribute("email", usuario3DsVO.getEmail());

						CompraResponceVO isReglasValidas = Utils.validaReglasNegocio(usuario3DsVO.getIdUsuario(),
								usuario3DsVO.getNumTarjeta(), 0, consultasService);

						if (isReglasValidas == null)
							forward = "pago3Dsec";
						else {
							this.log.info("Reglas No Validas");
							this.log.debug(isReglasValidas.getMensaje());							
														
							request.setAttribute("mensajeError",  AesCtr
									.encrypt(isReglasValidas.getMensaje(), keyResponse, 256));
							request.setAttribute("nextPage", "goToRecargaIAVE");
							forward = "errorRecarga";
						}
					} else {
						// enviar peticion de compra a AddcelBridge
						int active = 0;
						active = Integer.parseInt(consultasService.getParametro("@AMEX_ACTIVE")); // 1 SI, 0 NO
						if(active==1)
						    cadenaOriginal = this.consumeServiciosService.compraIAVE(protocol + urlBase + servicio,	parametros);
						else
						{
							cadenaOriginal = Crypto.aesEncrypt(Utils.parsePass(usuarioVO.getPassMobileCard()), "{\"resultado\":99, \"mensaje\":\"Estamos trabajando para mejorar nuestra plataforma de pagos AMEX. Todav&iacute;a puedes realizar tus recargas con tarjetas  VISA y MASTERCARD. Disculpa las molestias.\"}");
							TBitacoraVO tBitacoraVO_amex = new TBitacoraVO(
									Long.toString(usuarioVO.getIdUsuario()), 
									"Compra AMEX", 
									"0",  
									datosIAVE.getImei(), 
									datosIAVE.getTag(), 
									usuarioVO.getNumTarjeta(),
									usuarioVO.getPlataforma(), 
									"1.1.1", 
									"web", 
									datosIAVE.getImei());
							tBitacoraVO_amex.setBitTicket("TARJETA AMEX");
							tBitacoraVO_amex.setIdProveedor(17);
							tBitacoraVO_amex.setBitConcepto("compra i + d");
							long idBitacora_amex = this.consultasService.insertBitacora(tBitacoraVO_amex);
							this.log.info("amex(inactivo) " + idBitacora_amex);
						}
						if ((cadenaOriginal != null) && (cadenaOriginal.length() > 0)) {
							this.log.info("cadenaOriginal : " + cadenaOriginal);
							cadenaDesencriptada = Crypto.aesDecrypt(Utils.parsePass(usuarioVO.getPassMobileCard()),
									cadenaOriginal);
							this.log.info("cadenaDesencriptada : " + cadenaDesencriptada);
							jsonObj = new JSONObject(cadenaDesencriptada);
							request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
							if (Integer.toString(((Integer) jsonObj.get("resultado")).intValue()).equals("1")) {
								this.log.info("Pago exitoso");
								montoRecarga = this.consultasService.obtenMontoRecarga(usuarioVO.getMonto());
								request.setAttribute("aliasTag",
										AesCtr.encrypt(datosIAVE.getNombreTag(), keyResponse, 256));
								request.setAttribute("tag", AesCtr.encrypt(datosIAVE.getTag(), keyResponse, 256));
								request.setAttribute("mensageRecarga",
										AesCtr.encrypt((String) jsonObj.get("mensaje"), keyResponse, 256));
								request.setAttribute("monto", AesCtr.encrypt(montoRecarga, keyResponse, 256));
								request.setAttribute("autorizacion",
										AesCtr.encrypt((String) jsonObj.get("folio"), keyResponse, 256));
								request.setAttribute("nextPage", "goToConfirmacionPago");
								forward = "pagoExitoso";
								//forward = "endPagoExitoso";
							} else {
								this.log.info("Error al realizar pago");
								this.log.info("mensajeAmex: " + (String) jsonObj.get("mensaje") + " activo: " + active );
								request.setAttribute("errorMensaje",
										AesCtr.encrypt((String) jsonObj.get("mensaje"), keyResponse, 256));
								request.setAttribute("nextPage", "goToRecargaIAVE");
								forward = "errorRecarga";
							}
						} else {
							request.setAttribute("errorMensaje", "Error de comunicaci&oacute;n, intentelo nuevamente.");
							forward = "error";
						}
					}

				} else {
					this.log.info("Entro pass no valido");
					request.setAttribute("mensajeError", AesCtr
							.encrypt("El password ingresado no coincide con el usuario MobileCard", keyResponse, 256));
					request.setAttribute("nextPage", "goToRecargaIAVE");
					forward = "errorRecarga";
				}
			}
		} catch (Exception e) {
			this.log.fatal("Excepcion : ", e);
			request.setAttribute("titulo", "Error");
			request.setAttribute("errorMensaje", "Error al completar las operaciones");
			forward = "error";
			e.printStackTrace();
		}
		return mapping.findForward(forward);
	}

	public void setConsumeServiciosService(ConsumeServiciosService consumeServiciosService) {
		this.consumeServiciosService = consumeServiciosService;
	}

	public void setConsultasService(ConsultasService consultasService) {
		this.consultasService = consultasService;
	}
		
	

}
