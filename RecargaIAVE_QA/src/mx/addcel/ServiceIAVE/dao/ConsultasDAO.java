package mx.addcel.ServiceIAVE.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import mx.addcel.ServiceIAVE.service.vo.ReglasVO;
import mx.addcel.ServiceIAVE.service.vo.TBitacoraVO;
import mx.addcel.ServiceIAVE.service.vo.UsuarioVO;

public class ConsultasDAO extends SqlMapClientDaoSupport {
	public String existeUsuarioAsociado(String loginIAVE) {
		String resultado = "";
		List lista = getSqlMapClientTemplate().queryForList("obtenMapeoIAVE", loginIAVE);
		if ((lista != null) && (lista.size() > 0) && (lista.get(0) != null)) {
			resultado = (String) lista.get(0);
		}
		return resultado;
	}

	public Integer existeUsuario(String login) {
		Integer resultado = new Integer(0);
		List lista = getSqlMapClientTemplate().queryForList("existeUsuario", login);
		if ((lista != null) && (lista.size() > 0) && (lista.get(0) != null)) {
			resultado = (Integer) lista.get(0);
		}
		return resultado;
	}

	public void insertaMapeoIAVE(HashMap parametros) {
		getSqlMapClientTemplate().insert("insertaMapeoIAVE", parametros);
	}

	public String existeTDC(String tdc) {
		String resultado = "";
		List lista = getSqlMapClientTemplate().queryForList("existeTDC", tdc);
		if ((lista != null) && (lista.size() > 0) && (lista.get(0) != null)) {
			resultado = (String) lista.get(0);
		}
		return resultado;
	}

	public String existeEmail(String email) {
		String resultado = "";
		List lista = getSqlMapClientTemplate().queryForList("existeEmail", email);
		if ((lista != null) && (lista.size() > 0) && (lista.get(0) != null)) {
			resultado = (String) lista.get(0);
		}
		return resultado;
	}

	public String existeTelefono(String dn) {
		String resultado = "";
		List lista = getSqlMapClientTemplate().queryForList("existeDN", dn);
		if ((lista != null) && (lista.size() > 0) && (lista.get(0) != null)) {
			resultado = (String) lista.get(0);
		}
		return resultado;
	}

	public String tdcVigencia(String login) {
		String resultado = "";
		List lista = getSqlMapClientTemplate().queryForList("tdcVigencia", login);
		if ((lista != null) && (lista.size() > 0) && (lista.get(0) != null)) {
			resultado = (String) lista.get(0);
		}
		return resultado;
	}

	public String existeIMEI(String imei) {
		String resultado = "";
		List lista = getSqlMapClientTemplate().queryForList("existeIMEI", imei);
		if ((lista != null) && (lista.size() > 0) && (lista.get(0) != null)) {
			resultado = (String) lista.get(0);
		}
		return resultado;
	}

	public String obtenComision(String proveedor) {
		String resultado = "";
		List lista = getSqlMapClientTemplate().queryForList("obtenComision", proveedor);
		if ((lista != null) && (lista.size() > 0) && (lista.get(0) != null)) {
			resultado = (String) lista.get(0);
		}
		return resultado;
	}

	public String obtenMontoRecarga(String producto) {
		String resultado = "";
		List lista = getSqlMapClientTemplate().queryForList("obtenMontoRecarga", producto);
		if ((lista != null) && (lista.size() > 0) && (lista.get(0) != null)) {
			resultado = (String) lista.get(0);
		}
		return resultado;
	}

	public String validaStatusUsuario(String login) {
		String resultado = "";
		List lista = getSqlMapClientTemplate().queryForList("validaStatusUsuario", login);
		if ((lista != null) && (lista.size() > 0) && (lista.get(0) != null)) {
			resultado = (String) lista.get(0);
		}
		return resultado;
	}

	public void desvinculaCuentas(String idUserIAVE) {
		getSqlMapClientTemplate().delete("desvinculaCuentas", idUserIAVE);
	}

	public UsuarioVO consultaUsuario(String login) {
		UsuarioVO resultado = null;
		List lista = getSqlMapClientTemplate().queryForList("consultaUsuario", login);
		if ((lista != null) && (lista.size() > 0) && (lista.get(0) != null)) {
			resultado = (UsuarioVO) lista.get(0);
		}
		return resultado;
	}

	/**
	 * @param idBitacora
	 * @return
	 */
	public UsuarioVO consultaUsuarioByIdBitacora(long idBitacora) {
		UsuarioVO resultado = null;
		List lista = getSqlMapClientTemplate().queryForList("consultaUsuarioByIdBitacora", idBitacora);
		if ((lista != null) && (lista.size() > 0) && (lista.get(0) != null)) {
			resultado = (UsuarioVO) lista.get(0);
		}
		return resultado;
	}

	public String tdcTipo(String login) {
		String resultado = "";
		List lista = getSqlMapClientTemplate().queryForList("tdcTipo", login);
		if ((lista != null) && (lista.size() > 0) && (lista.get(0) != null)) {
			resultado = (String) lista.get(0);
		}
		return resultado;
	}

	/**
	 * Obtiene un parametro de la BD en la tabla t_parametros
	 * 
	 * @param key
	 *            llave del parametro a obtener
	 * @return parametro
	 */
	public String getParametro(String key) {
		String resultado = "";
		List lista = getSqlMapClientTemplate().queryForList("getParametro", key);

		if ((lista != null) && (lista.size() > 0) && (lista.get(0) != null)) {
			resultado = (String) lista.get(0);
		}

		return resultado;
	}

	public ReglasVO selectRuleU(HashMap parametros) {
		ReglasVO resultado = null;

		List lista = getSqlMapClientTemplate().queryForList("selectRuleU", parametros);
		if ((lista != null) && (lista.size() > 0) && (lista.get(0) != null)) {
			resultado = (ReglasVO) lista.get(0);
		}

		return resultado;
	}

	public ReglasVO selectRuleUt(HashMap parametros) {
		ReglasVO resultado = null;

		List lista = getSqlMapClientTemplate().queryForList("selectRuleUt", parametros);
		if ((lista != null) && (lista.size() > 0) && (lista.get(0) != null)) {
			resultado = (ReglasVO) lista.get(0);
		}

		return resultado;
	}

	public long insertBitacora(TBitacoraVO tBitacoraVO) {
		long resultado;

		resultado = ((Long) getSqlMapClientTemplate().insert("insertBitacora", tBitacoraVO)).longValue();

		return resultado;
	}
	
	public double getComisionByIdProveedor(long idProveedor){
		double resultado = 0.0;
		
		List lista = getSqlMapClientTemplate().queryForList("getComisionByIdProveedor", idProveedor);
		if ((lista != null) && (lista.size() > 0) && (lista.get(0) != null)) {
			resultado = (Double)lista.get(0);
		}

		return resultado;
	  }
	
	public boolean isBinNacional(long binBajo){
		boolean resultado = false;
		
		List lista = getSqlMapClientTemplate().queryForList("isBinNacional", binBajo);
		if ((lista != null) && (lista.size() > 0) && (lista.get(0) != null)) {
			resultado = (((Short)lista.get(0)) > 0) ? true : false;
		}

		return resultado;
	  }

}