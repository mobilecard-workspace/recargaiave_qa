package javacryption.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.KeyPair;
import java.util.Enumeration;
import javacryption.aes.AesCtr;
import javacryption.jcryption.JCryption;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

public class CryptoServlet
  extends HttpServlet
{
  private Logger log = Logger.getLogger(CryptoServlet.class);
  private static final long serialVersionUID = 4510110365995157499L;
  
  public void doPost(HttpServletRequest req, HttpServletResponse res)
    throws IOException, ServletException
  {
    HttpServletRequest request = req;
    HttpServletResponse response = res;
    PrintWriter out = null;
    String callback = null;
    String element = null;
    String output = null;
    Enumeration en = null;
    JCryption jc = null;
    KeyPair keys = null;
    String key = null;
    String e = null;
    String n = null;
    String md = null;
    en = request.getParameterNames();
    this.log.info("***************inicio***************");
    while (en.hasMoreElements())
    {
      element = (String)en.nextElement();
      this.log.info(element + " : " + request.getParameter(element));
    }
    this.log.info("***************fin***************");
    if (req.getParameter("publicKey") != null)
    {
      this.log.info("llave publica : " + req.getParameter("publicKey"));
      HttpSession session = request.getSession(false);
      if (session != null) {
        session.invalidate();
      }
      session = request.getSession(true);
      session.setAttribute("publicKey", req.getParameter("publicKey"));
    }
    if ((req.getParameter("generateKeyPair") != null) && (req.getParameter("generateKeyPair").equals("true")))
    {
      jc = new JCryption();
      keys = jc.getKeyPair();
      request.getSession().setAttribute("jCryptionKeys", keys);
      e = jc.getPublicExponent();
      n = jc.getKeyModulus();
      md = String.valueOf(jc.getMaxDigits());
      
      callback = request.getParameter("callback");
      output = null;
      if (callback != null)
      {
        response.setContentType("text/javascript");
        output = callback + "({\"e\":\"" + e + "\",\"n\":\"" + n + "\",\"maxdigits\":\"" + md + "\"});";
      }
      else
      {
        output = "{\"e\":\"" + e + "\",\"n\":\"" + n + "\",\"maxdigits\":\"" + md + "\"}";
      }
      this.log.info(output);
      out = response.getWriter();
      out.print(output);
      out.flush();
      return;
    }
    if ((req.getParameter("handshake") != null) && (req.getParameter("handshake").equals("true")))
    {
      jc = new JCryption((KeyPair)request.getSession().getAttribute("jCryptionKeys"));
      key = jc.decrypt(req.getParameter("key"));
      request.getSession().removeAttribute("jCryptionKeys");
      request.getSession().setAttribute("jCryptionKey", key);
      
      String ct = AesCtr.encrypt(key, key, 256);
      
      callback = request.getParameter("callback");
      if (callback != null)
      {
        response.setContentType("text/javascript");
        output = callback + "({\"challenge\":\"" + ct + "\"});";
      }
      else
      {
        output = "{\"challenge\":\"" + ct + "\"}";
      }
      this.log.info(output);
      out = response.getWriter();
      out.print(output);
      out.flush();
      return;
    }
    if ((req.getParameter("generateKeyPair_") != null) && (req.getParameter("generateKeyPair_").equals("true")))
    {
      jc = new JCryption();
      keys = jc.getKeyPair();
      request.getSession().setAttribute("jCryptionKeys_", keys);
      e = jc.getPublicExponent();
      n = jc.getKeyModulus();
      md = String.valueOf(jc.getMaxDigits());
      
      callback = request.getParameter("callback");
      output = null;
      if (callback != null)
      {
        response.setContentType("text/javascript");
        output = callback + "({\"e\":\"" + e + "\",\"n\":\"" + n + "\",\"maxdigits\":\"" + md + "\"});";
      }
      else
      {
        output = "{\"e\":\"" + e + "\",\"n\":\"" + n + "\",\"maxdigits\":\"" + md + "\"}";
      }
      this.log.info(output);
      out = response.getWriter();
      out.print(output);
      out.flush();
      return;
    }
    if ((req.getParameter("handshake_") != null) && (req.getParameter("handshake_").equals("true")))
    {
      jc = new JCryption((KeyPair)request.getSession().getAttribute("jCryptionKeys_"));
      key = jc.decrypt(req.getParameter("key"));
      request.getSession().removeAttribute("jCryptionKeys_");
      request.getSession().setAttribute("jCryptionKey_", key);
      
      String ct = AesCtr.encrypt(key, key, 256);
      
      callback = request.getParameter("callback");
      if (callback != null)
      {
        response.setContentType("text/javascript");
        output = callback + "({\"challenge\":\"" + ct + "\"});";
      }
      else
      {
        output = "{\"challenge\":\"" + ct + "\"}";
      }
      this.log.info(output);
      out = response.getWriter();
      out.print(output);
      out.flush();
      return;
    }
  }
  
  public void doGet(HttpServletRequest req, HttpServletResponse res)
    throws IOException, ServletException
  {
    doPost(req, res);
  }
}