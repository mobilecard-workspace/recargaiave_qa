package javacryption.exception;

public class CryptoException
  extends RuntimeException
{
  private static final long serialVersionUID = 2937872982389908084L;
  
  public CryptoException() {}
  
  public CryptoException(String message)
  {
    super(message);
  }
  
  public CryptoException(Throwable cause)
  {
    super(cause);
  }
  
  public CryptoException(String message, Throwable cause)
  {
    super(message, cause);
  }
}


/* Location:              /home/rhtrejo/workspace/RecargaIAVE/RecargaIAVE.war!/WEB-INF/classes/javacryption/exception/CryptoException.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */