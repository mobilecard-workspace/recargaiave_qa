/**
 * 
 */
package javacryption.hash;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.log4j.Logger;

/**
 * @author rhtrejo
 *
 */
public class SHA {
	
	/**
	 * @param text
	 * @return SHA-1 as HEX
	 */
	private static Logger log = Logger.getLogger(SHA.class);
	
	public static String sha1(String text){		
		BigInteger bInt = null;
		String digest="";		
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-1");
			md.update(text.getBytes());
			bInt = new BigInteger(1,md.digest());
			
			digest = bInt.toString(16);
			
			if(digest.length() < 40)
			{
				log.debug("Se detecto un digest menor a 40: "+ digest + " : " + String.format("%040x", bInt));
				digest = String.format("%040x", bInt);
			}
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return digest;//bInt.toString(16);
	}
}
