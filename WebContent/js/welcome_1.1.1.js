var welcomeForm;

function unMaskForm() {
    welcomeForm.setMasked(false);
}
function margenElemento(porCont, tamElementoY) {
    var height = $(window).height();
    var tamPanel = height * (porCont / 100);
    return (tamPanel - tamElementoY) / 2;
}

function centraElementoX(tamx, inicioX, finX) {
    var x = finX - inicioX;
    alert(inicioX + ((x - tamx) / 2));
    return inicioX + ((x - tamx) / 2);
}
function centraElementoY(tamy, inicioY, finY) {
    var y = finY - inicioY;
    return inicioY + ((y - tamy) / 2);
}


Ext.application({
    launch: function() {
        Ext.applyIf(Ext.data.Validations, {
            presenceMejora: function(config, value) {
                if (arguments.length === 1) {
                    value = config;
                }
                if (value.trim().length === 0) {
                    return false;
                } else {
                    return true;
                }
            }
        });
        Ext.define('loginModel', {
            extend: 'Ext.data.Model',
            config: {
                fields: [{
                        name: 'login',
                        type: 'string'
                    }, {
                        name: 'pass',
                        type: 'string'
                    }, {
                        name: 'flujo',
                        type: 'string'
                    }], // fields
                validations: [{
                        field: 'login',
                        type: 'presenceMejora',
                        message: 'El login es requerido'
                    }, {
                        field: 'pass',
                        type: 'presenceMejora',
                        message: 'El password es requerido'
                    }, {
                        field: 'pass',
                        type: 'length',
                        min: 8,
                        max: 13,
                        message: 'Password invalido.'
                    }
                ] // validations
            } // config
        }); // define()
        welcomeForm = Ext.create(
                'Ext.form.FormPanel',
                {
                    fullscreen: true,
                    itemId: 'welcome',
                    scrollable: 'vertical',
                    standardSubmit: true,
                    masked: {
                        xtype: 'loadmask',
                        message: 'Dando seguridad al canal... ' +
                                '<hr>Espere porfavor...'
                    }, // masked
                    items: [{
                            xtype: 'titlebar',
                            docked: 'top',
                            title: 'Inicio',
                            style: 'background:#09569b',
                            items: [{
                                    iconCls: "delete",
                                    iconMask: !0,
                                    ui: "plain",
                                    id: 'btnCancelar',
                                    handler: function(btn, evt) {
                                        location.href = "http://www.mobilecard.mx:8080/RecargaIAVE_test/close.html";
                                    }
                                }
                            ]
                        }, {
                            xtype: 'image',
                            //html: '<div><img src="images/mobilecard.png"></div>',
                            //margin: new String(((screen.availHeight / 16)) + 'px auto'),
                            //top:new String(centraElementoY(144,150,350)+'px'),
                            margin: new String(margenElemento(50, 144) + 'px auto'),
                            width: '144px',
                            height: '144px',
                            mode: 'foreground',
                            src: 'images/mobilecard.png'
                        }, {
                            xtype: 'button',
                            text: ' Usuario MobileCard',
                            id: 'btnUsuarioMC',
                            width: '80%',
                            margin: new String(margenElemento(25, 70) + 'px auto'),
                            handler: function() {
                                Ext.getCmp("btnUsuarioMC").disable();
                                Ext.Viewport.add(popup);
                                popup.show();
                            }
                        }, {
                            xtype: 'button',
                            text: 'Registrar nuevo usuario',
                            id: 'btnUsuarioNuevo',
                            width: '80%',
                            margin: new String(margenElemento(25, 70) + 'px auto'),
                            handler: function(btn, evt) {
                                Ext.getCmp("btnUsuarioNuevo").disable();
                                welcomeForm.submit({url: 'welcome.do?method=iniciaRegistro', method: 'POST'});
                            }
                        }
                    ]
                }

        );
        var popup = Ext.create('Ext.form.FormPanel', {
            floating: true,
            centered: true,
            modal: true,
            width: '98%',
            height: 250,
            standardSubmit: true,
            instructions: '<code>(*)</code> REQUERIDO',
            items: [{
                    xtype: 'titlebar',
                    title: 'Login MobileCard',
                    docked: 'top',
                    style: 'background:#09569b'
                }, {
                    xtype: 'fieldset',
                    items: [{
                            xtype: 'textfield',
                            name: 'login',
                            id: 'login',
                            label: 'Login:',
                            labelWidth: 150,
                            required: true
                        }, {
                            xtype: 'passwordfield',
                            name: 'pass',
                            id: 'pass',
                            label: 'Password:',
                            labelWidth: 150,
                            required: true
                        }, {
                            xtype: 'hiddenfield',
                            name: 'flujo',
                            id: 'flujo',
                            value: 'REGISTRO'
                        }] // items
                }, {
                    xtype: 'toolbar',
                    layout: {
                        pack: 'center'
                    }, // layout
                    ui: 'plain',
                    items: [{
                            xtype: 'button',
                            text: 'Regresar',
                            width: '35%',
                            handler: function(btn, evt) {
                                popup.hide();
                                Ext.getCmp("btnUsuarioMC").enable();
                            }
                        }, {
                            xtype: 'button',
                            text: 'Enviar',
                            width: '35%',
                            id: 'btnEnviar',
                            handler: function(btn, evt) {
                                Ext.getCmp("btnEnviar").disable();
                                var loginModel = Ext.create('loginModel');
                                var errors, errorMessage = '';
                                popup.updateRecord(loginModel);
                                errors = loginModel.validate();
                                if (!errors.isValid()) {
                                    errors.each(function(err) {
                                        errorMessage += err.getMessage() + '<br/>';
                                    }); // each()
                                    Ext.Msg.alert('Datos incorrectos', errorMessage);
                                    Ext.getCmp("btnEnviar").enable();
                                } else {

                                    loginEnc.setValues({loginEnc: $.jCryption.encrypt(Ext.getCmp("login").getValue(), password_),
                                        passwordEnc: $.jCryption.encrypt(Ext.getCmp("pass").getValue(), password_),
                                        flujoEnc: $.jCryption.encrypt(Ext.getCmp("flujo").getValue(), password_)});
                                    loginEnc.submit({url: 'login.do?method=loginUsuario', method: 'POST'});
                                } // if
                            } // handler
                        }] // items (toolbar)
                }]
        }); // create()
        var loginEnc = Ext.create('Ext.form.FormPanel', {
            standardSubmit: true,
            items: [
                {
                    xtype: 'hiddenfield',
                    name: 'loginEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'passwordEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'flujoEnc'
                }] // items (formpanel)
        }); // create()
        if (typeof mensajeDesvinculado !== "undefined") {
            Ext.Msg.alert("Cuenta desvinculada", mensajeDesvinculado);
        }
    } // launch
}); // application()