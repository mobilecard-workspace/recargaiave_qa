var errorPanel;
Ext.application({
    launch: function() {
		Ext.override(Ext.util.SizeMonitor, {
			constructor: function(config) {
			  var namespace = Ext.util.sizemonitor;

			  if (Ext.browser.is.Firefox) {
				return new namespace.OverflowChange(config);
			  } else if (Ext.browser.is.WebKit) {
				if (!Ext.browser.is.Silk && Ext.browser.engineVersion.gtEq('535') && !Ext.browser.engineVersion.ltEq('537.36')) {
				  return new namespace.OverflowChange(config);
				} else {
				  return new namespace.Scroll(config);
				}
			  } else if (Ext.browser.is.IE11) {
				return new namespace.Scroll(config);
			  } else {
				return new namespace.Scroll(config);
			  }
			}
		});
		Ext.override(Ext.util.PaintMonitor, {
			constructor: function(config) {
			  if (Ext.browser.is.Firefox || (Ext.browser.is.WebKit && Ext.browser.engineVersion.gtEq('536') && !Ext.browser.engineVersion.ltEq('537.36') && !Ext.os.is.Blackberry)) {
				return new Ext.util.paintmonitor.OverflowChange(config);
			  } else {
				return new Ext.util.paintmonitor.CssAnimation(config);
			  }
			}
		});
		
        errorPanel = Ext.create('Ext.form.Panel', {
            fullscreen: true,
            itemId: 'errorGeneral',
            scrollable: 'vertical',
            items: [{
                    xtype: 'titlebar',
                    docked: 'top',
                    title: 'Error',
                    style: 'background:#09569b',
                    items: [{
                            iconCls:"delete",
                            iconMask:!0,
                            ui:"plain",
                            id: 'btnCancelar',
                            handler: function(btn, evt) {
                                location.href = "http://www.mobilecard.mx:8080/RecargaIAVE_test/close.html";
                            }
                        }]
                }, {
                    xtype: 'panel',
                    styleHtmlContent: true,
                    html: '<center>' + error + '<br/><br/>Seleccione nuevamente su tag</center>',
                    height: '100%',
                    width: '100%',
                    ui: 'dark'
                }, {
                    xtype: 'toolbar',
                    docked: 'bottom',
                    styleHtmlContent: true,
                    title: '<div style="margin:-10px 0px 0px 0px;font-size: 10px; color: white;">Powered by Mobilecard &reg;</div>',                    
                    minHeight: '1.3em',
                    height: '1.3em',
                    style: 'background:#09569b'
                }]
        }); // create()
    } // launch
}); // application()

