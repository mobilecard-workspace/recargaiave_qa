<!DOCTYPE html>
<html>
    <head>
        <%request.setAttribute("recursoJS", "actualizaDatos_1.1.0.js");%>
        <%@ include file="/jsp/genInclude.jsp" %>
        <%@ include file="/jsp/commonsInclude.jsp" %>
        <%String idPage = "goToActualizaDatos";
            if (!idPage.equals(nextPage)) {
                request.removeAttribute("llavePublica");
                request.setAttribute("errorMensaje", "Acceso denegado");
                response.sendRedirect(redirectErrorURL);
            }%>
        <title>Edita datos</title>
        <script>
            var login = $.jCryption.decrypt('<%=request.getAttribute("login")%>', password);
            <%if (request.getAttribute("errorMensaje") != null) {%>
            var titulo = $.jCryption.decrypt('<%=request.getAttribute("titulo")%>', password);
            var error = $.jCryption.decrypt('<%=request.getAttribute("errorMensaje")%>', password);
            <%}%>
        </script>
    </head>
    <body></body>
</html>