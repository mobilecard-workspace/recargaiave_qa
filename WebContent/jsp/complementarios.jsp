<!DOCTYPE html>
<html>
    <head>
        <%request.setAttribute("recursoJS", "complementarios_1.1.1.js");%>
        <%@ include file="/jsp/genInclude.jsp" %>
        <%@ include file="/jsp/commonsInclude.jsp" %>
        <%String idPage = "goToDatosComplementariosUno";
            if (!idPage.equals(nextPage)) {
                request.removeAttribute("llavePublica");
                request.setAttribute("errorMensaje", "Acceso denegado");
                response.sendRedirect(redirectErrorURL);
            }%>
        <script>
            <%if (request.getAttribute("flujo").equals("REGISTRO")) {%>
                var email = $.jCryption.decrypt('<%=request.getAttribute("email")%>', password);
                var dn = $.jCryption.decrypt('<%=request.getAttribute("dn")%>', password);
                var login = $.jCryption.decrypt('<%=request.getAttribute("login")%>', password);
                var yearFrom = $.jCryption.decrypt('<%=request.getAttribute("yearFrom")%>', password);
                var yearTo = $.jCryption.decrypt('<%=request.getAttribute("yearTo")%>', password);
                var proveedores = Ext.JSON.decode($.jCryption.decrypt('<%=request.getAttribute("proveedores")%>', password));  
                var fechaNacimiento;
                var nombre;
                var apellidoP;
                var apellidoM;
                var sexo;
                var telefonoCasa;
                var telefonoOficina;
                var flujo = '<%=request.getAttribute("flujo")%>';
            <%} else if (request.getAttribute("flujo").equals("UPDATE")) {%>
                var email = $.jCryption.decrypt('<%=request.getAttribute("email")%>', password);
                var dn = $.jCryption.decrypt('<%=request.getAttribute("dn")%>', password);
                var login = $.jCryption.decrypt('<%=request.getAttribute("login")%>', password);
                var yearFrom = $.jCryption.decrypt('<%=request.getAttribute("yearFrom")%>', password);
                var yearTo = $.jCryption.decrypt('<%=request.getAttribute("yearTo")%>', password);
                var fechaNacimiento = Ext.JSON.decode($.jCryption.decrypt('<%=request.getAttribute("fechaNacimiento")%>', password));
                var nombre = $.jCryption.decrypt('<%=request.getAttribute("nombre")%>', password);
                var apellidoP = $.jCryption.decrypt('<%=request.getAttribute("apellidoP")%>', password);
                var apellidoM = $.jCryption.decrypt('<%=request.getAttribute("apellidoM")%>', password);
                var sexo = $.jCryption.decrypt('<%=request.getAttribute("sexo")%>', password);
                var telefonoCasa = $.jCryption.decrypt('<%=request.getAttribute("telefonoCasa")%>', password);
                var telefonoOficina = $.jCryption.decrypt('<%=request.getAttribute("telefonoOficina")%>', password);
                var proveedores = Ext.JSON.decode($.jCryption.decrypt('<%=request.getAttribute("proveedores")%>', password));
                var flujo = '<%=request.getAttribute("flujo")%>';
            <%}%>
        </script>
    </head>
    <body></body>
</html>