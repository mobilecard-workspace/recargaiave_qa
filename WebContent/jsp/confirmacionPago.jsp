<!DOCTYPE html>
<html>
    <head>
        <%@ include file="/jsp/genInclude.jsp" %>
        <%String idPage = "goToConfirmacionPago";
            if (!idPage.equals(nextPage)) {
                request.removeAttribute("llavePublica");
                request.setAttribute("errorMensaje", "Acceso denegado");
                response.sendRedirect(redirectErrorURL);
            }%>
        <script src="<%=request.getContextPath()%>/js/sencha-touch-all_2.2.1_v1.1.1.js"></script>
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/sencha-touch.2.2.1_v1.0.0.css">
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/confirmacion_1.1.1.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.min_1.1.0.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.jcryption.min_1.1.0.js"></script>    
        <title>Recarga</title>
        <script>
            var password = new jsSHA('<%=request.getAttribute("llavePublica")%>', "ASCII").getHash("SHA-512", "HEX");
            var aliasTag = $.jCryption.decrypt('<%=request.getAttribute("aliasTag")%>', password);
            var tag = $.jCryption.decrypt('<%=request.getAttribute("tag")%>', password);
            var monto = $.jCryption.decrypt('<%=request.getAttribute("monto")%>', password);
            var mensageRecarga = $.jCryption.decrypt('<%=request.getAttribute("mensageRecarga")%>', password);
            var autorizacion = $.jCryption.decrypt('<%=request.getAttribute("autorizacion")%>', password);
        </script>
    </head>
    <body></body>
</html>