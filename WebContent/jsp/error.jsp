<!DOCTYPE html>
<html>
    <head>
        <script>
            var Ext = Ext || {};
            Ext.theme = {
                name: "Default"
            };
        </script>  

        <title>Error</title>      
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/sencha-touch.2.2.1_v1.0.0.css">
        <script src="<%=request.getContextPath()%>/js/sencha-touch-all_2.2.1_v1.1.1.js"></script>
        <script src="<%=request.getContextPath()%>/js/error_1.1.1.js"></script>
        <%if (request.getAttribute("llavePublica") != null) {%>
            <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.min_1.1.0.js"></script>
            <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.jcryption.min_1.1.0.js"></script>
            <script>
                var password = new jsSHA('<%=request.getAttribute("llavePublica")%>', "ASCII").getHash("SHA-512", "HEX");
                var error = $.jCryption.decrypt('<%=request.getAttribute("errorMensaje")%>', password);
            </script>
        <%} else {%>
            <script>
                var error = '<%=request.getAttribute("errorMensaje") != null ? request.getAttribute("errorMensaje") : "Error inesperado en la aplicaci&oacute;n."%>';
            </script>
        <%}%>
        <!--Error <%=request.getAttribute("errorMensaje")%>-->
        <%request.getSession().invalidate();%>
    </head>
    <body>
    </body>
</html>