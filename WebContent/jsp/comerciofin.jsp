<%@ page contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="mx.addcel.ServiceIAVE.service.utils.Constantes"%>
<%@ page isELIgnored="false"%>

<input type="hidden" name="name" value="<%=request.getAttribute("nombre")%>" />
<input type="hidden" name="number" value="<%=request.getAttribute("TDC")%>" />
<input type="hidden" name="type" value="<%=request.getAttribute("tipoTDC")%>" />
<input type="hidden" name="expmonth" value="<%=request.getAttribute("mes")%>" />
<input type="hidden" name="expyear" value="<%=request.getAttribute("anio")%>" />

	<div id="3ds-prosa">
		
		<c:import var="testHtml"
			url="https://www.procom.prosa.com.mx/eMerchant/8039159_imdm.jsp"/>						
		<c:out value="${testHtml}" escapeXml="false" />
		
				
		<script type="text/javascript">

		
		var frm = document.getElementsByTagName('form')[0];
		//alert(frm.innerHTML);
		frm.innerHTML = frm.innerHTML+
		'<input type="hidden" name="total" value="<%=request.getAttribute("monto")%>" />'+
		'<input type="hidden" name="currency" value="<%=Constantes.CURRENCY%>"/>'+
		'<input type="hidden" name="address" value="<%=Constantes.ADDRESS%>"/>'+
		'<input type="hidden" name="order_id" value="<%=request.getAttribute("order_id")%>" />'+
		'<input type="hidden" name="merchant" value="<%=Constantes.MERCHANT%>"/>'+
		'<input type="hidden" name="store" value="<%=Constantes.STORE%>"/>'+
		'<input type="hidden" name="term" value="<%=Constantes.TERM%>"/>'+
		'<input type="hidden" name="digest" value="<%=request.getAttribute("digest")%>" />'+
		'<input type="hidden" name="return_target" value="" />'+
		'<input type="hidden" name="email" value="<%=request.getAttribute("email")%>" />'+
		'<input type="hidden" name="tag" value="<%=request.getAttribute("tag")%>" />'+
		'<input type="hidden" name="nombreTag" value="<%=request.getAttribute("nombreTag")%>" />'+
		'<input type="hidden" name="pin" value="<%=request.getAttribute("pin")%>" />'+
		'<input type="hidden" name="imei" value="<%=request.getAttribute("imei")%>" />'+
		'<input type="hidden" name="urlBack" value="<%=Constantes.URLBACK%>" />'
		;
		//alert(frm.innerHTML);
				
		frm.setAttribute('action','https://www.procom.prosa.com.mx/eMerchant/validaciones/valida.do');
		
		//alert(frm.getAttribute('action'));
		
		//alert(document.getElementsByName('cc_name').value);
		//alert(document.getElementsByName('cc_name')[0].parentElement);
		
		//Ocultar campos de entrada		
		document.getElementsByName('cc_name')[0].style.visibility="hidden"
		document.getElementsByName('cc_number')[0].style.visibility="hidden"
		document.getElementsByName('cc_type')[0].style.visibility="hidden"
		document.getElementsByName('_cc_expmonth')[0].style.visibility="hidden"
		document.getElementsByName('_cc_expyear')[0].style.visibility="hidden"
		
		//Establecer valores
		document.getElementsByName('cc_name')[0].value = document.getElementsByName('name')[0].value;
		document.getElementsByName('cc_number')[0].value = document.getElementsByName('number')[0].value;
		document.getElementsByName('cc_type')[0].value = document.getElementsByName('type')[0].value;
		document.getElementsByName('_cc_expmonth')[0].value = document.getElementsByName('expmonth')[0].value;
		document.getElementsByName('_cc_expyear')[0].value = document.getElementsByName('expyear')[0].value;
		
		//Eliminar renglones con las etiquetas de los campos que se ocultarón
		var tbl = document.getElementsByTagName('table')[0];
		tbl.deleteRow(0);
		tbl.deleteRow(1);
		tbl.deleteRow(2);
		tbl.deleteRow(3);
		tbl.deleteRow(4);
		
		//alert(document.getElementsByName('cc_name')[0].value);
		//alert(document.getElementsByName('number')[0].value);
		//alert(document.getElementsByName('cc_number')[0].value);
			
		
		</script>
	</div>
