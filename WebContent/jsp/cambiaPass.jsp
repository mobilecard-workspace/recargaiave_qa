<!DOCTYPE html>
<html>
    <head>
        <%request.setAttribute("recursoJS","cambiaPass_1.1.0.js");%>
        <%@ include file="/jsp/genInclude.jsp" %>
        <%@ include file="/jsp/commonsInclude.jsp" %>
        <%String idPage = "goToCambiaPass";
            if (!idPage.equals(nextPage)) {
                request.removeAttribute("llavePublica");
                request.setAttribute("errorMensaje", "Acceso denegado");
                response.sendRedirect(redirectErrorURL);
            }%>
        <title>Recarga</title>
        <script>
            var login = $.jCryption.decrypt('<%=request.getAttribute("login")%>', password);
            <%if (request.getAttribute("errorMensaje") != null) {%>
            var titulo = $.jCryption.decrypt('<%=request.getAttribute("titulo")%>', password);
            var error = $.jCryption.decrypt('<%=request.getAttribute("errorMensaje")%>', password);
            <%}%>      
            <%if (request.getAttribute("mensajeCambioPass") != null) {%>
            var mensajeCambioPass = '<%=request.getAttribute("mensajeCambioPass")%>';
            <%}%>  
        </script>
    </head>
    <body></body>
</html>