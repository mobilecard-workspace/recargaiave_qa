<!DOCTYPE html>

<html>
    <head>
        <%request.setAttribute("recursoJS", "recarga_1.1.1.js");%>
        <style> 
            .x-button-icon.compose3 { -webkit-mask-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAF/klEQVRoBd2aSYgdVRSG+/XrTKYj2YRWG0EEQd26ERRMFHEtCKJLFURMAmbh0oWabJKAu8QkptuOI7pyWASzERxAwbWCYwKKIk4xThna76+805ycruFVvar3qnLh505V1eere+9ft+p1b3l5eaqrqdfrbSD2JXQOPQTL30UsM0UHtLV/AHuQ+O5DGrXTtD0J9G+5MWuEuyaA1qNnkUBN5ynrBmzI4+kirKbxggM1YMsP0HdlFnSngAFZiw6hC8gAY36WvufRFWnQnQEWADqCNHUjZKzrGN2YVdO7E8AEvhm9hCJYXl2zQCO92Y90V1x6jsDvRGVSj4MfQRdw711AX3xkefo2lwlcwF+ivFHN6pN7bxJfJ6a0DQRB346+qwC9YmS9hJortCkxBWcVD7H9GeOi7zbaXkXXxr6Cukb/9emCg8beDZCesy+go5TnYwDchA9pexBppMskrel+q6Y0Aa1DzyBbixrJLTalfU77VvQjsmOL8kWO3dgaYIIR7FEUA3+Ptus9rJVp34q+QvGcWNc2dL3OawUwwRTtoAQ9Z6A+p73IyBYMthXABCOD0g4qb7uoEXsZXeNhrUy7jOwk8iOr3dZhtM6Omzgwwdj7rA9U5V/RaRTb36Dtag9gZdo10t+4c3QT11i/5ROb0gQTDcrg/qFvO3oUnULWbvkrtGUZ2Tb65N7ahs4apM8nAkwwWQb1F33bLUDKbyED9Xmekd3EORvtGjEfOzDBZBnUv/TtQH0FSZKzpk1rA880sgjp62MFBkAjq01FNKgztCUjS95H+9BZZHBZuab3vAcqKo8NmMBkUE+nQPxC2+NIO6FptAvJYbMgY/ubHJu6XtPgxwJMQJrGgo2jJoPy01hmpbYIlVd/jeMzP+lE6MaBCUawmsYx6MSgaLcXGK3ZeEPiObGubWiyg4pgWfVGgQkmz6A0mlqvmsqC/Q9FoLy6tqGlYHUTGgMmmCyD0iufN6j91MuMrNa3vletzRrFvPZGgAlGBqXpFkdI69Mb1BPUyxiUrqcdVCXYRkZYwSAZ1DnkgQW7F80kf/jibkrfmfwxReVjHJ98qskbxby+WkeYYGwax8AFpjWb7G3JqxrUqs+ueXBpfbUBA1FoUApgAFvGoLRJkUFd8taTBjNMWy3ACmYQVNxBaWvoDUqjXMagdL3KBpV2A0YGJqAyBlV2zer5XcvIGvxIwASzCclI4pr9nTYbWW0X9egp48YyPBlfZTc2wJhXBlYwSI+ICBsNSo+nMtNY19M5IxtUhFW9EjDBmEHFUdMrXrKDSi4+NbWbelmDqn0ae/DSwADox2i5ZhxZGZRtKvqU9VIQn8XxHF+v3aA8qJVLAQMgWE03H6jK2lRoZLVeJb3iVTGo2tesgVo+NDAAs+hFFGH/oM0MSi8C+1Cc6vEcX9f6bsSgDNLnQwET0LAGVWUHJdhGDMqDWrkQmGC0qdDDP45aNKg9HNMqgzJIn+cCA6A1u4DiDspPYxnUTtQ6g/KgVs4EBqBJg5LL17qDMqCiPBWYYLIMSh/cEoPShUl63YtT3RtSWlkuPxHYJOa0O0JAz6EYrKb1ApoewCrwMmtW11tCYzOoVLbYSEBXoZ9RBFb9JLofPYXKwOpmaRs6G//euOurpjRBPYy0kUgDVpv6ysIe4pzGNxXD3LwZAonpXhr0KMpKeX1p5yzSuINgdJMmnqZ9BPxPxQ3Ub/ZtI5a1EXkMWD2zW5EuASaiO9CWmiI7xnX2tAlWXCvAjK72wfcgPZJGSVrnesXTyOoFolXJr+F5Iht1OgtWbqxndSvWbLzbKyNMh35Ivi4eULK+yPE72worlgSY2SznvQvpX3SrJn3d0DTWY6u1yUZ4jgi3jRDlEufuBrY1bpzFYmv4Rg64JeugjHYZkt6a9CIg2NYZVFrcM0xnvdzfjfppB4Q2/fL3A/oYvY2OA6pvWZ1JGuE1KG866/8vvkBfo+PoBJDfkncyCfhWFKezNvvvoxPoA/Q5kD+Rdz4J+AFH8Rnldwc6BeT3ru+yKPag+AR9hN5Bn6IzgOpzzWWZ/gcl37KmeWjXcgAAAABJRU5ErkJggg=='); }
        </style>
        <%@ include file="/jsp/genInclude.jsp" %>
        <%@ include file="/jsp/commonsInclude.jsp" %>
        <%String idPage = "goToRecargaIAVE";
            if (!idPage.equals(nextPage)) {
                request.removeAttribute("llavePublica");
                request.setAttribute("errorMensaje", "Acceso denegado");
                response.sendRedirect(redirectErrorURL);
            }%>
        <title>Recarga</title>
        <script>
            var aliasTag = $.jCryption.decrypt('<%=request.getAttribute("aliasTag")%>', password);
            var tag = $.jCryption.decrypt('<%=request.getAttribute("tag")%>', password);
            var productos = Ext.JSON.decode($.jCryption.decrypt('<%=request.getAttribute("productos")%>', password));
            var comision = Ext.JSON.decode($.jCryption.decrypt('<%=request.getAttribute("comision")%>', password));
            var login=$.jCryption.decrypt('<%=request.getAttribute("login")%>', password);
            <%if (request.getAttribute("mensajeExito") != null) {%>
            var mensajeExito = '<%=request.getAttribute("mensajeExito")%>';
            <%}%>
            <%if (request.getAttribute("mensajeError") != null) {%>
            var tituloError = <%=request.getAttribute("titulo")%>);
            var mensajeError = <%=request.getAttribute("mensajeError")%>;
            <%}%>
            <%if (request.getAttribute("mensajeCambioPass") != null) {%>
            var mensajeCambioPass = '<%=request.getAttribute("mensajeCambioPass")%>';
            <%}%>           
            var tipoTDC = '<%=request.getAttribute("tipoTDC")%>';
            		
        </script>
    </head>
    <body></body>
</html>